import os

os.chdir('MCMC_Greenhouse/Processing')
os.system('python mcmc3.py')
os.system('python posteriorPredictiveSampling.py')

os.chdir('../../Analysis_Greenhouse/')
os.system('./updateElements.sh')
os.system('python Analysis_Master.py')
