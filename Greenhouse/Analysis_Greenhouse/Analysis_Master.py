#This script is a cumulation of a number of different scripts which analyze modeled and observed data from the greenhouse experiment conducted by Gerard Sapes. 

#Last Edit: 9-14-17

from __future__ import division
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import os, glob
os.environ['MKL_THREADING_LAYER']='GNU'
from scipy.stats import gaussian_kde
import scipy
import matplotlib.dates as mdates
from pandas.tseries.offsets import *
from pandas.tools.plotting import table
import math
from pandas.tools.plotting import table
import seaborn as sns
import pymc3 as mc
import seaborn as sns
import pickle
import matplotlib.dates as mdates

plt.rcParams.update({'font.size': 16})

#plt.style.use(['ggplot','seaborn-talk'])
#sns.set_style("dark")
#sns.set_style("whitegrid", {'axes.grid' : False})
def __Main__():
	dfTs_obs, dfLWPData, dfTransp, dfPoint_obs = Load_Observed_Data()
	simswc, simtrp, simlwp = import_modeled()

	#Traces_and_Tables()

	#VWC_Plot(simswc, dfTs_obs)
	#VWC_Scatter(simswc, dfTs_obs)
	Transp_Plot(simtrp, dfTransp)
	Transp_Scatter(simtrp, dfTransp)
	#LWP_Plot(simlwp, dfLWPData)
	#legend(simlwp,dfLWPData)
	#LWP_Scatter(simlwp, dfLWPData)
	
def Load_Observed_Data():
	#Load Observed Data
	dfPoint_obs = pd.read_csv('Obs_Data/Master_Data_Caelan.csv')
	dfPoint_obs['Measurement.Time'] = dfPoint_obs['Date'].map(str) +' '+ dfPoint_obs['Time']+':00'
	dfPoint_obs = dfPoint_obs.set_index(pd.DatetimeIndex(dfPoint_obs['Measurement.Time']))
	
	#Extract Rocky Mountain Race
	dfPoint_obs = dfPoint_obs[dfPoint_obs.Race == 'RMR']
	
	#Delete Values noted as problematic by Gerard Sapes the Data Collector
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '6/22/2017 13:58:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:29:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:22:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:28:00']
	
	#Reset Problematic PLC
	dfPoint_obs['Plant_PLC'] = np.where(dfPoint_obs['Plant_PLC_%'] > 1, 
	                                    dfPoint_obs['Plant_PLC_%'],0)
	dfPoint_obs['Stem_PLC'] = np.where(dfPoint_obs['Stem_PLC_%'] > 1, 
	                                   dfPoint_obs['Stem_PLC_%'],0)
	
	#Remove Nighttime Observations There were two obs at 1 in the morning.
	dfPoint_obs = dfPoint_obs[dfPoint_obs.index.hour > 2]
	
	
	dfPoint_obs['Plant_PLC'] = np.where(dfPoint_obs['Plant_PLC_%'] > 1, dfPoint_obs['Plant_PLC_%'],0)
	dfPoint_obs['Stem_PLC'] = np.where(dfPoint_obs['Stem_PLC_%'] > 1, dfPoint_obs['Stem_PLC_%'],0)
	
	
	dfTransp = dfPoint_obs.copy()
	dfTransp['Transp'] = dfTransp['Trmmol']*1.802E-8
	dfTransp['Transp'] = dfTransp['Transp']*8.64E7
	
	dfTransp = dfTransp.clip(lower=0)
	dfTransp = dfTransp.dropna(subset=['Transp'])
	dfTransp['Transp'] = dfTransp['Transp']*.27
	dfTransp.Transp = dfTransp.Transp.astype(float)
	
	#Sample mean Transpiration by day.
	dfTransp = dfTransp[['Transp']].resample('24H').mean()
	dfTransp = dfTransp.dropna(subset=['Transp'])
	
	dfLWPData = dfPoint_obs.copy()
	#Resample mean LWP by day.
	dfLWPData.Leaf_WP_MPa = dfLWPData.Leaf_WP_MPa.astype(float)
	dfLWPData = dfLWPData[['Leaf_WP_MPa']].resample('24H').mean()
	dfLWPData = dfLWPData.dropna(subset=['Leaf_WP_MPa'])
	
	dfTs_obs=pd.read_csv('Obs_Data/All_Data_Plants_5PM_Caelan.csv',delimiter=',')
	dfTs_obs['datetime'] = pd.to_datetime(dfTs_obs['Measurement.Time'])
	dfTs_obs=dfTs_obs.set_index(pd.DatetimeIndex(dfTs_obs['Measurement.Time']))
	dfTs_obs['Water_Potential'] = (dfTs_obs['CP_WP_mean_MPa'])
	dfTs_obs['VWC'] = (dfTs_obs['CP_VWC_top_mean_.']/100.)
	return dfTs_obs, dfLWPData, dfTransp, dfPoint_obs

def Traces_and_Tables():
	##Analysis of Monte Carlo runs for the Greenhouse.
	model = mc.Model()
	with model:
	    # Parameter Priors 
	    gsmax = mc.Normal('gsmax', mu=0.208, sd=0.1)
	    gsmin = mc.Normal('gsmin', mu=0.011, sd=0.01)
	    optT = mc.Normal('optT', mu=15.4, sd=5)
	    deltamaxT = mc.Normal('deltamaxT', mu =15.18, sd=10)
	    deltaminT = mc.Normal('deltaminT', mu=11.1,sd=5)
	    gslight = mc.Normal('gslight', mu=537, sd=1000)
	    gsvpd = mc.Normal('gsvpd', mu=0.0001, sd=4E-3)
	    lwp_low = mc.Normal('lwp_low', mu=7.7, sd=5)
	    sperryk = mc.Normal('sperryk', mu=.275, sd=.3)
	
	    lamb = mc.Normal('lambd', mu=2.106, sd=.1)
	    ksat = mc.Normal('ksat', mu=.000898, sd=2.7E-4)
	    psiae = mc.Normal('psiae', mu=0.309, sd=0.05)
	    poros = mc.Normal('poros', mu=0.401, sd =.01)
	    sd1 = mc.HalfNormal('sd1', sd=10)
	    sd2 = mc.HalfNormal('sd2', sd=50)
	    sd3 = mc.HalfNormal('sd3', sd=10)
	 
	    trace_full  = mc.backends.text.load('Modeled_Data/trace_results')
	    trace = trace_full
	    trace = trace_full[500:]
	
	(mc.df_summary(trace)).to_csv('../Figures_Greenhouse/Greenhouse_MC_Table.csv')
	
	(mc.df_summary(trace)).to_html('table.html')
	
	
	d = {}
	for var in trace.varnames:
	    if "log" not in var:
	        d[var] = trace[var][:-2].mean()
	        mc.traceplot(trace, varnames=[var], grid=True, lines=d)
		plt.savefig('../Figures_Greenhouse/Traces/Greenhouse_MC_trace_'+var)
		plt.close()

def plot_timeseries(df_sim, df_obs, **args):
    mean = df_sim.mean(axis=1)
    q025 = df_sim.quantile(q=0.025, axis=1)
    q01 = df_sim.quantile(q=0.1, axis=1)
    q20 = df_sim.quantile(q=0.2, axis=1)
    q80 = df_sim.quantile(q=0.8, axis=1)
    q90 = df_sim.quantile(q=0.9, axis=1)
    q985 = df_sim.quantile(q=0.985, axis=1)

    #q80 = q80.clip(-10,0)
    print q80
    p = plt.figure(figsize=(8,5))
    ax = plt.plot(mean, c='red', label="Mean Modeled",alpha=.9)
    ax = plt.fill_between(mean.index, q20, q80, label='60% Confidence Interval',color='0.1', alpha=0.35)

    
    df_obs = df_obs.dropna()
    ax = plt.plot(df_obs.index, df_obs,'o', color='blue',label="Observed", **args)
    #ax = plt.plot()
    #ax = plt.plot(data_swc)
    #ax.axvline(data.mean(), color='r', ls='--', label='True mean')
    #ax.legend()
    #plt.ylim(-15,5)
    return p, ax, mean

def import_modeled():
	dfData = pd.read_csv('Modeled_Data/postPred3.csv')
	
	dfData = dfData.drop('Unnamed: 0', axis =1)
	dfPredTraces = dfData
	
	## Give dates to the dataframes
	n = 3563
	simswc = dfPredTraces[:n]
	simtrp = dfPredTraces[n:n*2]
	simlwp = dfPredTraces[n*2:n*3]
	
	ind = pd.date_range('2017-02-27 20:00:00', periods=simlwp.shape[0], freq='H')
	simswc.index = ind
	simtrp.index = ind
	simlwp.index = ind
	
	simlwp = simlwp.drop(simlwp.loc['2017-02-27'].index)
	simtrp = simtrp.drop(simtrp.loc['2017-02-27'].index)
	simlwp = simlwp.drop(simlwp.loc['2017-07-26'].index)
	simtrp = simtrp.drop(simtrp.loc['2017-07-26'].index)
	return simswc, simtrp, simlwp



def VWC_Plot(simswc, dfTs_obs):
	p, ax, mean =  plot_timeseries(simswc, dfTs_obs['VWC'])
	
	a = p.gca()
	myFmt = mdates.DateFormatter('%m/%d')
	a.xaxis.set_major_formatter(myFmt)
	a.yaxis.set_major_locator(plt.MaxNLocator(5))
	plt.locator_params(axis='y', nticks=4)
	a.set_ylabel("Soil VWC $(m^3 m^{-3})$",fontsize=20)
	#a.legend(fontsize=20)
	plt.tight_layout()
	plt.ylim(-.02,.2)
	p.savefig("../Figures_Greenhouse/VWC_mcmc",dpi=600)
	plt.close()

def VWC_Scatter(simswc,dfTs_obs):
	mean = simswc.mean(axis=1)
	fig = plt.figure(figsize=[7,7])
	SWC_both = pd.concat([dfTs_obs['VWC'],mean],axis=1)
	SWC_both = SWC_both.dropna(subset=['VWC'])
	
	slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(
	    SWC_both['VWC'], SWC_both[0])
	
	print 'VWC', ' Rsquared=',r_value**2,' p_value=',p_value,' slope=',slope,' intercept=',intercept	
	
	plt.plot(SWC_both['VWC'],SWC_both[0],'.',label='Rsquared='+str(r_value**2)[0:5])
	plt.legend(loc=0)
	plt.xlabel('Observed VWC ($m3/m3$)')
	plt.ylabel('Mean Modeled VWC ($m3/m3$)')
	plt.plot([0,1],[0,1],'--')
	plt.plot([0,1],[intercept,slope+intercept],label='bestfit')
	plt.ylim(0,.18)
	plt.xlim(0,.18)

	plt.savefig('../Figures_Greenhouse/VWC_scatter')
	plt.close()

def Transp_Plot(simtrp, dfTransp):
	p, ax, mean = plot_timeseries(simtrp, dfTransp['Transp'])
	a = p.gca()
	a.yaxis.set_major_locator(plt.MaxNLocator(5))
	myFmt = mdates.DateFormatter('%m/%d')
	a.xaxis.set_major_formatter(myFmt)
	a.set_ylabel("Transpiration ($mm d^{-1}$)",fontsize=20)
	plt.ylim(0,2)
	plt.tight_layout()
	p.savefig("../Figures_Greenhouse/Transp_mcmc", dpi=600)
	plt.close()

def Transp_Scatter(simtrp,dfTransp):
	mean = simtrp.mean(axis=1)
	mean = mean.resample('24H').max()
	
	peak1 = mean.max()
	peak2 = dfTransp['Transp'].max()
	peak = max(peak1,peak2)
	
	fig = plt.figure(figsize=[7,7])
	Transp_both = pd.concat([dfTransp['Transp'],mean],axis=1)
	Transp_both = Transp_both.dropna(subset=['Transp'])
	slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(
	    Transp_both['Transp'], Transp_both[0])
	print 'Transpiration', ' Rsquared=',r_value**2,' p_value=',p_value,' slope=',slope,' intercept=',intercept	
	
	plt.plot(Transp_both['Transp'],Transp_both[0],'.',label='Rsquared='+str(r_value**2)[0:5])
	plt.legend(loc=0)
	plt.xlabel('Observed Transpiration ($mm/d$)')
	plt.ylabel('Mean Modeled Transpiration ($mm/d$)')
	plt.ylim(-.1,peak)
	plt.xlim(-.1,peak)
	plt.plot([0,1],[0,1],'--')
	plt.plot([0,1],[intercept,slope+intercept],label='bestfit')
	plt.savefig('../Figures_Greenhouse/Transp_scatter')
	plt.close()

def LWP_Plot(simlwp, dfLWPData):
	p, ax ,mean= plot_timeseries(simlwp, dfLWPData['Leaf_WP_MPa'])

       	sperryb = 7.84 #PLC= 1 - e^(-(-Psi/b))^c  7.84    1.49
        sperryc = 1.49
	mean = simlwp.mean(axis=1)
	mean = mean.resample('24H').min()
        PLC = 1.0 - math.e ** (-((-np.asarray(mean) / sperryb) ** sperryc))
	
	a = p.gca()
	a.yaxis.set_major_locator(plt.MaxNLocator(5))
	ax2 = a.twinx()
	ax2.plot(mean.index,PLC,c='g')
	myFmt = mdates.DateFormatter('%m/%d')
	a.xaxis.set_major_formatter(myFmt)
	a.set_ylabel(r"LWP ($MPa$)",fontsize=20)
	ax2.set_ylabel(r"PLC",fontsize=20)
	#a.legend(fontsize=20)
	a.set_ylim(-10,2)
	ax2.set_ylim(-5,1)
	plt.tight_layout()
	p.savefig("../Figures_Greenhouse/LWP_mcmc",dpi=600)
	plt.close()

def legend(simlwp, dfLWPData):
	p, ax ,mean= plot_timeseries(simlwp, dfLWPData['Leaf_WP_MPa'])
	
	a = p.gca()
	myFmt = mdates.DateFormatter('%m/%d')
	a.xaxis.set_major_formatter(myFmt)
	a.set_ylabel(r"LWP, ($MPa$)",fontsize=20)
	plt.legend(bbox_to_anchor=(0., 1.02, 1, .102), loc=3,
        	ncol=3, mode="expand", borderaxespad=0.)	
	plt.ylim(-10,0)
	p.savefig("../Figures_Greenhouse/legend")
	plt.close()

def LWP_Scatter(simlwp,dfLWPData):
	mean = simlwp.mean(axis=1)
	mean = mean.resample('24H').min()
	
	peak1 = mean.min()
	peak2 = dfLWPData['Leaf_WP_MPa'].min()
	peak = min(peak1,peak2)
	
	LWP_both = pd.concat([dfLWPData['Leaf_WP_MPa'],mean],axis=1)
	
	LWP_both = LWP_both.dropna(subset=['Leaf_WP_MPa'])
	slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(
	    LWP_both['Leaf_WP_MPa'], LWP_both[0])
	print 'LWP', ' Rsquared=',r_value**2,' p_value=',p_value,' slope=',slope,' intercept=',intercept	
	
	fig = plt.figure(figsize=[7,7])
	plt.plot(LWP_both['Leaf_WP_MPa'],LWP_both[0],'.',label='Rsquared='+str(r_value**2)[0:5])
	plt.legend(loc=0)
	plt.xlabel('Observed Leaf Water Potential (MPa)')
	plt.ylabel('Mean Modeled Leaf Water Potential (MPa)')
	plt.ylim(peak,.1)
	plt.xlim(peak,.1)
	plt.plot([-10,0],[-10,0],'--')
	plt.plot([-10,0],[intercept-(slope*10),intercept],label='bestfit')
	plt.savefig('../Figures_Greenhouse/LWP_Scatter')
	plt.close()
	mean.to_csv('Mean_LWP.csv')

__Main__()
