This files that were used in the thesis are in the greenhouse folder. If i need to re-run the simulations then I will need to move to the final greenhouse folder. This will be a pain though, because it runs slightly differently each time due to the stochastic nature of the MCMC process.

File Run Order. 
1.MCMC_Greenhouse/Processing/	  python mcmc3.py  
2.MCMC_Greenhouse/Processing/ 	  python posteriorPredictiveSampling.py 
3.Analysis_Greenhouse/  ./updateElements.sh
4.Analysis_Greenhouse/	python Analysis_Master.py 

These steps can be run using the Execute.py script.
