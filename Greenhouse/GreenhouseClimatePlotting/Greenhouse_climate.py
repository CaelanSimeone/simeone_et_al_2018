import numpy as np
import matplotlib.pylab as plt
import pandas as pd 

#plt.style.use(['ggplot','seaborn-poster'])

Wilbur = pd.read_csv('Wilbur_Caelan.csv')
Senglar = pd.read_csv('Senglar_Caelan.csv')
Senglar = Senglar.set_index(pd.DatetimeIndex(Senglar['Date']))
Wilbur = Wilbur.set_index(pd.DatetimeIndex(Wilbur['Date']))

Wilbur=Wilbur.drop(Wilbur.index[:(310+3336)])
Senglar = Senglar.drop(Senglar.index[:3336])
#print Senglar
#print Wilbur
Wilbur['Radiation (W/m2)']=Wilbur['Average Rad flux dens (kW/m2)']*1000
#Wilbur['Air Temp (C)']=Wilbur['Air temp (Marcos Sensor)']
Wilbur['Windspeed']=Wilbur['Wind Speed (m/s)']
Senglar['RH']=Senglar['RH (%)']*.01
Senglar['Air Temp (C)']=Senglar['Air Temp from RH sensor (C)']
Sigma=5.670e-8
Senglar['Ldown']=Sigma*((Senglar['Air Temp from RH sensor (C)']+273)**4)

Ldown=np.asarray(Senglar['Ldown'])
Tavg=np.asarray(Senglar['Air Temp (C)'])
RH=np.asarray(Senglar['RH'])
Sdown=np.asarray(Wilbur['Radiation (W/m2)'])
Windspeed=np.asarray(Wilbur['Windspeed'])+.5
Precip=np.zeros(len(Windspeed))
Precip[335] = 8e-7
Precip[954] = 8e-7
Precip[1754] = 8e-7 

Wilbur['Precip'] = Precip * 3.6E6

f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(Wilbur['Radiation (W/m2)'],color='red',label='Solar Radiation')
axarr[0].set_ylabel('Solar Radiation $(W m^{-2})$')
ax1=axarr[0].twinx()
ax1.plot(Senglar['Air Temp (C)'],':',color='blue',label='Air Temperature')
ax1.set_ylabel('Air Temperature $(C)$')
axarr[1].plot(Senglar['RH'],color='grey',label='Relative Humidity')
axarr[1].set_ylabel('Relative Humidity') 
ax2 = axarr[1].twinx()
ax2.plot(Wilbur['Precip'],color = 'black',label='Precipitation')
ax2.set_ylabel('Precipitation $(mm)$')
ax2.set_ylim(.1,4)
ax2.set_xlabel('Date')
ax1.set_xlabel('Date')
axarr[0].legend(loc=2)
ax1.legend(loc=1)
axarr[1].legend(loc=2)
ax2.legend(loc=1)
plt.savefig('GreenhouseClimate.png',dpi=600)
plt.show()

