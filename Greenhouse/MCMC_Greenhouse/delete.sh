#!/bin/sh
set +e
rm -R ./res*
rm ./Spatial/kl1* 
rm ./Spatial/lam* 
rm ./Spatial/keff*
rm ./Spatial/tab*.tab

cp ./Spatial/poros.map ./Spatial/por.map
rm ./Spatial/poros*.map
cp ./Spatial/por.map ./Spatial/poros.map

cp ./Spatial/psi_ae.map ./Spatial/pae.map
rm ./Spatial/psi*.map 
cp ./Spatial/pae.map ./Spatial/psi_ae.map

rm ./Spatial/por.map
rm ./Spatial/pae.map

rm ./Postprocess/file*
rm ./file*.ini
