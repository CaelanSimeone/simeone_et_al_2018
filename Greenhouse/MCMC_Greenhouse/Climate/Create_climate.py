import numpy as np
import csv
import matplotlib.pylab as plt

Timestep=np.arange(0,2920)
Sdown=(np.sin(np.pi*Timestep/4)/2 + .5)*342 * ((np.sin(np.pi*Timestep/(4*365))/4)+.75)
Tave=(np.sin(np.pi*Timestep/4)/2 + .4)*5 +((np.sin(np.pi*Timestep/(4*365))*10))+5+np.random.random_sample(2920)*5
Tmax=Tave+5
Tmin=Tave-5
Precip=np.zeros(2920)
Ldown=np.ones(2920)*280
Windspeed=np.ones(2920)
RH=np.ones(2920)*-.2

def Writefile(name,array):
	"""This function creates climate text files for ech2o. The input is the name of the desired file and an array of values that has the same name as the desired file. """
	with open(str(name)+'.txt', 'wb') as csvfile:
    		length=len(array)
		writer = csv.writer(csvfile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
    		writer.writerow('#somestuffhere')
		writer.writerow([str(length)])
    		writer.writerow(np.arange(1,length+1))
    		writer.writerow(['1'])
    		writer.writerow(['1'])
    		for i in array:
			writer.writerow([i])

#Writefile('Sdown',Sdown)
#Writefile('Tavg',Tave)
#Writefile('Tmax',Tmax)
#Writefile('Tmin',Tmin)
#Writefile('Precip',Precip)
#Writefile('Windspeed',Windspeed)
#Writefile('RH',RH)
#Writefile('Ldown',Sdown)
