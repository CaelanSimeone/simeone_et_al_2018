import numpy as np
import matplotlib.pylab as plt
import pandas as pd 
import Create_climate as cc

Wilbur=pd.read_csv('CR10X_Wilbur_all_data.csv',delimiter='\t')
Senglar=pd.read_csv('CR10X_Senglar_all_data.csv',delimiter='\t')
Wilbur=Wilbur.drop(Wilbur.index[:310])
#Wilbur['Name']=Wilbur.index()-309
Wilbur['Name']=np.arange(0,len(Wilbur))+1
#print Senglar
#print Wilbur
Wilbur['Radiation (W/m2)']=Wilbur['Total radiation(MJ/m2)']*277.77
Wilbur['Air Temp (C)']=Wilbur['Air temp (Marcos Sensor)']
Wilbur['Windspeed']=Wilbur['Wind Speed']
Senglar['RH']=Senglar['Relative Humidity']*.01
Senglar['Air Temp (C)']=Senglar['Air Temperature RH']
Sigma=5.670e-8
Senglar['Ldown']=Sigma*((Senglar['Air Temp (C)']+273)**4)

Ldown=np.asarray(Senglar['Ldown'])
Tavg=np.asarray(Senglar['Air Temp (C)'])
RH=np.asarray(Senglar['RH'])
Sdown=np.asarray(Wilbur['Radiation (W/m2)'])
Windspeed=np.asarray(Wilbur['Windspeed'])+.5
Precip=np.zeros(len(Windspeed))

cc.Writefile('Tavg',Tavg)
cc.Writefile('Tmax',Tavg)
cc.Writefile('Tmin',Tavg)
cc.Writefile('RH',RH)
cc.Writefile('Sdown',Sdown)
cc.Writefile('Ldown',Ldown)
cc.Writefile('Windspeed',Windspeed)
cc.Writefile('Precip',Precip)

#plt.plot(Wilbur['Radiation (W/m2)'])
#plt.plot(Wilbur['Name'],Wilbur['Air Temp (C)'])
#plt.plot(Wilbur['Windspeed'])
#plt.plot(Senglar['Air Temp (C)'])
#plt.plot(Senglar['RH'])
#plt.plot(Wilbur['Ldown'])
#plt.show()
