import os
#os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu"
os.environ["OMP_NUM_THREADS"] = "1"
os.environ['MKL_THREADING_LAYER']='GNU'
import mapio
from customDist import logp_gmix
import pymc3 as mc
from subprocess import call, check_call
import writeVegetParamTab
import theano.tensor as T
from theano import as_op
from scipy import optimize 
from scipy.stats import gaussian_kde
import numpy as np
import pandas as pd
from shutil import copyfile, rmtree
import pickle
from pandas.tseries.offsets import *

# LOAD OBSERVATIONS
dfObsAll=pd.read_csv('../All_Data_Plants_5PM_Caelan.csv',delimiter=',')
dfObsAll=dfObsAll.set_index(pd.DatetimeIndex(dfObsAll['Measurement.Time']))
#dfObsAll['Water_Potential'] = (dfObsAll['RMR_WP_mean_MPa']+dfObsAll['CP_WP_mean_MPa'])/2.
dfObsAll['VWC'] = (dfObsAll['RMR_VWC_top_mean_.'])/100#+dfObsAll['CP_VWC_top_mean_.'])/200.
dfObsAll = dfObsAll.resample('24H').mean()

dfData = pd.read_csv('../Master_Data_Caelan.csv')
dfData['Measurement.Time'] = dfData['Date'].map(str) +' '+ dfData['Time']+':00'
dfData = dfData.set_index(pd.DatetimeIndex(dfData['Measurement.Time']))

#Extract Rocky Mountain Race
dfData = dfData[dfData.Race == 'RMR']

#Delete Values noted as problematic by Gerard Sapes the Data Collector
dfData = dfData[dfData['Measurement.Time'] != '6/22/2017 13:58:00']
dfData = dfData[dfData['Measurement.Time'] != '7/7/2017 13:29:00']
dfData = dfData[dfData['Measurement.Time'] != '7/7/2017 13:22:00']
dfData = dfData[dfData['Measurement.Time'] != '7/7/2017 13:28:00']

#Remove night measurements, and edit transp values. 
dfData = dfData[dfData.index.hour > 2]
dfTransp = dfData.copy()
dfTransp['Transp'] = dfTransp['Trmmol']*1.802E-8
dfTransp['Transp'] = dfTransp['Transp']*8.64E7
dfTransp = dfTransp.clip(lower=0)
dfTransp = dfTransp.dropna(subset=['Transp'])
dfTransp['Transp'] = dfTransp['Transp']*.27
dfTransp.Transp = dfTransp.Transp.astype(float)

#Sample mean Transpiration by day.
dfTransp = dfTransp[['Transp']].resample('24H').mean()
dfTransp = dfTransp.dropna(subset=['Transp'])
#dfTransp.index =dfTransp.index + DateOffset(hour= 12)

#Resample mean LWP by day.
dfData.Leaf_WP_MPa = dfData.Leaf_WP_MPa.astype(float)
dfData = dfData[['Leaf_WP_MPa']].resample('24H').mean()
dfData = dfData.dropna(subset=['Leaf_WP_MPa'])
#dfData.index = dfData.index + DateOffset(hour= 12)


swcobs = 0
trnsobs = 0
lwpobs = 0


# Default vegetation parameter values

ks = [
    "SpeciesID",
    "NPP / GPPRatio",
    "gsmax(ms - 1)",
    "gsmin(ms - 1)",
    "CanopyQuantumEffic(gCJ - 1)",
    "MaxForestAge(yrs)",
    "OptimalTemp(C)",
    "MaxTemp(C)",
    "MinTemp(C)",
    "FoliageAllocCoef_a",
    "FoliageAllocCoef_b",
    "StemAllocCoef_a",
    "StemAllocCoef_b",
    "gs_light_coeff",
    "gs_vpd_coeff",
    "lwp_low(-m)",
    "lwp_high(-m)",
    "WiltingPnt",
    "SpecificLeafArea(m2g - 1)",
    "SpecificRootArea(m2kg - 1)",
    "Crown2StemDRat",
    "TreeShapeParam",
    "WoodDens(gCm - 2)",
    "Fhdmax",
    "Fhdmin",
    "LeafTurnoverRate(s - 1)",
    "MaxLeafTurnoverWaterStress(s - 1)",
    "LeafTurnoverWaterStressParam",
    "MaxLeafTurnoverTempStress(s - 1)",
    "LeafTurnoverTempStressParam",
    "ColdStressParam(degC)",
    "RootTurnoverRate(s - 1)",
    "MaxCanStorageParam(m)",
    "albedo",
    "emissivity",
    "KBeers",
    "CanopyWatEffic(gCm - 1)",
    "sperry_d_par(m)",
    "sperry_c_par(m)",
    "Sperry_Kp(ms - 1)",
    "gsr_param_a",
    "is_grass",
    "DeadGrassLeafTurnoverRate(s - 1)",
    "DeadGrassLeafTurnoverTempAdjustment(degC)"]
    
vals = [
    1, 0.47, 0.01, 0.000000001,  0.0018, 290, 15.45, 45.55, 5.787, 2.235, 0.0006, 3.30E+000, 6.00E-007, 1800, 5.09e-4,
    4.1, 0.00002324, 0.009, 0.009, 0.022, 0.25, 0.4, 220000, 15, 5, 8.56E-009, 1.80E-008, 0.2,
    1.80E-008, 0.2, 1, 5.34E-009, 0.0000624, 0.1, 0.95, 0.55, 800.0, 7.84, 1.49, 0.0073, 8, 0,
    0, 0]

vegPars = dict(zip(ks, vals))


key_pars = ['gsmax', 'gsmin','optT','deltamaxT','deltaminT', 'gslight', 'gsvpd', 'lwp_low', 'sperryk', 'lambd', 'ksat', 'psiae','poros', 'sd1', 'sd2', 'sd3']
init_pars = [0.238, 0.013,16.61,22.92,11.92, 487, 0.00095, 7, 0.3313, 2.5, 0.000571, 0.335,0.401, 1 ,5 ,5]
start_pars = dict(zip(key_pars, init_pars))

count = 1
@as_op(itypes=[T.dscalar]*13, otypes=[T.dvector,T.dvector,T.dvector])
def theta(gsmax,
	 gsmin,
	 optT,
	 deltamaxT,
	 deltaminT,
         gslight,
         gsvpd,
         lwp_low,
         sperryk,
         lamb,
         ksat, 
         psiae,
	 poros):
    global count


    count_str=str(count)
    if count_str[-2:] == '00':
    	print "Number of model calls ", count
    count+=1
    
    # Do some checks on parameters    
    gsmax = max(gsmax, 0.00002)
    gsmin = min(gsmin,0.001) #Edit
    gsmin = max(gsmin,0.000000001)
    deltamaxT = max(deltamaxT,1)
    deltaminT = max(deltaminT,1)
    gsvpd = max(gsvpd, 0.0000001)
    lwp_low = max(lwp_low, 0.01)
    lwp_low = min(lwp_low,15)
    sperryk = max(sperryk, 0)
    ksat = max(ksat,0)
    lamb = max(lamb, 2)
    psiae = max(psiae, 0.1) 
    #for f in glob.glob('../Results/*'):
    #    os.remove(f) # Makes sure we are not reading all data if the process fails

    minT = optT - deltaminT
    maxT = optT + deltamaxT

    vegPars['gsmax(ms - 1)'] = gsmax
    vegPars['gsmin(ms - 1)'] = gsmin
    vegPars['gs_light_coeff'] = gslight
    vegPars['gs_vpd_coeff'] = gsvpd
    vegPars['lwp_low(-m)'] = lwp_low
    vegPars['Sperry_Kp(ms - 1)'] = sperryk
    vegPars['OptimalTemp(C)'] = optT 
    vegPars['MaxTemp(C)'] = maxT      
    vegPars['MinTemp(C)'] = minT
    # Generate unique file and folder names
    configini = os.tempnam('../', '')+".ini"
    speciesParams = os.tempnam('../Spatial', 'tab')+".tab"
    
    lammap = os.tempnam('../Spatial', 'lam')+".map"
    keffmap = os.tempnam('../Spatial', 'keff')+".map"
    psimap = os.tempnam('../Spatial', 'psi')+".map"
    porosmap = os.tempnam('../Spatial','poros')+".map"
    resfolder = os.tempnam('../', 'res')
    os.makedirs(resfolder)
 
    # write random config ini file
    copyfile(r"../config.ini", configini)
    
    # write random outputfolder file
    keys = ["Output_Folder", "Brooks_Corey_lambda",
             "Horiz_Hydraulic_Conductivity",
             "Air_entry_pressure", "Species_Parameters"]
    values = [resfolder, lammap, keffmap, psimap, speciesParams]
    mapio.modifyConfigIni(r"../config.ini", configini, keys, values)
    
    # Generate SpeciesParameter File
    writeVegetParamTab.writeVegetParamTab(speciesParams, [vegPars])
    
    
    # write pcraster maps
    mapio.modifyPCRasterMap('../Spatial/BClambda.map', lamb, dest=lammap)
    mapio.modifyPCRasterMap('../Spatial/Keff.map', ksat, dest=keffmap)
    mapio.modifyPCRasterMap('../Spatial/psi_ae.map', psiae, dest=psimap)
    mapio.modifyPCRasterMap('../Spatial/poros.map', poros, dest=porosmap)

    log = os.tempnam('./','')+".txt"
    
    #Call the actual model
    try:
    	with open(log, 'w') as f:
                    call(["ech2o", configini], stdout=f)
	#print 'Run Sucess'	
    except:
        print "Run failed, filling resfolder with dummy results\n"     
        copyfile('../dummyResults.tab', resfolder+'/LeafWaterPot[0].tab')
        copyfile('../dummyResults.tab', resfolder+'/Transpiration[0].tab')
        copyfile('../dummyResults.tab', resfolder+'/SoilMoistureL1.tab')
    
    # postrocess and return the data
    
    dfSimLWP = pd.read_table(resfolder+'/LeafWaterPot[0].tab', header=None, skiprows=6,names=['site1','nan'])

    dfSimTransp = pd.read_table(resfolder+'/Transpiration[0].tab', header=None, skiprows=6,names=['site1','nan'])

    dfSimVWC = pd.read_table(resfolder+'/SoilMoistureL1.tab', header=None, skiprows=6,names=['site1','nan'])

   
		
    ind = pd.date_range('2017-02-27 12:00:00', periods=dfSimVWC.shape[0], freq='H')

    dfSimLWP.index = ind
    dfSimTransp.index = ind
    dfSimVWC.index = ind

    dfSimTransp['site1'] = dfSimTransp['site1'] * 8.64E7
     
    dfSimLWP = dfSimLWP.resample('24H').min()
    dfSimTransp = dfSimTransp.resample('24H').max()
    dfSimVWC = dfSimVWC.resample('24H').mean()
 

    #dfSimTransp = dfSimTransp.resample('T').asfreq().interpolate()
    #dfSimLWP = dfSimLWP.resample('T').asfreq().interpolate()    
    
    global dfObsAll
    global swcobs
    global trnsobs
    global lwpobs



    #Concatenate Modeled and Observed
    swcobs = pd.concat([dfObsAll[['VWC']].dropna()
    , dfSimVWC], axis=1, join='inner')

        
    trnsobs = pd.concat([dfTransp[['Transp']]
    , dfSimTransp], axis=1, join='inner')
 
    lwpobs = pd.concat([dfData['Leaf_WP_MPa']
    , dfSimLWP], axis=1, join='inner')

    #print 'Lenght of transp and lwp'
    #print len(trnsobs)
    #print len(lwpobs)	
    
    os.remove(configini)
    os.remove(lammap)
    os.remove(keffmap)
    os.remove(psimap)
    os.remove(porosmap)
    os.remove(speciesParams)
    [os.remove(os.path.join('../Spatial', f)) for f in os.listdir('../Spatial') if f.endswith(".xml")] # remove leftover .xml files
    rmtree(resfolder)
    os.remove(log)
    
    if count_str[-2:] == '00': 
    	print "Parameter values, ", gsmax, gslight, \
    	optT,deltamaxT,deltaminT,gsvpd, lwp_low, sperryk, lamb, \
    	ksat, psiae, poros
    

    return swcobs['site1'].values, trnsobs['site1'].values, lwpobs['site1'].values



model = mc.Model()
with model:


    # Parameter Priors 
    gsmax = mc.Normal('gsmax', mu=0.238, sd=0.08)
    gsmin = mc.Normal('gsmin', mu=0.013, sd=0.008)
    optT = mc.Normal('optT', mu=16.6, sd=4)
    deltamaxT = mc.Normal('deltamaxT', mu =22.92, sd=7)
    deltaminT = mc.Normal('deltaminT', mu=11.9,sd=5)
    gslight = mc.Normal('gslight', mu=487, sd=600)
    gsvpd = mc.Normal('gsvpd', mu=0.00095, sd=0.0003)
    lwp_low = mc.Normal('lwp_low', mu=7.7, sd=3)
    sperryk = mc.Normal('sperryk', mu=.335, sd=.22)
    
    lamb = mc.Normal('lambd', mu=2.5, sd=.2)
    ksat = mc.Normal('ksat', mu=.000571, sd=.0005)
    psiae = mc.Normal('psiae', mu=0.335, sd=0.05)  
    poros = mc.Normal('poros', mu=0.401, sd =.01)
    sd1 = mc.HalfNormal('sd1', sd=10)
    sd2 = mc.HalfNormal('sd2', sd=50)
    sd3 = mc.HalfNormal('sd3', sd=10)
    
    vlist = [gsmax, gsmin, optT,deltamaxT,deltaminT,gslight, gsvpd, lwp_low, 
                sperryk, lamb, ksat, psiae,poros]
    
    mod_swc, mod_tr, mod_lwp = theta(*vlist)
    means = T.concatenate([mod_swc, mod_tr, mod_lwp])
    
    sigma1 = 0.02
    sigma2 = 0.49
    sigma3 = 0.49

    if sigma1+sigma2+sigma3 != 1:
        sys.exit('ERROR Status 1: Sigmas do not sum to 1. Please change their values')

    p1 = T.ones_like(mod_swc)*sd1*sigma1
    p2 = T.ones_like(mod_tr)*sd2*sigma2
    p3 = T.ones_like(mod_lwp)*sd3*sigma3
    sd = T.concatenate((p1,p2, p3))
   

 

    observations = np.vstack((swcobs['VWC'].values[:,np.newaxis],
                                                trnsobs['Transp'].values[:,np.newaxis], lwpobs['Leaf_WP_MPa'].values[:,np.newaxis])).flatten()
                                                
    print type(observations)
    print type(means)
	
    obs = mc.Normal('obs', mu=means, sd=sd, 
                        observed=observations)
                        
	#Set Sampler Type.
    step_slice = mc.Slice([ sd1, sd2, sd3])
    step_mh1 = mc.Metropolis(vars=[lwp_low], S=np.ones(1)*5., tune=True, tune_interval=100)
    step_mh2 = mc.Metropolis(vars=[gsmin], S=np.ones(1)*0.01, tune=True, tune_interval=100)
    step_mh3 = mc.Metropolis(vars=[sperryk], S=np.ones(1)*0.4, tune=True, tune_interval=100)
    step_mh4 = mc.Metropolis(vars=[gslight], S=np.ones(1)*1000., tune=True, tune_interval=100)
    step_mh5 = mc.Metropolis(vars=[gsmax], S=np.ones(1)*0.1, tune=True, tune_interval=100)
    step_mh6 = mc.Metropolis(vars=[ksat], S=np.ones(1)*0.0005, tune=True, tune_interval=100)
    step_mh7 = mc.Metropolis(vars=[poros], S=np.ones(1)*0.01, tune=True, tune_interval=100)
    step_mh8 = mc.Metropolis(vars=[gsvpd], S=np.ones(1)*0.00005, tune=True, tune_interval=100)
    step_mh9 = mc.Metropolis(vars=[lamb], S=np.ones(1)*0.05, tune=True, tune_interval=100)
    step_mh10 = mc.Metropolis(vars=[psiae], S=np.ones(1)*0.01, tune=True, tune_interval=100)
    step_mh11 = mc.Metropolis(vars=[optT], S=np.ones(1)*2, tune=True, tune_interval=100)
    step_mh12 = mc.Metropolis(vars=[deltamaxT], S=np.ones(1)*3, tune=True, tune_interval=100)
    step_mh13 = mc.Metropolis(vars=[deltaminT], S=np.ones(1)*3, tune=True, tune_interval=100)

    backend = mc.backends.Text('trace_results')
    #oldtrace = mc.backends.text.load('trace_results')
    #start_pars = oldtrace.point(-1)

    trace = mc.sample(draws=5000, start=start_pars, init='ADVI', 
     njobs=1, step=[step_slice,
     step_mh1,
     step_mh2,
     step_mh3,
     step_mh4,
     step_mh5,
     step_mh6,
     step_mh7,
     step_mh8,
     step_mh9,
     step_mh10,
     step_mh11,
     step_mh12,
     step_mh13],
     trace=backend,
     )

