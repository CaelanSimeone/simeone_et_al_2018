#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 04:59:11 2017

@author: marco
"""
from pymc3.math import logsumexp
import numpy as np
import theano.tensor as tt

# Log likelihood of normal distribution
def logp_normal(mu, tau, value):
    # log probability of individual samples
    k = tau.shape[0]
    delta = lambda mu: value - mu
    return (-1 / 2.) * (k * tt.log(2 * np.pi) + tt.log(1./tt.nlinalg.det(tau)) +
                         (delta(mu).dot(tau) * delta(mu)).sum(axis=1))

# Log likelihood of Gaussian mixture distribution
class logp_gmix(object):
    def __init__(self, mus, pi, taus):
        self.mus = mus
        self.pi = pi
        self.taus = taus
    def __call__(self, value):    
        return self.logp_(value)
    
    def logp_(self, value):
        logps = [tt.log(self.pi[i]) + logp_normal(mu, tau, value)
                 for i, (mu, tau) in enumerate(zip(self.mus, self.taus))]
        ret = tt.sum(logsumexp(tt.stacklists(logps)[:, :np.shape(value)[0]], axis=0))
        ret_print = tt.printing.Print('Ret')(ret)
        return ret 
        #return logp_
