from osgeo import gdal, gdalnumeric, gdalconst
import numpy as np

def modifyConfigIni(src_fn, dest_fn, key, value):
	with open(src_fn) as fin, open(dest_fn, 'w') as fout:
		for line in fin:
			cfky = line.split("=")
			if str(cfky[0]).strip() in key:
				ind = value[key.index(str(cfky[0]).strip())]
				fout.write(str(cfky[0]).strip() + '=' + ind +'\n')
			else:
			    fout.write(line)

def modifyPCRasterMap(fn, value, dest=None):
    ds = gdal.Open(fn)
    m = np.array(ds.GetRasterBand(1).ReadAsArray())
    # transfers the no data values as nan
    m[m == ds.GetRasterBand(1).GetNoDataValue()] = np.nan
    # assigns the paramteer value to teh raster
    m[~np.isnan(m)] = value

    # write out the file
    if dest == None:
		dest = fn
    driver = gdal.GetDriverByName('PCRaster')
    dsOut = driver.Create(dest, ds.RasterXSize, ds.RasterYSize, 1, ds.GetRasterBand(1).DataType, ["PCRASTER_VALUESCALE=VS_SCALAR"])
    gdalnumeric.CopyDatasetInfo(ds,dsOut)
    
    bandOut=dsOut.GetRasterBand(1)
    gdalnumeric.BandWriteArray(bandOut, m)
    dsOut.FlushCache()
    
    ds = None
    m = None
    bandOut=None
    dsOut = None
    
