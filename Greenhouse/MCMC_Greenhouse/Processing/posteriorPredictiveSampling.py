import os
#os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu"
os.environ["OMP_NUM_THREADS"] = "1"
os.environ['MKL_THREADING_LAYER']='GNU'
import mapio
from customDist import logp_gmix
import pymc3 as mc
from subprocess import call, check_call
import writeVegetParamTab
import theano.tensor as T
from theano import as_op
from scipy import optimize 
#import scipy.stats as st
import numpy as np
import pandas as pd
#import os, glob
from shutil import copyfile, rmtree
#import uuid
#import sys, StringIO
import pickle
import csv

####### LOAD OBSERVATIONS
dfObsAll=pd.read_csv('../All_Data_Plants_5PM_Caelan.csv',delimiter=',')
dfObsAll=dfObsAll.set_index(pd.DatetimeIndex(dfObsAll['Measurement.Time']))
dfObsAll['Water_Potential'] = (dfObsAll['RMR_WP_mean_MPa']+dfObsAll['CP_WP_mean_MPa'])/2.
dfObsAll['VWC'] = (dfObsAll['RMR_VWC_top_mean_.']+dfObsAll['CP_VWC_top_mean_.'])/200.

dfData = pd.read_csv('../Master_Data_Caelan.csv')
dfData['Measurement.Time'] = dfData['Date'].map(str) +' '+ dfData['Time']+':00'
dfData = dfData.set_index(pd.DatetimeIndex(dfData['Measurement.Time']))

#Extract Rocky Mountain Race
dfData = dfData[dfData.Race == 'RMR']

#Delete Values noted as problematic by Gerard Sapes the Data Collector
dfData = dfData[dfData['Measurement.Time'] != '6/22/2017 13:58:00']
dfData = dfData[dfData['Measurement.Time'] != '7/7/2017 13:29:00']
dfData = dfData[dfData['Measurement.Time'] != '7/7/2017 13:22:00']
dfData = dfData[dfData['Measurement.Time'] != '7/7/2017 13:28:00']

#Delete Data With different Treatment
dfData = dfData[dfData['Treatment'] != 'C2' ]
dfData = dfData[dfData['Treatment'] != 'CFC1' ]
dfData = dfData[dfData['Treatment'] != 'CFC2' ]

#Reset Problematic PLC
dfData['Plant_PLC'] = np.where(dfData['Plant_PLC_%'] > 1, dfData['Plant_PLC_%'],0)
dfData['Stem_PLC'] = np.where(dfData['Stem_PLC_%'] > 1, dfData['Stem_PLC_%'],0)

#Remove night measurements.
dfData = dfData[dfData.index.hour > 2]
dfTransp = dfData.copy()
dfTransp['Transp'] = dfTransp['Trmmol']*1.802E-8
dfTransp['Transp'] = dfTransp['Transp']*8.64E7
dfTransp = dfTransp[dfTransp.Transp >= 0]

swcobs = 0
trnsobs = 0
lwpobs = 0
####################################333 Setup model
# Default vegetation parameter values

ks = [
    "SpeciesID",
    "NPP / GPPRatio",
    "gsmax(ms - 1)",
    "gsmin(ms - 1)",
    "CanopyQuantumEffic(gCJ - 1)",
    "MaxForestAge(yrs)",
    "OptimalTemp(C)",
    "MaxTemp(C)",
    "MinTemp(C)",
    "FoliageAllocCoef_a",
    "FoliageAllocCoef_b",
    "StemAllocCoef_a",
    "StemAllocCoef_b",
    "gs_light_coeff",
    "gs_vpd_coeff",
    "lwp_low(-m)",
    "lwp_high(-m)",
    "WiltingPnt",
    "SpecificLeafArea(m2g - 1)",
    "SpecificRootArea(m2kg - 1)",
    "Crown2StemDRat",
    "TreeShapeParam",
    "WoodDens(gCm - 2)",
    "Fhdmax",
    "Fhdmin",
    "LeafTurnoverRate(s - 1)",
    "MaxLeafTurnoverWaterStress(s - 1)",
    "LeafTurnoverWaterStressParam",
    "MaxLeafTurnoverTempStress(s - 1)",
    "LeafTurnoverTempStressParam",
    "ColdStressParam(degC)",
    "RootTurnoverRate(s - 1)",
    "MaxCanStorageParam(m)",
    "albedo",
    "emissivity",
    "KBeers",
    "CanopyWatEffic(gCm - 1)",
    "sperry_d_par(m)",
    "sperry_c_par(m)",
    "Sperry_Kp(ms - 1)",
    "gsr_param_a",
    "is_grass",
    "DeadGrassLeafTurnoverRate(s - 1)",
    "DeadGrassLeafTurnoverTempAdjustment(degC)"]
    
vals = [
    1, 0.47, 0.01, 0.000000001,  0.0018, 290, 15.45, 45.55, 5.787, 2.235, 0.0006, 3.30E+000, 6.00E-007, 1800, 5.09e-4,
    4.1, 0.00002324, 0.009, 0.009, 0.022, 0.25, 0.4, 220000, 15, 5, 8.56E-009, 1.80E-008, 0.2,
    1.80E-008, 0.2, 1, 5.34E-009, 0.0000624, 0.1, 0.95, 0.55, 800.0, 7.84, 1.49, 0.0073, 8, 0,
    0, 0]

vegPars = dict(zip(ks, vals))

key_pars = ['gsmax', 'gsmin','optT','deltamaxT','deltaminT', 'gslight', 'gsvpd', 'lwp_low', 'sperryk', 'lambd', 'ksat', 'psiae','poros', 'sd1', 'sd2', 'sd3']
init_pars = [0.057, 0.00366,15.6,30.017,9.983, 1812.4, 0.0002783379, 5.861, 0.153822, 2.458, 0.0001457, 0.25446,0.4, 5 ,5 ,5]

start_pars = dict(zip(key_pars, init_pars))

count = 1
@as_op(itypes=[T.dscalar]*13, otypes=[T.dvector, T.dvector, T.dvector])
def theta(gsmax,
	 gsmin,
	 optT,
	 deltamaxT,
	 deltaminT,
         gslight,
         gsvpd,
         lwp_low,
         sperryk,
         lamb,
         ksat, 
         psiae,
	 poros):
    global count
    count_str = str(count)
    if count_str[-2:0] == '00':
    	print "Number of model calls ", count
    count+=1
    
    # Do some checks on parameters    
    gsmax = max(gsmax, 0.00002)
    gsmin = min(gsmin,0.001) #Edit
    gsmin = max(gsmin,0.000000001)
    gsvpd = max(gsvpd, 0.0000001)
    lwp_low = max(lwp_low, 0.01)
    #sperryd = max(sperryd, 0.5)
    #sperryc = max(sperryc, 0.5)
    sperryk = max(sperryk, 0)
    ksat = max(ksat,0)
    lamb = max(lamb, 2)
    psiae = max(psiae, 0.1)
    poros = max(poros,.1)
    gslight = max(gslight,0)
    optT = max(optT,6)
    deltaminT = max(deltaminT,1)
    deltamaxT = max(deltamaxT,1) 
    #for f in glob.glob('../Results/*'):
    #    os.remove(f) # Makes sure we are not reading all data if the process fails
    minT = optT - deltaminT
    maxT = optT + deltamaxT

    vegPars['gsmax(ms - 1)'] = gsmax
    vegPars['gsmin(ms - 1)'] = gsmin
    vegPars['gs_light_coeff'] = gslight
    vegPars['gs_vpd_coeff'] = gsvpd
    vegPars['lwp_low(-m)'] = lwp_low
    vegPars['Sperry_Kp(ms - 1)'] = sperryk
    vegPars['OptimalTemp(C)'] = optT
    vegPars['MaxTemp(C)'] = maxT
    vegPars['MinTemp(C)'] = minT
 
    # Generate unique file and folder names
    configini = os.tempnam('../', '')+".ini"
    speciesParams = os.tempnam('../Spatial', 'tab')+".tab"
    
    lammap = os.tempnam('../Spatial', 'lam')+".map"
    keffmap = os.tempnam('../Spatial', 'keff')+".map"
    psimap = os.tempnam('../Spatial', 'psi')+".map"
    porosmap = os.tempnam('../Spatial','poros')+".map"    

    resfolder = os.tempnam('../', 'res')
    os.makedirs(resfolder)
 
    # write random config ini file
    copyfile(r"../config.ini", configini)
    
    # write random outputfolder file
    keys = ["Output_Folder", "Brooks_Corey_lambda",
             "Horiz_Hydraulic_Conductivity",
             "Air_entry_pressure",'porosity', "Species_Parameters"]
    values = [resfolder, lammap, keffmap, psimap, poros,speciesParams]
    mapio.modifyConfigIni(r"../config.ini", configini, keys, values)
    
    # Generate SpeciesParameter File
    writeVegetParamTab.writeVegetParamTab(speciesParams, [vegPars])
    
    
    # write pcraster maps
    mapio.modifyPCRasterMap('../Spatial/BClambda.map', lamb, dest=lammap)
    mapio.modifyPCRasterMap('../Spatial/Keff.map', ksat, dest=keffmap)
    mapio.modifyPCRasterMap('../Spatial/psi_ae.map', psiae, dest=psimap)
    mapio.modifyPCRasterMap('../Spatial/poros.map',poros,dest=porosmap)
    log = os.tempnam('./','')+".txt"
    #Call the actual model
    try:
    	with open(log, 'w') as f:
                    call(["ech2o", configini], stdout=f)
    except:
        print "Run failed, filling resfolder with dummy results\n"    

    
        copyfile('../dummyResults.tab', resfolder+'/LeafWaterPot[0].tab')
        copyfile('../dummyResults.tab', resfolder+'/Transpiration[0].tab')
        copyfile('../dummyResults.tab', resfolder+'/SoilMoisture[0].tab')
    
    # postrocess and return the data
    
	
    dfSimLWP = pd.read_table(resfolder+'/LeafWaterPot[0].tab', header=None, skiprows=6,names=['site1','nan'])


    dfSimTransp = pd.read_table(resfolder+'/Transpiration[0].tab', header=None, skiprows=6,names=['site1','nan'])

    dfSimVWC = pd.read_table(resfolder+'/SoilMoistureL1.tab', header=None, skiprows=6,names=['site1','nan'])

   
		
    ind = pd.date_range('2017-02-27 12:00:00', periods=dfSimVWC.shape[0], freq='H')


    dfSimLWP.index = ind
    dfSimTransp.index = ind
    dfSimVWC.index = ind

    dfSimTransp['site1'] = dfSimTransp['site1'] * 8.64E7
 

    #dfSimTransp*=86400000
    
    global dfObsAll
    global swcobs
    global trnsobs
    global lwpobs
    swcobs = dfSimVWC
    lwpobs = dfSimLWP
    trnsobs = dfSimTransp
    
    os.remove(configini)
    os.remove(lammap)
    os.remove(keffmap)
    os.remove(psimap)
    os.remove(porosmap)
    os.remove(speciesParams)
    [os.remove(os.path.join('../Spatial', f)) for f in os.listdir('../Spatial') if f.endswith(".xml")] # remove leftover .xml files
    rmtree(resfolder)
    os.remove(log)
    
    
    if count_str[-2:0] == '00':
    	print "Parameter values, ", gsmax,gsmin,optT,deltamaxT,deltaminT,gslight,gsvpd,\
			lwp_low,sperryk,lamb,ksat,psiae,poros
    
    #return dfSimLWP, dfSimTransp, swcobs['site1'].values
    
    
    return swcobs['site1'].values, trnsobs['site1'].values, lwpobs['site1'].values



model = mc.Model()
with model:

    # Parameter Priors 
    gsmax = mc.Normal('gsmax', mu=0.057, sd=0.1)
    gsmin = mc.Normal('gsmin', mu=0.00366, sd=0.01)
    optT = mc.Normal('optT', mu=15.6, sd=5)
    deltamaxT = mc.Normal('deltamaxT', mu =30, sd=10)
    deltaminT = mc.Normal('deltaminT', mu=10,sd=5)
    gslight = mc.Normal('gslight', mu=1812.4, sd=1000)
    gsvpd = mc.Normal('gsvpd', mu=0.0002783, sd=4E-3)
    lwp_low = mc.Normal('lwp_low', mu=5.876, sd=5)
    sperryk = mc.Normal('sperryk', mu=.1538, sd=.3)

    lamb = mc.Normal('lambd', mu=2.458, sd=.1)
    ksat = mc.Normal('ksat', mu=.000146, sd=2.7E-4)
    psiae = mc.Normal('psiae', mu=0.2545, sd=0.05)
    poros = mc.Normal('poros', mu=0.4, sd =.01)
    sd1 = mc.HalfNormal('sd1', sd=10)
    sd2 = mc.HalfNormal('sd2', sd=50)
    sd3 = mc.HalfNormal('sd3', sd=10)

    
    vlist = [gsmax,
         gsmin,
         optT,
         deltamaxT,
         deltaminT,
         gslight,
         gsvpd,
         lwp_low,
         sperryk,
         lamb,
         ksat,
         psiae,
         poros]
    
    mod_swc, mod_tr, mod_lwp = theta(*vlist)
    means = T.concatenate([mod_swc, mod_tr, mod_lwp])
    
    
    p1 = T.ones_like(mod_swc)*sd1
    p2 = T.ones_like(mod_tr)*sd2
    p3 = T.ones_like(mod_lwp)*sd3
    sd = T.concatenate((p1,p2, p3))

    observations = np.vstack((swcobs['site1'].values[:,np.newaxis],
                   trnsobs['site1'].values[:,np.newaxis], lwpobs['site1'].values[:,np.newaxis])).flatten()
                                 
    print(observations)
    print(means.tag.test_value)
	
    obs = mc.Normal('obs', mu=means, sd=sd, 
                        observed=observations)
                        


    trace = mc.backends.text.load('trace_results')
    trace = trace[4000:]    
    print "Starting postserior predictions"
    postPred = mc.sample_ppc(trace)
    #print type(postPred)
    #print postPred

    Pred = postPred['obs']   
    #print Pred 
    #with open('postPred2.txt', 'wb') as buff:
    #    pickle.dump(postPred, buff)
   

    dfData = pd.DataFrame(np.swapaxes(Pred,0,1))
    print dfData
    dfData.to_csv('postPred3.csv')
