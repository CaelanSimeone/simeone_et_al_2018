import os
import numpy as np
import shutil as shu

#os.system('rm -r /mnt/lurbira/Caelan/Darby_Final/Results/*')


wdir=os.getcwd()
years=np.array([2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015])

for i in years:
	os.mkdir('/mnt/lurbira/Caelan/Darby_Final/Results/'+str(i))

def spinupcopy(resultsdir,spatialdir,numb):

    shu.copyfile(resultsdir + '/SWE0000'+numb, spatialdir + '/SWE.map')
    shu.copyfile(resultsdir + '/SWC1_00'+numb, spatialdir + '/Soil_moisture_1.map')
    shu.copyfile(resultsdir + '/SWC2_00'+numb, spatialdir + '/Soil_moisture_3.map')
    shu.copyfile(resultsdir + '/SWC3_00'+numb, spatialdir + '/Soil_moisture_3.map')
    shu.copyfile(resultsdir + '/Ts00000'+numb, spatialdir + '/soiltemp.map')
    shu.copyfile(resultsdir + '/Q000000'+numb, spatialdir + '/streamflow.map')

    os.system('cat ' + resultsdir +'/lai[0].tab >>' +spatialdir+'/laiacum.txt')
    os.system('cat ' + resultsdir +'/NPP[0].tab >>' +spatialdir + '/NPPacum.txt')
    os.system('cat ' + resultsdir + '/SoilMoistureAv.tab >>' + spatialdir + '/SWCacum.txt')



ObsPerDay=8
config_name='/config'
for iRun in range(0,len(years)):
        print iRun
        pdir='/mnt/lurbira/Caelan/Darby_Final/Results/'+str(years[iRun]-1)
        rdir='/mnt/lurbira/Caelan/Darby_Final/Results/'+str(years[iRun])

	#os.system('cp ./Spatial/lai_'+str(years[iRun])+'.map ./Spatial/lai[0].map')

        fo=open(wdir+config_name+'.ini',"rw+")
        config_base=fo.readlines()

        print str(years[iRun])
        config_base[8]='Output_Folder='+'/mnt/lurbira/Caelan/Darby_Final/Results/' +str(years[iRun]) +'\n'
        config_base[60]='Precipitation=precip_'+str(years[iRun])+'.bin'+'\n'
        config_base[61]='AirTemperature=tavg_'+str(years[iRun])+'.bin'+'\n'
        config_base[62] = 'MaxAirTemp=tmax_' + str(years[iRun]) + '.bin'+'\n'
        config_base[63] = 'MinAirTemp=tmin_' + str(years[iRun]) + '.bin'+'\n'
        config_base[64]='RelativeHumidity=rhavg_'+str(years[iRun])+'.bin'+'\n'
        config_base[65] = 'WindSpeed=wind_' + str(years[iRun]) + '.bin'+'\n'
        config_base[66] = 'IncomingLongWave=lwrad_' + str(years[iRun]) + '.bin'+'\n'
        config_base[67] = 'IncomingShortWave=swrad_' + str(years[iRun]) + '.bin'+'\n'

        with open(wdir+config_name+'_'+str(years[iRun])+'.ini', 'w') as file:
            file.writelines(config_base)
            file.close
        fo.close()
        # End of modiifiying configuration file.

        #Run the model.

        os.system('export OMP_NUM_THREADS=32 && ech2o '+wdir+ config_name + '_'+str(years[iRun]) + '.ini')#This could cause an error if PEST and config files are not the same.
       # export OMP_NUM_THREADS=2
        resultsdir = rdir
        spatialdir = wdir + '/Spatial'
        
	maps=1
        if maps==1:
            spinupcopy(resultsdir,spatialdir,'2.920')

