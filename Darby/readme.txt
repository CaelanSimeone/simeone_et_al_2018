This folder contains data to run Ech2o for the distributed Bitterroot Portion of this study.

Run steps.
1. Move climate and spatial folders into this directory. Create a folder called results. 
2. Call ./Spinup_Multiyear.sh
3. Move to Scripts. Call ./Analyze_Maps.sh
