import os
import numpy as np

config = 'config_spinup.ini'
results_location = '/mnt/lurbira/Caelan/Darby_Final/Results/'

def spinup():
	os.system('rm ./Results/*')
	os.system(' export OMP_NUM_THREADS=32 && ech2o '+config)
	#os.system('cp -f ./Results/2002/root[0]2.920 ./Spatial/root[0].map')
	#os.system('cp -f ./Results/2002/p[0]0002.920 ./Spatial/p[0].map')
	#os.system('cp -f ./Results/2002/ntr[0]02.920 ./Spatial/ntr[0].map')
	#os.system('cp -f ./Results/2002/lai[0]02.920 ./Spatial/lai[0].map')
	#os.system('cp -f ./Results/2002/hgt[0]02.920 ./Spatial/hgt[0].map')
	#os.system('cp -f ./Results/2002/bas[0]02.920 ./Spatial/bas[0].map')
	#os.system('cp -f ./Results/2002/age[0]02.920 ./Spatial/age[0].map')

	os.system('cp -f '+results_location+'/SWE00002.920 ./Spatial/SWE.map')
	os.system('cp -f '+results_location+'/SWC1_002.920 ./Spatial/Soil_moisture_1.map')
	os.system('cp -f '+results_location+'/SWC2_002.920 ./Spatial/Soil_moisture_2.map')
	os.system('cp -f '+results_location+'/SWC3_002.920 ./Spatial/Soil_moisture_3.map')
	os.system('cp -f '+results_location+'/Ts000002.920 ./Spatial/soiltemp.map')
	os.system('cp -f '+results_location+'/Q0000002.920 ./Spatial/streamflow.map')

	#os.system('cat '+results_location+'/lai[0].tab >> ./Results/2002/laiacum.txt')
	#os.system('cat '+results_location+'/NPP[0].tab >> ./Results/2002/NPPacum.txt')
	#os.system('cat '+results_location+'/SoilMoistureAv.tab >> ./Results/2002/SWCacum.txt')


for i in np.arange(0,5):
	spinup()

#os.system('./runech2o.sh')
