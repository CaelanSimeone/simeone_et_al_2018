### This script extracts important maps and datasets from Ech2o results for the bitteroots.
#export OMP_NUM_THREADS=12

location='/mnt/lurbira/Caelan/Darby_Final/Results/'
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

cp ../Spatial/DEM.map ./Maps

'''Slope'''
cd Maps
pcrcalc 'Slope.map = slope(DEM.map)'
cd ../

'''Aspect'''
cd Maps
pcrcalc 'Aspect.map = aspect(DEM.map)'
cd ../

#Convergence
#This is done in Saga.

'''Precip Total'''
python Calculate_Precip.py $location

'''Sdown'''
python Calculate_Sdown.py $location

#'''Transpiraiton'''
python Calculate_Transpiration.py

#'''Evaporation'''
python Calculate_Evaporation.py

#'''Maximum Skin Temperature'''
python Calculate_Max_T.py

'''Max SWE'''
python Calculate_SWE.py $location

'''Air Temperature'''
python Calculate_Temperature.py $location

'''VPD''' #Mean Daily Peak VPD in the Summer. 
python Calculate_VPD.py $location

'''SWC''' #Mean Summer VWC in the rooting zone
python Calculate_VWC.py $location

#Soil water deficit

#Soil water potential

'''LWP''' 
#python Calculate_LWP.py
#PLC

'''DSI'''
python stress.py $location

python Map_list.py
ls Maps
