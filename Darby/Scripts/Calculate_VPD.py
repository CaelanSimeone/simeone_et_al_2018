# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]

def Return_Temp(year):
	''' Return df of Daily peak Tp'''
	location = results_folder+year
	Temp_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'Tp000000' or file_name == 'Tp000001' or file_name == 'Tp000002':
		Temp_pcr = pcr.readmap(location + '/' + f)
	        Temp = pcr.pcr2numpy(Temp_pcr, 9999)
	        Temp_flat=Temp.flatten()
	        Temp_df[str(f)[7:]]=Temp_flat
	Temp_df = Temp_df.sort_index(axis=1)
	Temp_df_daily = Temp_df[Temp_df.columns[5::8]]
	return Temp_df_daily


def Return_RH(year):
	''' Return df of Daily peak RH'''
	location = results_folder+year
	RH_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'RH000000' or file_name == 'RH000001' or file_name == 'RH000002':
	        RH_pcr = pcr.readmap(location + '/' + f)
	        RH = pcr.pcr2numpy(RH_pcr, 9999)
	        RH_flat=RH.flatten()
	        RH_df[str(f)[7:]]=RH_flat
	RH_df = RH_df.sort_index(axis=1)
	RH_df_daily = RH_df[RH_df.columns[5::8]]
	return RH_df_daily


Years = np.arange(2001,2016,1)
for year in Years:
	year=str(year)
	print year
	Temp_df = Return_Temp(year).transpose()
	print Temp_df
	ind = pd.date_range(year+'-01-01 00:00:00', periods=Temp_df.shape[0], freq='24H')
	Temp_df.index = ind
	
	
	RH_df = Return_RH(year).transpose()
	print RH_df
	ind = pd.date_range(year+'-01-01 00:00:00', periods=RH_df.shape[0], freq='24H')
	RH_df.index = ind

	### Full Year
	#RH = RH_df.transpose().as_matrix()
	#Temp = Temp_df.transpose().as_matrix()
	##Saturated vapor pressure
	#es = 0.6108 * np.exp(17.27 * Temp / (Temp + 237.3))
	#ea = RH * es 
	#VPD = ea - es
	#Mean_VPD = np.mean(VPD,axis=1)
	#Mean_VPD=Mean_VPD.reshape((468,429))
	#Mean_VPD_pcr = pcr.numpy2pcr(Scalar, Mean_VPD, 9999)
	#pcr.report(Mean_VPD_pcr, 'VPD_VWC/Yearly_VPD'+year+'.map')

	## Summer
	RH_df = RH_df.loc[RH_df.index.month.isin([7,8,9])]
	Temp_df = Temp_df.loc[Temp_df.index.month.isin([7,8,9])]
	RH = RH_df.transpose().as_matrix()
	Temp = Temp_df.transpose().as_matrix()
	#Saturated vapor pressure
	es = 0.6108 * np.exp(17.27 * Temp / (Temp + 237.3))
	ea = RH * es 
	VPD = ea - es
	Mean_VPD = np.mean(VPD,axis=1)
	Mean_VPD=Mean_VPD.reshape((468,429))
	Mean_VPD_pcr = pcr.numpy2pcr(Scalar, Mean_VPD, 9999)
	pcr.report(Mean_VPD_pcr, 'Maps/VPD'+year+'.map')

#	Mean_Temp = np.mean(Temp,axis=1)
#	Mean_Temp=Mean_Temp.reshape((468,429))
#	Mean_Temp_pcr = pcr.numpy2pcr(Scalar, Mean_Temp, 9999)
#	pcr.report(Mean_Temp_pcr, 'Maps/Text_Temp'+year+'.map')
#
#	Mean_RH = np.mean(RH,axis=1)
#	Mean_RH=Mean_RH.reshape((468,429))
#	Mean_RH_pcr = pcr.numpy2pcr(Scalar, Mean_RH, 9999)
#	pcr.report(Mean_RH_pcr, 'Maps/Text_RH'+year+'.map')
	### August
	#RH_df = RH_df.loc[RH_df.index.month.isin([8])]
	#Temp_df = Temp_df.loc[Temp_df.index.month.isin([8])]
	#Temp = Temp_df.transpose().as_matrix()
	#RH = RH_df.transpose().as_matrix()
	#
	##Saturated vapor pressure
	#es = 0.6108 * np.exp(17.27 * Temp / (Temp + 237.3))
	#ea = RH * es 
	#VPD = ea - es
	#
	#Mean_VPD = np.mean(VPD,axis=1)
	#Mean_VPD=Mean_VPD.reshape((468,429))
	#Mean_VPD_pcr = pcr.numpy2pcr(Scalar, Mean_VPD, 9999)
	#pcr.report(Mean_VPD_pcr, 'VPD_VWC/August_VPD'+year+'.map')











