import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import matplotlib.dates as mdates

fig = plt.figure(figsize = (10,10))
ax = fig.add_subplot(2, 1, 1)





dfData = pd.read_table('/mnt/lurbira/Caelan/Darby_Final/Results_high_gsmin/2002/Streamflow.tab',header=None, skiprows=11)
ind = pd.date_range('2002-01-01 00:00:00', periods=dfData.shape[0], freq='3H')
dfData.index = ind
dfData.drop(np.arange(0, 6,1),axis=1,inplace=True)
dfData.drop(np.arange(7, 10,1),axis=1,inplace=True)
dfData = dfData.resample('24H').mean()
dfData2002 = dfData.copy()
ax.plot(dfData,alpha=1,color='red',label='Modeled')
#
dfData = pd.read_table('/mnt/lurbira/Caelan/Darby_Final/Results_high_gsmin/2003/Streamflow.tab',header=None, skiprows=11)
ind = pd.date_range('2003-01-01 00:00:00', periods=dfData.shape[0], freq='3H')
dfData.index = ind
dfData.drop(np.arange(0, 6,1),axis=1,inplace=True)
dfData.drop(np.arange(7, 10,1),axis=1,inplace=True)
dfData = dfData.resample('24H').mean()
dfData2003 = dfData.copy()
ax.plot(dfData,alpha=1,color='red',label='_nolegend_')

dfData = pd.read_table('/mnt/lurbira/Caelan/Darby_Final/Results_high_gsmin/2004/Streamflow.tab',header=None, skiprows=11)
ind = pd.date_range('2004-01-01 00:00:00', periods=dfData.shape[0], freq='3H')
dfData.index = ind
dfData.drop(np.arange(0, 6,1),axis=1,inplace=True)
dfData.drop(np.arange(7, 10,1),axis=1,inplace=True)
dfData = dfData.resample('24H').mean()
dfData2004 = dfData.copy()
ax.plot(dfData,alpha=1,color='red',label='_nolegend_')

dfModeled = pd.concat([dfData2002,dfData2003,dfData2004])
print dfModeled

USGS = pd.read_csv('gauge.csv',delimiter='\t',skiprows=np.arange(0,31))
USGS.drop([0],inplace=True)
USGS = USGS['81980_00060_00003']
USGS = USGS.astype('float',inplace=True)
ind = pd.date_range('2001-10-01 00:00:00', periods=USGS.shape[0], freq='24H')
USGS.index = ind 
USGS = USGS[USGS.index.year.isin([2002,2003,2004])]
USGS = USGS /35.3
print USGS

ax.plot(USGS,label='Observed')

dfModeled['USGS'] = USGS
## Nash-Sutcliffe Efficiency. Look at wiki for beta.
Mean = dfModeled['USGS'].mean()
Numerator = ((dfModeled[6] - dfModeled['USGS'])**2).sum()
Denominator = ((dfModeled['USGS'] - Mean)**2).sum()
Nash_Sutcliffe = 1 - (Numerator/Denominator)
#plt.plot([], [], ' ', label='Nash-Sutcliffe = '+str(round(Nash_Sutcliffe,3)))

myFmt = mdates.DateFormatter('%m/%Y')
ax.xaxis.set_major_formatter(myFmt)

plt.xlabel('Date (month/year)')
plt.ylabel('Discharge ($m^3 s^{-1}$)')
#ax.set_yscale('log')
ax.legend(loc=0)
#plt.tight_layout()
plt.savefig('Hydrograph.png')
plt.show()

