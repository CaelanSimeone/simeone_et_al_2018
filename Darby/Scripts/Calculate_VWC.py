# Calculate SWC

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]
def Return_SWC_1(year):
	''' Return df of Daily peak SWC_1'''
	location = results_folder+year
	SWC_1_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'SWC1_000' or file_name == 'SWC1_001' or file_name == 'SWC1_002':
		SWC_1_pcr = pcr.readmap(location + '/' + f)
	        SWC_1 = pcr.pcr2numpy(SWC_1_pcr, 9999)
	        SWC_1_flat=SWC_1.flatten()
	        SWC_1_df[str(f)[7:]]=SWC_1_flat
	SWC_1_df = SWC_1_df.sort_index(axis=1)
	#SWC_1_df_daily = SWC_1_df[SWC_1_df.columns[4::8]]
	return SWC_1_df

def Return_SWC_2(year):
	''' Return df of Daily peak SWC_2'''
	location = results_folder+year
	SWC_2_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'SWC2_000' or file_name == 'SWC2_001' or file_name == 'SWC2_002':
		SWC_2_pcr = pcr.readmap(location + '/' + f)
	        SWC_2 = pcr.pcr2numpy(SWC_2_pcr, 9999)
	        SWC_2_flat=SWC_2.flatten()
	        SWC_2_df[str(f)[7:]]=SWC_2_flat
	SWC_2_df = SWC_2_df.sort_index(axis=1)
	#SWC_2_df = SWC_2_df[SWC_2_df.columns[4::8]]
	return SWC_2_df
Years = np.arange(2001,2016,1)
for year in Years:
	year = str(year)
	SWC_1_df = Return_SWC_1(year).transpose()	
	ind = pd.date_range(year+'-01-01 00:00:00', periods=SWC_1_df.shape[0], freq='3H')
	SWC_1_df.index = ind
	
	SWC_2_df = Return_SWC_2(year).transpose()	
	ind = pd.date_range(year+'-01-01 00:00:00', periods=SWC_2_df.shape[0], freq='3H')
	SWC_2_df.index = ind

	## Summer
	SWC_1_df = SWC_1_df.loc[SWC_1_df.index.month.isin([7,8,9])]
	SWC_1 = SWC_1_df.transpose().as_matrix()	
	Mean_SWC_1 = np.mean(SWC_1,axis=1)

	SWC_2_df = SWC_2_df.loc[SWC_2_df.index.month.isin([7,8,9])]
	SWC_2 = SWC_2_df.transpose().as_matrix()	
	Mean_SWC_2 = np.mean(SWC_2,axis=1)
	
	Mean_RootingVWC = (Mean_SWC_1 + 3*Mean_SWC_2)/4
	Mean_RootingVWC=Mean_RootingVWC.reshape((468,429))
	Mean_RootingVWC_pcr = pcr.numpy2pcr(Scalar, Mean_RootingVWC, 9999)
	pcr.report(Mean_RootingVWC_pcr, 'Maps/VWC'+year+'.map')

