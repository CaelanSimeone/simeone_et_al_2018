# Calculate SWC

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]
def Return_Transpiraiton(year):
	''' Return df of Daily peak Transpiraiton'''
	location = results_folder+year
	Transpiraiton_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'SWC1_000' or file_name == 'SWC1_001' or file_name == 'SWC1_002':
		Transpiraiton_pcr = pcr.readmap(location + '/' + f)
	        Transpiraiton = pcr.pcr2numpy(Transpiraiton_pcr, 9999)
	        Transpiraiton_flat=Transpiraiton.flatten()
	        Transpiraiton_df[str(f)[7:]]=Transpiraiton_flat
	Transpiraiton_df = Transpiraiton_df.sort_index(axis=1)
	Transpiraiton_df_daily = Transpiraiton_df[Transpiraiton_df.columns[4::8]]
	return Transpiraiton_df_daily


for year in np.arange(2001,2016):
	year = str(year)
	Transpiraiton_df = Return_Transpiraiton(year)
	Transpiraiton = Transpiraiton_df.as_matrix()	
	Mean_Transpiraiton = np.sum(Transpiraiton,axis=1)
	Mean_Transpiraiton=Mean_Transpiraiton.reshape((468,429))
	Mean_Transpiraiton_pcr = pcr.numpy2pcr(Scalar, Mean_Transpiraiton, 9999)
	pcr.report(Mean_Transpiraiton_pcr, 'Maps/Mean_Transpiraiton'+year+'.map')











