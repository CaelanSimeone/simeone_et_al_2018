# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]

def Return_Precip(year):
	''' Return df of Daily peak Precip'''
	location = results_folder+year
	Precip_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'Pp000000' or file_name == 'Pp000001' or file_name == 'Pp000002':
		Precip_pcr = pcr.readmap(location + '/' + f)
	        Precip = pcr.pcr2numpy(Precip_pcr, 9999)
	        Precip_flat=Precip.flatten()
	        Precip_df[str(f)[7:]]=Precip_flat
	Precip_df = Precip_df.sort_index(axis=1)
	return Precip_df


for year in np.arange(2001,2016):
	year = str(year)
	Precip_df = Return_Precip(year)
	Precip = Precip_df.as_matrix()
	Precip = np.sum(Precip,axis=1)
	
	Precip=Precip.reshape((468,429))
	Precip_pcr = pcr.numpy2pcr(Scalar, Precip, 9999)
	pcr.report(Precip_pcr, 'Maps/Precip'+year+'.map')














