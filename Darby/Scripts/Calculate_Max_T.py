# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]

def Return_Tskin(year):
	''' Return df of Daily peak Tskin'''
	location = results_folder+year
	Tskin_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'Tskin000' or file_name == 'Tskin001' or file_name == 'Tskin002':
		Tskin_pcr = pcr.readmap(location + '/' + f)
	        Tskin = pcr.pcr2numpy(Tskin_pcr, 9999)
	        Tskin_flat=Tskin.flatten()
	        Tskin_df[str(f)[7:]]=Tskin_flat
	Tskin_df = Tskin_df.sort_index(axis=1)
	Tskin_df_daily = Tskin_df[Tskin_df.columns[4::8]]
	return Tskin_df_daily


for year in np.arange(2001,2016):
	year = str(year)
	Tskin_df = Return_Tskin(year)
	Tskin = Tskin_df.as_matrix()
	Max_Tskin = np.max(Tskin,axis=1)
	
	Max_Tskin=Max_Tskin.reshape((468,429))
	Max_Tskin_pcr = pcr.numpy2pcr(Scalar, Max_Tskin, 9999)
	pcr.report(Max_Tskin_pcr, 'Maps/Max_Tskin'+year+'.map')














