# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
import math

results_folder = sys.argv[1]

def Return_LWP(year):
	''' Return df of Daily peak LWP'''
	location = results_folder+year
	LWP_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'LWP[0]00' or file_name == 'LWP[0]01' or file_name == 'LWP[0]02':
		LWP_pcr = pcr.readmap(location + '/' + f)
	        LWP = pcr.pcr2numpy(LWP_pcr, 9999)
	        LWP_flat=LWP.flatten()
	        LWP_df[str(f)[7:]]=LWP_flat
	LWP_df = LWP_df.sort_index(axis=1)
	#LWP_df_daily = LWP_df[LWP_df.columns[4::8]]
	return LWP_df


for year in np.arange(2001,2016):
	year = str(year)
	LWP_df = Return_LWP(year).transpose()	
	print LWP_df	
	ind = pd.date_range(year+'-01-01 00:00:00', periods=LWP_df.shape[0], freq='3H')
	LWP_df.index = ind

	LWP_predawn = LWP_df.resample('24H').max()
	LWP_midday = LWP_df.resample('24H').min()


	LWP_predawn = LWP_predawn.transpose().as_matrix()
	LWP_midday = LWP_midday.transpose().as_matrix()

	sperryb = 7.84 #PLC= 1 - e^(-(-Psi/b))^c  7.84    1.49
	sperryc = 1.49  
	PLC = 1.0 - math.e ** (-((-LWP_midday / sperryb) ** sperryc))     
	
	Mean_predawn = np.mean(LWP_predawn,axis=1)
	Min_predawn = np.min(LWP_predawn,axis=1)
	Mean_midday = np.mean(LWP_midday,axis=1)
	Min_midday = np.min(LWP_midday,axis=1)
	Max_PLC = np.max(PLC,axis=1)
	
	Mean_predawn=Mean_predawn.reshape((468,429))
	Min_predawn=Min_predawn.reshape((468,429))
	Mean_midday=Mean_midday.reshape((468,429))
	Min_midday=Min_midday.reshape((468,429))
	Max_PLC = Max_PLC.reshape((468,429))	

	Mean_predawn_pcr = pcr.numpy2pcr(Scalar, Mean_predawn, 9999)
	pcr.report(Mean_predawn_pcr, 'Maps/Mean_predawn'+year+'.map')

	Min_predawn_pcr = pcr.numpy2pcr(Scalar, Min_predawn, 9999)
	pcr.report(Min_predawn_pcr, 'Maps/Min_predawn'+year+'.map')

	Mean_midday_pcr = pcr.numpy2pcr(Scalar, Mean_midday, 9999)
	pcr.report(Mean_midday_pcr, 'Maps/Mean_midday'+year+'.map')

	Min_midday_pcr = pcr.numpy2pcr(Scalar, Min_midday, 9999)
	pcr.report(Min_midday_pcr, 'Maps/Min_midday'+year+'.map')
	
	Max_PLC_pcr = pcr.numpy2pcr(Scalar, Max_PLC, 9999)
	pcr.report(Max_PLC_pcr, 'Maps/Max_PLC'+year+'.map')








