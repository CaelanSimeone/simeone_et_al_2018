import pcraster as pcr
import numpy as np
import os as os
from pcraster import *
import math as math
import shutil as sh
import collections as cl
import Utility as epp
import pandas as pd
import sys

sperryb = 7.84 #PLC= 1 - e^(-(-Psi/b))^c  7.84    1.49
sperryc = 1.49  
Tsmin = 5 #The soil temperature threshold for plant growth (Beedlow 2013)
PLCinit = 0.263
PLCcrit = 0.548
Por_k = 0.166
Por_r = .5

results= sys.argv[1] 

map_pcr = pcr.readmap('DEM.map')
Map = pcr.pcr2numpy(map_pcr, 9999) 

def run_stress(PLCinit,PLCcrit,sperryb,sperryc,porporatok,Tsmin,resultslocation,year,r,Map):
	Tseasmap,winter_df =epp.TseasonCalculation(resultslocation,Tsmin,year,Map)
	
	PLC_df,PLC_df_rolling=epp.Pull_trans(resultslocation,PLCinit,PLCcrit,sperryb,sperryc,year,Map)


	PLC_df = pd.DataFrame(PLC_df.as_matrix() * winter_df.as_matrix()).transpose()
	PLC_df_rolling = pd.DataFrame(PLC_df_rolling.as_matrix() * winter_df.as_matrix()).transpose()

	
	weightedave_array,PLC_df_boolean=epp.weightedavePLC(PLC_df,PLCinit,PLCcrit,year,Map,PLC_df_rolling)
	epp.Calculate_dynamic_Stress_PLC(PLC_df,PLC_df_boolean,
		weightedave_array,Tseasmap,porporatok,sperryb,
		sperryc,PLCinit,str(year),r,Map,PLC_df_rolling)
Years = np.arange(2001,2016,1)
for i in Years:
	resultslocation = results + str(i)

	run_stress(PLCinit,PLCcrit,sperryb,sperryc,Por_k,Tsmin,resultslocation,i,Por_r,Map)	

