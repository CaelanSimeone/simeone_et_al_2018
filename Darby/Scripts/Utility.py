from __future__ import division
import pcraster as pcr
import numpy as np
import os as os
from pcraster import *
import math as math
import shutil as sh
import collections as cl
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from tqdm import tqdm

def Pull_trans(location, PLCinit, PLCcrit, sperryb, sperryc,year,Map):
	''' This function pulls in LWP data from maps and converts each map to PLC using the sperry framework. The new PLC maps are the droped into the ./Results/PLC folder. A later update will use a dataframe to conserve map values rather than transfer them to a folder to reduce computational power. Additionally PLC will eventually have to be normalized against PLCcrit.'''
       	Shape = Map.shape
	Length = Shape[0] * Shape[1]
	print '1/4 Pull and Transfer Maps'
	PLC_df=pd.DataFrame(index=np.arange(Length))
        for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'LWP[0]00' or file_name == 'LWP[0]01' or file_name == 'LWP[0]02':
		phi_pcr = pcr.readmap(location + '/' + f)
                Phi = pcr.pcr2numpy(phi_pcr, 9999) 
		PLC = 1.0 - math.e ** (-((-Phi / sperryb) ** sperryc))     
		PLC_flat=PLC.flatten()
		PLC_df[str(f)[7:]]=PLC_flat
	PLC_df = PLC_df.sort_index(axis=1).transpose()
      	#plt.plot(PLC_df.isnull().sum(axis=1))
	#plt.show()
	ind = pd.date_range(str(year)+'-01-01 00:00:00', periods=PLC_df.shape[0], freq='3H')
        PLC_df.index = ind
        PLC_df = PLC_df.resample('24H').max()
	PLC_rolling = PLC_df.rolling(3).mean()	
	return PLC_df,PLC_rolling


def weightedavePLC(PLC_df,PLC_init,PLCcrit,year,Map,PLC_df_rolling):
	'''This function inputs the pandas df that is output from the above equation and calculates the weighted average of each of the timesteps for the whole year. This is the transfer from Porporatos static stress to mean static stress. '''
	print '3/4 Weighted Average'
       	
	Shape = Map.shape
	Length = Shape[0] * Shape[1]
	weightedaverage=[]
	PLC_df_boolean=pd.DataFrame(index=PLC_df.index)
	for column in PLC_df:
		PLC_df_boolean[column]=np.where(PLC_df[column]>=PLC_init,1.0,0.0)
	PLC_df_boolean_rolling=pd.DataFrame(index=PLC_df.index)
	for column in PLC_df_rolling:
		PLC_df_boolean_rolling[column]=np.where(PLC_df_rolling[column]>=PLC_init,1.0,0.0)
	
	Static_stress = (PLC_init - PLC_df)/(PLC_init-PLCcrit)
	Static_stress = np.where(Static_stress >= 0,Static_stress,0)
	Static_stress = np.where(Static_stress <= 1,Static_stress,1)
	Static_sum=Static_stress.sum(axis=1)#.values
	PLC_sum_boolean=PLC_df_boolean.sum(axis=1).values
	weightedaverage = (Static_sum/PLC_sum_boolean)

	weightedaverage = np.array([9999 if x is None or np.isnan(x) else x for x in weightedaverage])
	weightedaverage=weightedaverage.reshape(Shape)
	weightedaverage_pcr = pcr.numpy2pcr(Scalar, weightedaverage, 9999)
	#pcr.report(weightedaverage_pcr, 'WeightedAverageStress_'+str(year)+'.map')
	
	Static_sum=Static_sum.reshape(Shape)
	Static_sum_pcr = pcr.numpy2pcr(Scalar, Static_sum, 9999)
	#pcr.report(Static_sum_pcr, 'TotalStaticStress_'+str(year)+'.map')
	
	PLC_sum_boolean=PLC_sum_boolean.reshape(Shape)
	PLC_sum_boolean_pcr = pcr.numpy2pcr(Scalar, PLC_sum_boolean, 9999)
	#pcr.report(PLC_sum_boolean_pcr, 'DroughtDays_'+str(year)+'.map')

	return weightedaverage, PLC_df_boolean_rolling

def Calculate_dynamic_Stress_PLC(PLC_df,PLC_df_boolean_daily,weightedavearray,Tseas,k,sperryb,sperryc,PLCinit,year,r,Map,PLC_df_rolling):
	'''This function calculates the dynamic stress based on the mean weighted static stress. '''
	print '4/4 Calculate DSI'
       	Shape = Map.shape
	Length = Shape[0] * Shape[1]
	#This slices the entire dataframe and only looks at values that are taken at midday.
	#PLC_df_boolean_daily=PLC_df_boolean[PLC_df_boolean.columns[4::8]]
	#T is a series of the total number of days on which there is stress.
	t=PLC_df_boolean_daily.sum(axis=1).values
	
	#This series of lines counts the number of droughts there are during the period by measuring the number of times that there is a midday transition from no drought induced PLC to drought induced PLC.
	
	Nlist=[]
	Narray=PLC_df_rolling.as_matrix()
	for i in np.arange(0,Narray.shape[0]): 
		ncount=0.000000001
		for j in np.arange(1,Narray.shape[1]):
			#if Narray[i,j+1]-Narray[i,j]>0.5:
			if Narray[i,j] > PLCinit and Narray[i,j-1] <PLCinit:
				ncount+=1.	
		Nlist.append(ncount)	

	#This section calculates the average duration of droughtes by deviding the total amount of drought time by the number of drought periods.
	Narray=np.array(Nlist)
        
	Frequency=Narray.reshape(Shape)
        frequency = pcr.numpy2pcr(Scalar, Frequency, 0)
        #pcr.report(frequency, 'StressFrequency_'+str(year)+'.map')
	
	tarray=np.asarray(t)
	Tarray=(tarray/Narray)
	
	print Tarray
	
	Duration=Tarray.reshape(Shape)
        duration = pcr.numpy2pcr(Scalar, Duration, 0)
        #pcr.report(duration, 'AverageDuration_'+str(year)+'.map')



	#This is the estimated length of the growing season. This needs to be upgraded. 
	Tseas=Tseas.flatten()

	weightedavearray=weightedavearray.flatten()
	
    	Dynamic_Stress= (weightedavearray*Tarray/(k*Tseas)) ** (Narray**(-r))
	
	Dynamic_Stress = np.array([-9999 if x is None or np.isnan(x) else x for x in Dynamic_Stress])
	Dynamic_Stress = np.where(Dynamic_Stress < 1,Dynamic_Stress,1)
	
	Dynmap=Dynamic_Stress.reshape(Shape)	
	dynmap = pcr.numpy2pcr(Scalar, Dynmap, -9999)
    	pcr.report(dynmap, './Maps/Dynamicstress'+str(year)+'.map')
	#aguila(dynmap)
    ## I need a valid way to put a cap on the hs map at 1.

def TseasonCalculation(location,Tsmin,year,Map):
	'''This function estimates the season length, based on the amount of time that the soil temperature is above threshold Tsmin '''
	print '2/4 Calculate Season Lenght'
       	Shape = Map.shape
	Length = Shape[0] * Shape[1]
	Ts_df=pd.DataFrame(index=np.arange(Length))
        Ts_df_boolean=pd.DataFrame(index=np.arange(Length))
	
	for f in tqdm(os.listdir(location)):
            file_name, file_ext = os.path.splitext(f)
            if file_name == 'Ts000000' or file_name == 'Ts000001' or file_name == 'Ts000002':
                ts = pcr.readmap(location + '/' + f)
                Ts = pcr.pcr2numpy(ts, 9999)
	       	Ts_flat=Ts.flatten()
                Ts_df[str(f)[7:]]=Ts_flat
        Ts_df = Ts_df.sort_index(axis=1)
	
	
	for column in Ts_df:
                Ts_df_boolean[column]=np.where(Ts_df[column]>=Tsmin,1.0,0.0)

	Ts_df_boolean = Ts_df_boolean.transpose()
	
	ind = pd.date_range(str(year)+'-01-01 00:00:00', periods=Ts_df_boolean.shape[0], freq='3H')
        Ts_df_boolean.index = ind
        Ts_df_boolean = Ts_df_boolean.resample('24H').max()
	

	Ts_sum_boolean=Ts_df_boolean.transpose().sum(axis=1).values
	#Ts_sum_boolean = Ts_sum_boolean/2 + (365/2)	
	Tseas=Ts_sum_boolean.reshape(Shape)
        tseas = pcr.numpy2pcr(Scalar, Tseas, 365)
	pcr.report(tseas, './Maps/Tseason_'+str(year)+'.map')
	#aguila(tseas)
	return Ts_sum_boolean, Ts_df_boolean
		
	



