# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
import sys

results_folder = sys.argv[1]

def Return_Sdown(year):
	''' Return df of Daily peak Sdown'''
	location = results_folder+year
	Sdown_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'Sdown000' or file_name == 'Sdown001' or file_name == 'Sdown002':
		Sdown_pcr = pcr.readmap(location + '/' + f)
	        Sdown = pcr.pcr2numpy(Sdown_pcr, 9999)
	        Sdown_flat=Sdown.flatten()
	        Sdown_df[str(f)[7:]]=Sdown_flat
	Sdown_df = Sdown_df.sort_index(axis=1)
	return Sdown_df


for year in np.arange(2001,2016):
	year = str(year)
	Sdown_df = Return_Sdown(year)
	Sdown = Sdown_df.as_matrix()
	Sdown = np.mean(Sdown,axis=1)
	
	Sdown=Sdown.reshape((468,429))
	Sdown_pcr = pcr.numpy2pcr(Scalar, Sdown, 9999)
	pcr.report(Sdown_pcr, 'Maps/Sdown'+year+'.map')














