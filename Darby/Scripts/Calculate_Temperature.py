# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]

def Return_Temp(year):
	''' Return df of Daily peak Temp'''
	location = results_folder+year
	Temp_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'Tp000000' or file_name == 'Tp000001' or file_name == 'Tp000002':
		Temp_pcr = pcr.readmap(location + '/' + f)
	        Temp = pcr.pcr2numpy(Temp_pcr, 9999)
	        Temp_flat=Temp.flatten()
	        Temp_df[str(f)[7:]]=Temp_flat
	Temp_df = Temp_df.sort_index(axis=1)
	return Temp_df


for year in np.arange(2001,2016):
	year = str(year)
	Temp_df = Return_Temp(year).transpose()
	ind = pd.date_range(year+'-01-01 00:00:00', periods=Temp_df.shape[0], freq='3H')
	Temp_df.index = ind
	Temp_df = Temp_df.resample('24H').max()
	Temp = Temp_df.transpose().as_matrix()
	Temp = np.mean(Temp,axis=1)
	
	Temp=Temp.reshape((468,429))
	Temp_pcr = pcr.numpy2pcr(Scalar, Temp, 9999)
	pcr.report(Temp_pcr, 'Maps/Temp'+year+'.map')














