# Calculate VPD

import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm

results_folder = sys.argv[1]

def Return_SWE(year):
	''' Return df of Daily peak SWE'''
	location = results_folder+year
	SWE_df=pd.DataFrame(index=np.arange(200772))
	for f in tqdm(os.listdir(location)):
	    file_name, file_ext = os.path.splitext(f)
	    if file_name == 'SWE00000' or file_name == 'SWE00001' or file_name == 'SWE00002':
		SWE_pcr = pcr.readmap(location + '/' + f)
	        SWE = pcr.pcr2numpy(SWE_pcr, 9999)
	        SWE_flat=SWE.flatten()
	        SWE_df[str(f)[7:]]=SWE_flat
	SWE_df = SWE_df.sort_index(axis=1)
	return SWE_df


for year in np.arange(2001,2016):
	year = str(year)
	SWE_df = Return_SWE(year)
	SWE = SWE_df.as_matrix()
	SWE = np.max(SWE,axis=1)
	
	SWE=SWE.reshape((468,429))
	SWE_pcr = pcr.numpy2pcr(Scalar, SWE, 9999)
	pcr.report(SWE_pcr, 'Maps/SWE'+year+'.map')














