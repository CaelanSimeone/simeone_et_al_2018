Welcome to the data repository for Simeone et al. 

Data and scripts to reproduce results for this paper are located in this repository. 
This repo is formatted as a git repo so all big files are stored in a data_repository folder, 
and must be moved to the proper folders with the execute.py script in order to execute futher files.
Large data files cannot be stored within the online repository. Please contact Marco.Maneta@mso.umt.edu or 
caelansimeone@gmail.com for access to the data files. 

Inside each folder, there should be a readme.txt file and an execute.py file. 
he exceptions to this are the Tools, Figures, and Data_Repository folders.

Due to its constantly changing nature, the Mapping Bitterroot folder has the newest version with full maps 
located in Data_Repository/New_Maps.

Figures were polished in Inkscape. All necessary files for this are in Data_Repository/Inkscape.

Any additional questions and concerns can be directed to Caelan Simeone at caelansimeone@gmail.com.

Cheers,

Caelan
5-18-2018

 
