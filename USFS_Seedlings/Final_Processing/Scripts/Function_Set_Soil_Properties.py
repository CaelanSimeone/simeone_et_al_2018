import pandas as pd
import os
from tqdm import tqdm

def Set_Soil_Properties(folders,scalar,directory):
	dfSoil = pd.read_csv('../Data/seedling_soils.csv')
	dfSoil.index = dfSoil['ID']

	cwd = os.getcwd()
	for index,row in tqdm(dfSoil.iterrows()):
		if index in folders:
	        	pass
		else:
			continue
	       	if directory == '9cell_Satparams':
			Kscale = 400
		else:
			Kscale = 1 
		os.chdir('../'+directory+'/'+index+'/Spatial')
	        os.system("pcrcalc 'BClambda.map = unit.map * " + str((1/row['bc'])) + "' >/dev/null 2>&1")
	        os.system("pcrcalc 'Keff.map = unit.map * " + str(Kscale*row['ksat']/1000000) + "' >/dev/null 2>&1")
	        os.system("pcrcalc 'poros.map = unit.map * " + str(row['por']) + "' >/dev/null 2>&1")
	        os.system("pcrcalc 'depth.map = unit.map * " + str(row['depth']/100) + "' >/dev/null 2>&1")
	        os.system("pcrcalc 'albedo.map = unit.map * " + str(row['alb']) + "' >/dev/null 2>&1")
	        os.chdir(cwd)
