import pandas as pd
import os
from tqdm import tqdm

def Duplicates(folders,directory):
	dfObs = pd.read_csv('../Data/'+directory+'.csv')	
	for fn in tqdm(folders):
		site = fn [1:]
		test = dfObs.loc[dfObs['NewSUID'] == site]
		if len(test) == 2:
			df = test.iloc[1:]
			os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_2')
	       		dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_2')
	       		#print dfObs.iloc[df.index.values]['NewSUID'].values
	        if len(test) == 3:
	                df = test.iloc[1:]
	       		df = df.iloc[:-1]
	                os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_2')
	                dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_2')
	                #print dfObs.iloc[df.index.values]['NewSUID'].values
	                df = test.iloc[2:]
	                os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_3')
	                dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_3')
	                #print dfObs.iloc[df.index.values]['NewSUID'].values
	        if len(test) == 4:
	       		df = test.iloc[1:]
	       		df = df.iloc[:-2]
	                os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_2')
	               	dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_2')
	                #print dfObs.iloc[df.index.values]['NewSUID'].values
	               	df = test.iloc[2:]
	       		df = df.iloc[:-1]
	                os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_3')
	               	dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_3')
	               	#print dfObs.iloc[df.index.values]['NewSUID'].values
	               	df = test.iloc[3:]
	                os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_4')
	                dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_4')
	                #print dfObs.iloc[df.index.values]['NewSUID'].values
		if len(test) == 5:
                        df = test.iloc[1:]
                        df = df.iloc[:-3]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_2')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_2')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values
                        df = test.iloc[2:]
                        df = df.iloc[:-2]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_3')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_3')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values
                        df = test.iloc[3:]
			df = df.iloc[:-1]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_4')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_4')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values	
                        df = test.iloc[4:]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_5')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_5')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values	
		if len(test) == 6:
                        df = test.iloc[1:]
                        df = df.iloc[:-4]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_2')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_2')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values
                        df = test.iloc[2:]
                        df = df.iloc[:-3]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_3')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_3')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values
                        df = test.iloc[3:]
			df = df.iloc[:-2]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_4')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_4')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values	
                        df = test.iloc[4:]
			df = df.iloc[:-1]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_5')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_5')
                        #print dfObs.iloc[df.index.values]['NewSUID'].values	
                        df = test.iloc[5:]
                        os.system('cp -r ../'+directory+'/'+fn+' ../'+directory+'/'+fn+'_6')
                        dfObs = dfObs.set_value(df.index.values,'NewSUID',dfObs.iloc[df.index.values]['NewSUID'].values + '_6')


	dfObs.to_csv('../Data/'+directory+'duplicate.csv')
