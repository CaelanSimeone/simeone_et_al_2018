import pandas as pd
import os
from tqdm import tqdm

def Clean_Folders(inputname,outputname,species):
	'''This function cleans the data from the observed csv file and ouputs it into a form set for use by Ech2o. var1 = the name of the folder where the raw data is. var2 = the name of the new folder. var3(str) = the species abrreviation if only one species is to be considered and nan if all species are to be considered.'''
	print "Cleaning"
	dfData = pd.read_csv('../Data/Master_Observed.csv')
	dfData = dfData.dropna(axis=1,how='all')
	
	if species != 'nan':
		dfData = dfData.drop(dfData.index[dfData.Spp != species])
	
	if inputname == '/mnt/lurbira/Caelan/Final_Seedlings/ech2o_inputs_9cell-1-0':
		inputname = '/mnt/lurbira/Caelan/Final_Seedlings/ech2o_inputs_9cell'
	
	dfData = dfData.drop(dfData.index[dfData['Stock Type'] != '1-0'])

	dfData = dfData.dropna(subset = ['1 YR Pct survival'])
		
	dfData = dfData.drop(dfData.index[dfData['Remark Keywords'].str.contains('Operational',na=False)])
	dfData = dfData.drop(dfData.index[dfData['Remark Keywords'].str.contains('Animal Dmg',na=False)])
	dfData = dfData.drop(dfData.index[dfData['Remark Keywords'].str.contains('Stock',na=False)])
	dfData = dfData.drop(dfData.index[dfData['Remark Keywords'].str.contains('Pest',na=False)])
	
	#dfData = dfData.drop(dfData.index[dfData.Sea != 'Spring'])	
	dfData = dfData.drop(dfData.index[dfData['Yr Plt'] <= 2000])
	#dfData = dfData.drop(dfData.index[dfData['Tool'] != 'H'])

	print dfData	
	for site in tqdm(dfData.NewSUID):
		os.system('cp -r '+inputname+'/X'+site+' ../'+outputname+'/X'+site)# + ' >/dev/null 2>&1')	
		
	dfData.to_csv('../Data/'+outputname+'.csv')
	print 'Done Cleaning'
