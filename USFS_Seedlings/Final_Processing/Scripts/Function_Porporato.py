from __future__ import  division
import pandas as pd
import os
import numpy as np
import matplotlib.pylab as plt
import math
from datetime import datetime
import scipy
from scipy.stats import gaussian_kde
import gc
#from multiprocessing import Process,current_process
#import multiprocessing 
#from joblib import Parallel, delayed
#import pymc3 as mc
#from subprocess import call, check_call
#import theano.tensor as T
#from theano.tensor import _shared
#from theano import as_op
#from scipy import optimize
#from shutil import copyfile, rmtree
#import pickle
#import operator
from tqdm import tqdm


def Porporato(folders, directory, cells, sperryb, sperryc, PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold):
	''' This function will take all of the folders and run the DSI on them for the year they are planted vs their mortality at 1 year. '''

	# LOAD OBSERVATIONS
	if directory == '9cell-1-0':
		dfObservations = pd.read_csv('../Data/9cellduplicate.csv')
	else:
		dfObservations = pd.read_csv('../Data/'+directory+'duplicate.csv')	
	
	dfObservations.set_index('NewSUID',inplace=True)

	#Create lists for variables to save. Line 1 is variables to be exported. 
	P_mortlist = []
	Site_list = []
	One_yr_survival_list = []
	DSI_list_0 = []
	DSI_list_1 = []
	DSI_list_2 = []
	DSI_list_3 = []
	Spp_list = []	
        X_list = []
        Y_list = []
	Z_list = []
        For_list = []
        Dir_list = []
	Stk_list = []
	Stock_type_list = []
	Stock_category_list = []
	Spp_list = []
	Aspect_list = []
	Aspect_category_list = []
	Three_yr_survival_list = []
	Year_list = []
        Static_Stress_list = []
        Weighted_Average_list = []
        Max_PLC_list = [] 

	for fn in tqdm(folders):
		## Get Data for folder.
		site = fn [1:]
		site_obs = dfObservations.loc[dfObservations.index == site]
		plant_year = site_obs['Yr Plt'].values[0]
		start_year = int(str(plant_year)[:-2])
	
		## Get Information to pass on to final csv file.
		p_mort = 1-(float(site_obs['1 YR Pct survival'].values[0][:-1])/100.)
		oneyr_surv = site_obs['1 YR Pct survival'].values[0]
		threeyr_surv = site_obs['3 YR PCT Survival'].values[0]
		with open('../'+directory+'/'+fn+'/Spatial/dem.asc') as f:
	                content = f.readlines()

		Spp = site_obs['Spp'].values[0]	
		Stks = site_obs['Trees stk'].values[0]
		Forest = site_obs['For'].values[0] 
	        District = site_obs['Dist '].values[0]
		stktype = site_obs['Stock Type'].values[0]
		stkcategory = site_obs['Stock Category'].values[0]
		aspect = site_obs['Asp'].values[0]
		aspect_cat = site_obs['Aspect Category'].values[0]
		
		#Pull Data	
		dfSimulated = mcmc_PullData(start_year,fn,'../'+directory,cells)

		#dfSimulated_0 is the year where the seedlings were planted. Simulated 1 - 3 are 1 - 3 years after planting respectivly. 
		#dfSimulated_0 = dfSimulated.loc[(dfSimulated.index.year == start_year)]
		#dfSimulated_1 = dfSimulated.loc[(dfSimulated.index.year == start_year+1)]
		#dfSimulated_2 = dfSimulated.loc[(dfSimulated.index.year == start_year+2)]
		#dfSimulated_3 = dfSimulated.loc[(dfSimulated.index.year == start_year+3)]
		

		#Calculate DSI	
		Dynamic_Stress_0,weightedaverage,staticstress,maxplc = Calculate_DSI(dfSimulated.loc[(dfSimulated.index.year == start_year)], 
											sperryb, sperryc,PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold)	
		#Dynamic_Stress_1 = Calculate_DSI(dfSimulated_1, sperryb, sperryc,PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold)	
		#Dynamic_Stress_2 = Calculate_DSI(dfSimulated_2, sperryb, sperryc,PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold)	
		#Dynamic_Stress_3 = Calculate_DSI(dfSimulated_3, sperryb, sperryc,PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold)	
		## collect variables		
		P_mortlist.append(p_mort)
		Site_list.append(site)
		One_yr_survival_list.append(oneyr_surv)	
		Three_yr_survival_list.append(threeyr_surv)	
		DSI_list_0.append(Dynamic_Stress_0)
		#DSI_list_1.append(Dynamic_Stress_1)
		#DSI_list_2.append(Dynamic_Stress_2)
		#DSI_list_3.append(Dynamic_Stress_3)
        	X_list.append(content[2][10:-3])
        	Y_list.append(content[3][10:-3])
		Z_list.append(content[7][6:12])
        	For_list.append(Forest)
        	Dir_list.append(District)
		Stk_list.append(Stks)
		Stock_type_list.append(stktype)
		Stock_category_list.append(stkcategory)
		Spp_list.append(Spp)
		Aspect_list.append(aspect)
		Aspect_category_list.append(aspect_cat)
		Year_list.append(plant_year)
		Static_Stress_list.append(staticstress)
		Weighted_Average_list.append(weightedaverage)
		Max_PLC_list.append(maxplc)
	dfModeled = pd.DataFrame({	'ID':Site_list,'1_Year_Survival':One_yr_survival_list,'DSI':DSI_list_0, 
					'Percent_Mortality': P_mortlist,'1_Year_Survival':Three_yr_survival_list,
				  	'X':X_list, 'Y':Y_list, 'Z':Z_list, 'Forest':For_list, 'District':Dir_list, 
					'Stakes':Stk_list, 
					'Stock_Type':Stock_type_list, 'Stock_Category': Stock_category_list, 'Species':Spp_list,
					'Aspect':Aspect_list, 'Aspect_Category':Aspect_category_list,
					'3_Year_Survival':Three_yr_survival_list,
					'Year':Year_list,'Weighted_Average':Weighted_Average_list,'Static_Stress':Static_Stress_list,
					'Max_PLC':Max_PLC_list})
					#'DSI_1':DSI_list_1,'DSI_2':DSI_list_2,'DSI_3':DSI_list_3,
	dfModeled.set_index('ID',inplace=True)

	dfModeled.to_csv('../Outputs/'+directory+'r75.csv') 


def Calculate_DSI(dfSimulated, sperryb, sperryc, PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold):
	''' Run the Porporato framework and output the Dynamic Stress Value'''
	dfSimulated['PLC'] = 1.0 - math.e ** (-((-np.asarray(dfSimulated['site1']) / sperryb) ** sperryc))
	Max_PLC = np.asarray(dfSimulated['PLC']).max()
	dfSimulated['PLC'] = np.where(dfSimulated['SoilTemp'] >= Soil_Threshold, dfSimulated['PLC'],0.0)	
	dfSimulated['PLC_rolling'] = dfSimulated['PLC'].rolling(3).mean()

	dfSimulated['Drought_Days'] = np.where(dfSimulated['PLC']>=PLC_init,1.0,0.0)

	Tseas = np.where(dfSimulated['SoilTemp'] >= Soil_Threshold, 1.0,0).sum()

        Drought_Start = np.asarray(dfSimulated['PLC_rolling'])
        start_count = 0.00
        for i in np.arange(1,len(Drought_Start)):
                if Drought_Start[i] > PLC_init and Drought_Start[i-1] <= PLC_init:
                        start_count+=1
        Stress_Frequency = start_count
	#print Stress_Frequency
	
	dfSimulated['Static_Stress'] = (PLC_init - dfSimulated['PLC'])/(PLC_init - PLC_crit)
        dfSimulated['Static_Stress'] = np.where(dfSimulated['Static_Stress'] >= 0, dfSimulated['Static_Stress'],0)
        dfSimulated['Static_Stress'] = np.where(dfSimulated['Static_Stress'] <= 1, dfSimulated['Static_Stress'],1)

	Days_of_Stress = dfSimulated['Drought_Days'].sum()
	Stress_Sum = dfSimulated['Static_Stress'].sum()

	if Days_of_Stress == 0:
		Weighted_Average_Stress = 0
	else:
                Weighted_Average_Stress = Stress_Sum / Days_of_Stress
	

	if Stress_Frequency == 0:
		Dynamic_Stress = 0
	else:
		Average_Duration = Days_of_Stress/Stress_Frequency
		Dynamic_Stress = ((Weighted_Average_Stress * Average_Duration) / (Por_k * Tseas)) ** (Stress_Frequency ** (-Por_r))

	#plt.plot(dfSimulated['PLC'])
	#plt.plot(dfSimulated['PLC_rolling'])
	#print Dynamic_Stress
	#plt.show()

	return Dynamic_Stress, Weighted_Average_Stress,Stress_Sum, Max_PLC 

def mcmc_PullData(start_year, fn, directory,cells):
        ''' Pull soil temperature and LWP and combine them into a single df at a daily resolution'''
	if cells == 9:
		 dfData = pd.read_table(directory+'/'+fn+'/Results/LeafWaterPot[0].tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
	else:
		 dfData = pd.read_table(directory+'/'+fn+'/Results/LeafWaterPot[0].tab',header=None, skiprows=6,names=['site1','nan']).drop('nan',axis=1)
 	
	ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfData.shape[0], freq='3H')
        dfData.index = ind
	
	if cells == 9:
		dfData_soil = pd.read_table(directory+'/'+fn+'/Results/SoilTemp.tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
        else:
		dfData_soil = pd.read_table(directory+'/'+fn+'/Results/SoilTemp.tab',header=None, skiprows=6,names=['site1','nan']).drop('nan',axis=1)
	
	ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfData_soil.shape[0], freq='3H')
        dfData_soil.index = ind

        dfData['SoilTemp'] = dfData_soil['site1']
        dfData = dfData.resample('24H').min()
	return dfData	


