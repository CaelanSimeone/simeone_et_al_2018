import pandas as pd
import os
from tqdm import tqdm
import numpy as np

def Spinup(folders,directory,scalar):

	os.chdir('../')
	cwd = os.getcwd()
	
	for fn in tqdm(folders):	
		os.chdir(cwd)
		directory_temp = directory
		dfSl1 = pd.read_csv('./'+directory_temp+'/'+fn+'/Results/SoilMoistureL1.tab',skiprows=4,delimiter = '\t',names = [1,'site','nan'],header=None)
		Sl1last = np.asarray(dfSl1['site'])[2919]

		dfSl2 = pd.read_csv('./'+directory_temp+'/'+fn+'/Results/SoilMoistureL2.tab',skiprows=4,delimiter = '\t',names = [1,'site','nan'],header=None)
		Sl2last = np.asarray(dfSl2['site'])[2919]

		dfSl3 = pd.read_csv('./'+directory_temp+'/'+fn+'/Results/SoilMoistureL3.tab',skiprows=4,delimiter = '\t',names = [1,'site','nan'],header=None)
		Sl3last = np.asarray(dfSl3['site'])[2919]

		dfSWE = pd.read_csv('./'+directory_temp+'/'+fn+'/Results/SWE.tab',skiprows=4,delimiter = '\t',names = [1,'site','nan'],header=None)
		SWElast = np.asarray(dfSWE['site'])[2919]

		dfSoilT = pd.read_csv('./'+directory_temp+'/'+fn+'/Results/SoilTemp.tab',skiprows=4,delimiter = '\t',names = [1,'site','nan'],header=None)
		STlast = np.asarray(dfSoilT['site'])[2919]


		#print dfSl1last
		#print dfSl2last
		#print dfSl3last
		#print dfSWElast
		#print STlast

		os.chdir('./'+directory+'/'+fn+'/Spatial')

	        os.system("pcrcalc 'soiltemp.map = unit.map *"+str(STlast)+"' >/dev/null 2>&1")
	        os.system("pcrcalc 'SWCL1.map = unit.map *"+ str(Sl1last)+"' >/dev/null 2>&1")
	        os.system("pcrcalc 'SWCL3.map = unit.map *"+str(Sl3last)+"' >/dev/null 2>&1")
	        os.system("pcrcalc 'SWCL2.map = unit.map *"+str(Sl2last)+"' >/dev/null 2>&1")
	        os.system("pcrcalc 'SWE.map = unit.map *"+str(SWElast)+"' >/dev/null 2>&1")
		os.chdir(cwd+'/Scripts')
