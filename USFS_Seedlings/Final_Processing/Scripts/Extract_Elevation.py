from __future__ import  division
import pandas as pd
import os
import numpy as np
import matplotlib.pylab as plt
import math
from datetime import datetime
import scipy
from scipy.stats import gaussian_kde
import gc
from tqdm import tqdm

directory = '9cell-1-0'
folders = os.listdir('../'+directory)

def Porporato(folders, directory):
	''' This function will take all of the folders and run the DSI on them for the year they are planted vs their mortality at 1 year. '''

	# LOAD OBSERVATIONS
	dfObservations = pd.read_csv('../Data/'+directory+'duplicate.csv')	
	
	dfObservations.set_index('NewSUID',inplace=True)

	Elevation = []
	Precipitaiton = []
	Temperature = []

	for fn in tqdm(folders):
		## Get Data for folder.
		site = fn [1:]
		site_obs = dfObservations.loc[dfObservations.index == site]
		
		if  site_obs['Stock Type'].values[0] != '1-0':
			continue
		elif site_obs['Spp'].values[0] == 'ES':
			continue	
		elif site_obs['Spp'].values[0] == 'LP':
			continue	
		print site_obs
		if site_obs['Trees stk'].values[0] < 30:
			continue	
		plant_year = site_obs['Yr Plt'].values[0]
		start_year = int(str(plant_year)[:-2])
	
		with open('../'+directory+'/'+fn+'/Spatial/dem.asc') as f:
	                content = f.readlines()
		print content
		elevations = content[8].split()[2]
		print elevations
		#Pull Data	
        	''' Pull soil temperature and LWP and combine them into a single df at a daily resolution'''
		dfData = pd.read_table('../'+directory+'/'+fn+'/Results/Precip.tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
 		
		ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfData.shape[0], freq='3H')
        	dfData.index = ind
		dfData = dfData[dfData.index.year == start_year]
		precip = dfData.sum()	
		
		dfData = pd.read_table('../'+directory+'/'+fn+'/Results/AvgTemp.tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
 		
		ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfData.shape[0], freq='3H')
        	dfData.index = ind
		dfData = dfData[dfData.index.year == start_year]
        	dfData = dfData.resample('24H').max()
		temp = dfData.mean()	

		Elevation.append(float(elevations))
		Precipitaiton.append(precip)
		Temperature.append(temp)	
	dfModeled = pd.DataFrame({'Elevation':Elevation,'Precipitation':Precipitaiton,'Temperature':Temperature})


	dfModeled.to_csv('../Outputs/SiteStats.csv') 

	print dfModeled

	print 'Mean Elevation=', dfModeled.Elevation.mean()
	print 'Std Elevation=',np.std(np.asarray(dfModeled.Elevation))
	
	print 'Mean Precipitation=', dfModeled.Precipitation.mean()*86400/8
	print 'Std Precipitation=',np.std(np.asarray(dfModeled.Precipitation))*86400/8

	print 'Mean Temperature=', dfModeled.Temperature.mean()
	print 'Std Temperature=',np.std(np.asarray(dfModeled.Temperature))
Porporato(folders,directory)
