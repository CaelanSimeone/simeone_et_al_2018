from __future__ import  division
import pandas as pd
import os
import numpy as np
import matplotlib.pylab as plt
import math
from datetime import datetime
import scipy
from scipy.stats import gaussian_kde
import gc
from multiprocessing import Process,current_process
import multiprocessing 
from joblib import Parallel, delayed
import pymc3 as mc
from subprocess import call, check_call
import theano.tensor as T
from theano.tensor import _shared
from theano import as_op
from scipy import optimize
from shutil import copyfile, rmtree
import pickle
import operator



def Monte_Carlo(folders,directory,scalar,cells):
	# LOAD OBSERVATIONS
	Year = 1 # Choose 1 or 3
	dfObs = pd.read_csv('../Data/'+directory+'duplicate.csv')	
	observe_yr1 = []
	P_mortlst,temperature,PLC_lst,LWP_lst,deficits,Spp_lst,wavelst,aspect_category,Tlst,Nlst,Tseaslst,fn_lst,aspect_lst,z_lst,x_lst,y_lst,For_lst,Dir_lst,Stk_lst,threeyr_lst,oneyr_lst,Stock_type,Stock_category =[],[],[],[],[],[],[],[],[], [],[],[],[],[],[],[],[],[],[],[],[],[],[]
	count =1.
	failed = 0.
	number = len(folders)
	
	for fn in folders:
		site = fn [1:]
		test = dfObs.loc[dfObs['NewSUID'] == site]
		count += 1
		try:
			start_year = test['Yr Plt'].values[0]
		except: 
			print test
			print fn, ' failed'
			continue
		with open('../'+directory+'/'+fn+'/Spatial/dem.asc') as f:
	                content = f.readlines()
		Spp = test['Spp'].values[0]	
		Stks = test['Trees stk'].values[0]
		Forest = test['For'].values[0] 
	        District = test['Dist '].values[0]
		stktype = test['Stock Type'].values[0]
		stkcategory = test['Stock Category'].values[0]
		aspect = test['Asp'].values[0]
		aspect_cat = test['Aspect Category'].values[0]
		if Stks < 30:
			failed += 1
			continue
		start_year = int(str(start_year)[:-2])
		if start_year == 2000:
			failed+=1
			continue
		
		try:
			if Year == 1:
				p_mort = 1-(float(test['1 YR Pct survival'].values[0][:-1])/100.)
#			if Year == 3:
#				p_mort = 1-(float(test['3 YR PCT Survival'].values[0][:-1])/100.)
		except:
			failed +=1	
			continue

		oneyr_surv = test['1 YR Pct survival'].values[0]
		threeyr_surv = test['3 YR PCT Survival'].values[0]
		if cells == 9:
			dfSpecial = pd.read_table('../'+directory+'/'+fn+'/Results/SkinTemp.tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
                else:
			dfSpecial = pd.read_table('../'+directory+'/'+fn+'/Results/SkinTemp.tab',header=None, skiprows=6,names=['site1','nan']).drop('nan',axis=1)
                ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfSpecial.shape[0], freq='3H')
                dfSpecial.index = ind

                MaxT = dfSpecial.site1.max()


		if cells ==9:
			dfSpecial = pd.read_table('../'+directory+'/'+fn+'/Results/SoilSatDef.tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
                else:
			dfSpecial = pd.read_table('../'+directory+'/'+fn+'/Results/SoilSatDef.tab',header=None, skiprows=6,names=['site1','nan']).drop('nan',axis=1)
                ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfSpecial.shape[0], freq='3H')
                dfSpecial.index = ind

                dfSpecial = dfSpecial.loc[dfSpecial.index.month == 7]

                Deficit = dfSpecial.site1.mean()


		dfData = mcmc_PullData(start_year,fn,'../'+directory,cells)

		wave_yrs = []
	        T_yrs = []
     	        N_yrs = []
     		Tseas_yrs = []

		if Year == 1:
			dates = [0]
		if Year == 3: 
			dates = [0,1,2]	

		#print dfData['site1']
		for shift in dates:
			#print fn, 'plc stuff'
			dfData_short = dfData.loc[(dfData.index.year == start_year + shift)]
			
			sperryb = 7.84436 #PLC= 1 - e^(-(-Psi/b))^c
			sperryc = 1.48807
			PLC_init = .300#0.33
			PLC_crit = .495#0.53
			Por_k =0.246	
			LWP_min = np.asarray(dfData_short['site1']).min()
			dfData_short['PLC'] = 1.0 - math.e ** (-((-np.asarray(dfData_short['site1']) / sperryb) ** sperryc))
			PLC_max = np.asarray(dfData_short['PLC']).max()
			dfData_short['boolean'] = np.where(dfData_short['PLC']>=PLC_init,1.0,0.0)
	
			Tseas = np.where(dfData_short['SoilTemp'] >= 5, 1.0,0).sum()
			N = max(sum([y for (x,y,z,zz) in zip(dfData_short.boolean.tolist(),dfData_short.boolean.tolist()[1:],dfData_short.boolean.tolist()[2:],dfData_short.boolean.tolist()[3:]) if y == 1 and x < 1 and z ==1 and zz ==1]),1)
			if dfData_short['boolean'].sum() == 0:
				weightedaverage = 0.0
			else:
				weightedaverage = (((np.where(dfData_short['boolean'] == 1,dfData_short['PLC'],0)).sum()/(dfData_short['boolean'].sum()))-PLC_init)/(PLC_crit-PLC_init)
			T_p = (dfData_short['boolean'].sum())/N
	                if any( [fn == 'X011403A580100141000',math.isnan(weightedaverage) == True,math.isnan(T_p) == True,math.isnan(N) == True,math.isnan(Tseas) == True,math.isnan(p_mort) == True]):
	                        failed += 1
	                        continue



	                T_yrs.append(T_p)
	                wave_yrs.append(weightedaverage)
	                N_yrs.append(N)
	                Tseas_yrs.append(Tseas)
	
	
	        wavelst.append(np.asarray(wave_yrs).mean())
		Tlst.append(np.asarray(T_yrs).mean())
	        Nlst.append(np.asarray(N_yrs).mean())
	        Tseaslst.append(np.asarray(Tseas_yrs).mean())
		P_mortlst.append(p_mort)
		fn_lst.append(fn)
        	x_lst.append(content[2][10:-3])
        	y_lst.append(content[3][10:-3])
		z_lst.append(content[7][6:12])
        	For_lst.append(Forest)
        	Dir_lst.append(District)
		Stk_lst.append(Stks)
		oneyr_lst.append(oneyr_surv)	
		threeyr_lst.append(threeyr_surv)
		Stock_type.append(stktype)
		Stock_category.append(stkcategory)
                temperature.append(MaxT)
                deficits.append(Deficit)
		Spp_lst.append(Spp)
		aspect_lst.append(aspect)
		LWP_lst.append(LWP_min)
		aspect_category.append(aspect_cat)
		PLC_lst.append(PLC_max)
		#print fn
	Por_r = scalar
	means = (np.asarray(wavelst)*np.asarray(Tlst)/(Por_k*np.asarray(Tseaslst))) ** (np.asarray(Nlst)**(-Por_r))
	#means = means.clip(a_min=0,a_max=1)
	means= np.where(means < 1 , means,1)
	#model = mc.Model()
	#with model:
	#    Por_r = mc.Uniform('Por_r',lower = 0,upper = 1)# mu=.9, sd=0.025)
	#    #Por_r = 0
	#	#Por_k = mc.Uniform('Por_k',lower=.15,upper=.25)
	#    sd1 = mc.HalfNormal('sd1',sd=0.001/(0.1+np.asarray(P_mortlst)),shape = len(P_mortlst) )

	#    #sd1 = mc.HalfNormal('sd1',sd=.2 )
	#    
	#    #bias = mc.HalfNormal('bias', sd=0.5) 
	#    means = (np.asarray(wavelst)*np.asarray(Tlst)/(Por_k*np.asarray(Tseaslst))) ** (np.asarray(Nlst)**(-Por_r))
	#    means = means.clip(a_min=0,a_max=1)
	#    #means = 1- ((1 - means)**3)
	#    #observations =(P_mortlst + bias).clip(a_min=0,a_max=1)
	#    observations = P_mortlst
	#    obs = mc.Normal('obs', mu=means, sd=sd1, observed=observations) 
	#     
	#    trace = mc.sample(draws=5000,  njobs=1 )
	#    trace = trace#[4500:]
	#    postPred = mc.sample_ppc(trace)
	#    (mc.df_summary(trace)).to_csv('../../Figures/MC_Table.csv')
	#
	##mc.traceplot(trace)
	#

	##dfTrace = mc.df_summary(trace)	
	##print dfTrace
	##bias = dfTrace.iloc[2]['mean']
	##postPred['obs'] = np.where(postPred['obs'] < 1,postPred['obs'],1)	

	##P_mortlst = P_mortlst + bias
	### Create csv file with important values as listed below.
        ##print len(fn_lst),len(oneyr_lst),len(postPred['obs'].mean(axis=0)),len(x_lst),len(y_lst),len(For_lst),len(Dir/np_lst),len(Stk_lst),len(threeyr_lst)


	#Mapdf = pd.DataFrame({'ID':fn_lst,'1YR_surv':oneyr_lst,'X':x_lst,'Y':y_lst,'Forest':For_lst,'Stock_Type':Stock_type,'Stock_Category':Stock_category,
        #                       'District':Dir_lst,'Stakes':Stk_lst,'3YR_surv':threeyr_lst,'Percent_Mortality':P_mortlst,'DSI':postPred['obs'].mean(axis=0)})
        #Mapdf.to_csv('Mapdataframe'+str(scalar)+'.csv') 

	#mcmc_plot(postPred['obs'],np.asarray(P_mortlst),trace,scalar)
	#slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(P_mortlst,postPred['obs'].mean(axis=0))
        #print 'Rsquared = ', r_value **2
	#return r_value**2

### Temp#orary stuff.

	Mapdf = pd.DataFrame({'ID':fn_lst,'1YR_surv':oneyr_lst,'X':x_lst,'Y':y_lst,'Forest':For_lst,'Stock_Type':Stock_type,'Stock_Category':Stock_category,
                               'Species':Spp_lst,'District':Dir_lst,'Stakes':Stk_lst,'3YR_surv':threeyr_lst,'Percent_Mortality':P_mortlst,'DSI':means,
				'MaxT':temperature,'Deficit':deficits,'Elev':z_lst,'Aspect':aspect_lst,'Aspect_Category':aspect_category})
        Mapdf.to_csv('../Ouputs/Mapdataframe'+directory+'.csv') 



        #slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(means,P_mortlst)
        #print 'Rsquared = ', r_value **2

        #plt.plot(means,P_mortlst,'.',color='red',label='Rsquared='+str(r_value**2))
        #plt.plot([0,1],[0,1],'--')
        #plt.plot([0,1],[intercept,intercept+slope],label='Best Fit')
        #plt.legend(loc=0)
        #plt.ylabel('Observed Mortailty')
        #plt.xlabel('Modeled Mortality')
        #plt.savefig('../../Figures/Seedlings_Comparison'+str(scalar)+'.png')
        #plt.close()

	#return r_value**2



def mcmc_plot(postPred,P_mortlst,trace,scalar):
        R_square = []
        R_dict = {}
        count = 0
        for column in postPred:
        #       print len(column)
                slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(column,P_mortlst)
                R_square.append(r_value**2)
                R_dict[count] = r_value**2
                count +=1
                plt.plot([0,1],[intercept,intercept+slope],color='grey',alpha = .5,label=None)
        print 'max rsquared=',np.asarray(R_square).max()
        best_column = max(R_dict.iteritems(), key=operator.itemgetter(1))[0]
        print 'bestParams=',trace[best_column]

        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(postPred.mean(axis=0),P_mortlst)
        print 'Rsquared = ', r_value **2

        plt.plot(postPred.mean(axis=0),P_mortlst,'.',color='red',label='Rsquared='+str(r_value**2))
        plt.plot([0,1],[0,1],'--')
        plt.plot([0,1],[intercept,intercept+slope],label='Best Fit')
        plt.legend(loc=0)
        plt.xlabel('Observed Mortailty')
        plt.ylabel('Modeled Mortality')
        plt.savefig('../../Figures/Seedlings_Comparison'+str(scalar)+'.png')
        plt.show()



	
def mcmc_PullData(start_year, fn, directory,cells):
        if cells == 9:
		 dfData = pd.read_table(directory+'/'+fn+'/Results/LeafWaterPot[0].tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
	else:
		 dfData = pd.read_table(directory+'/'+fn+'/Results/LeafWaterPot[0].tab',header=None, skiprows=6,names=['site1','nan']).drop('nan',axis=1)
#	ind = pd.date_range('2000-10-01 00:00:00', periods=dfData.shape[0], freq='3H')       
 	ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfData.shape[0], freq='3H')
        dfData.index = ind
	if cells == 9:
		dfData_soil = pd.read_table(directory+'/'+fn+'/Results/SoilTemp.tab',header=None,skiprows=12,names=[1,2,3,4,'site1',6,7,8,9]).drop([1,2,3,4,6,7,8,9],axis=1)
        else:
		dfData_soil = pd.read_table(directory+'/'+fn+'/Results/SoilTemp.tab',header=None, skiprows=6,names=['site1','nan']).drop('nan',axis=1)
#        ind = pd.date_range('2000-10-01 00:00:00', periods=dfData_soil.shape[0], freq='3H')
	ind = pd.date_range(str(start_year-1)+'-10-01 00:00:00', periods=dfData_soil.shape[0], freq='3H')
        dfData_soil.index = ind

        dfData['SoilTemp'] = dfData_soil['site1']
        dfData = dfData.resample('24H').min()
	return dfData	


