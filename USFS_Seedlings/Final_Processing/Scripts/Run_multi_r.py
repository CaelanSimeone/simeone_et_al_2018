#import Function_Monte_Carlo as F_MC
import os
import Function_Porporato as F_P
import numpy as np

directory = '9cell'
folders = os.listdir('../'+directory)
cells = 9
#
sperryb = 7.84436 #PLC= 1 - e^(-(-Psi/b))^c
sperryc = 1.48807

PLC_init = 0.270
PLC_crit = .546
Por_k = 0.165	
Soil_Threshold = 5

for Por_r in np.arange(0,2.1,.1):
	F_P.Porporato(folders, directory, cells, sperryb, sperryc, PLC_init, PLC_crit, Por_k, Por_r, Soil_Threshold)
		







