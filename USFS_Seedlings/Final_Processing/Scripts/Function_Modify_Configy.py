import pandas as pd
import os

from tqdm import tqdm

def Modify_Config(folders,directory):
	dfObs = pd.read_csv('../Data/'+directory+'duplicate.csv')
	P_mort_list = []
	StartYear_dict = {}
	for fn in tqdm(folders):
	         site = fn [1:]
	         test = dfObs.loc[dfObs['NewSUID'] == site]
	         start_year_str = test['Yr Plt'].values[0]
	         start_year = int(str(start_year_str)[:-2])
	         try:
	
	                p_mort = 1-(float(test['1 YR Pct survival'].values[0][:-1])/100.)
	         except:
	                print test['1 YR Pct survival'].values[0]
	                os.system('rm -r ../'+directory+'/'+fn)
			folders.remove(fn)
	                print 'problem with pmort ', fn
	                continue
	         diference = start_year-1-2000
	         seconds = diference * 3.154E7
	         end = seconds + 4.2E7 + (3.154E7*3)
	         if seconds < 0:
	                folders.remove(fn)
	                print 'problem with time ', fn
			#os.system('rm -r ../All_Cleaned/'+fn)
			print test['Yr Plt'].values[0]
	                continue
	         P_mort_list.append(p_mort)
	         StartYear_dict[fn] = start_year
	
	         f = open('../Data/config.ini', 'r')    # pass an appropriate path of the required file
	         lines = f.readlines()
	         lines[41] = "Simul_start = " +  str(seconds) +'\n'
	         lines[42] = 'Simul_end = ' + str(end) + '\n'
	         f.close()
	
	         f = open('../Data/config.ini', 'w')
	         f.writelines(lines)
	         f.close()
	         os.system('cp ../Data/config.ini ../'+directory+'/'+str(fn)+ '/')         
	#print 'successful=',len(folders)
