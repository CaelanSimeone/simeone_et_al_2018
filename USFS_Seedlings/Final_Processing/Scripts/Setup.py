import numpy as np
import os
#import Utility as ut
import sys
import multiprocessing
from joblib import Parallel, delayed
import Function_Clean_Folders as F_CF
import Function_Set_Maps as F_SM
import Function_Set_Soil_Properties as F_SSP
import Function_Duplicates as F_D
import Function_Modify_Configy as F_MC
import Function_Multithread_Ech2o as F_ME
import Function_Spinup as F_S
import argparse

# pick 1cell, 9cell or 9cell_Satparams
#directory = '9cell-1-0'
directory = sys.argv[1]

###################################################################################
def Setup(directory,scalar,cells):
	print 'Setup 1/3'
	os.system('rm -r ../'+directory)
	os.system('mkdir ../'+directory)
	##
	print '	Clean Folders 1/6'
	if directory == '1cell':
		F_CF.Clean_Folders('/mnt/lurbira/Caelan/Final_Seedlings/ech2o_inputs_1cell',directory,'nan')
	else:
		F_CF.Clean_Folders('/mnt/lurbira/Caelan/Final_Seedlings/ech2o_inputs_9cell',directory,'nan')
       #
       	folders = os.listdir('../'+directory)
       ###
        print '	Set Maps 2/6'
        F_SM.Set_Maps(folders,directory,scalar,cells) 
        #
        print '	Set Soil Properties 3/6'
        F_SSP.Set_Soil_Properties(folders,scalar,directory)
        
        os.system('rm -r ../'+directory+'/*_*')
        #
        print 'Duplicats 4/6'
        F_D.Duplicates(folders,directory)
        #
        folders = os.listdir('../'+directory)
        #
        print '	Modify Config 5/6'
        F_MC.Modify_Config(folders,directory)
        #
        
        ####Remove non functional files
        #for fn in folders:
        #	myfile = '../'+directory+'/'+fn+'/Climate/precip.bin'
        #	if os.path.isfile(myfile):
        #        	pass
        #	else:
        #        	os.system('rm -r ../'+directory+'/'+fn) 


def Spinup(directory,scalar):
	folders = os.listdir('../'+directory)
	F_S.Spinup(folders,directory,scalar)
	F_ME.Multithread_Ech2o(folders,directory)

def __Main__(directory,spins,cells): # '1cell' or '9cell'
	scalar = 1
	#Setup(directory,scalar,cells)
	folders = os.listdir('../'+directory)
	print 'Starting'
	F_ME.Multithread_Ech2o(folders,directory)
	#for i in np.arange(spins):
	#	print 'Spinup '+str(i)+'/'+str(spins)
	#	Spinup(directory,scalar)

if directory == '1cell':
	cells = 1
else: 
	cells = 9 
__Main__(directory,5,cells)
