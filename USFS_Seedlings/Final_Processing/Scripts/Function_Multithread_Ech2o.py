import os
from multiprocessing import Process,current_process
import multiprocessing
from tqdm import tqdm
from joblib import Parallel, delayed

def EasyEch2o(fn,directory):
	os.chdir('/mnt/lurbira/Caelan/Final_Seedlings/Final_Processing/Scripts')
        os.chdir('../'+directory+'/'+fn)
        os.system('rm Results/*')
        os.system('./runech2o.sh')
	print fn

def Multithread_Ech2o(folders,directory):
	#print 'Ech2o'
	num_cores =10#multiprocessing.cpu_count()
	#print num_cores
	Parallel(n_jobs = num_cores)(delayed(EasyEch2o)(fn,directory) for fn in tqdm(folders))
