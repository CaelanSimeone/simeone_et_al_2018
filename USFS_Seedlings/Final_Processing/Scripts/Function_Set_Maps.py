import os
from tqdm import tqdm

def Set_Maps(folders,directory,scalar,cells):
	os.chdir('../')
	cwd = os.getcwd()
	
	for fn in tqdm(folders):
	        
		os.chdir(cwd)
		directory_temp = directory
	        
		os.chdir('./'+directory+'/'+fn+'/Spatial')

	        os.system('cp '+cwd+'/Data/SpeciesParams.tab ./SpeciesParams.tab')
                os.system('cp '+cwd+'/Data/runech2o.sh ../runech2o.sh')
		os.system('cp '+cwd+'/Data/config.ini ../config.ini')
	        os.system("pcrcalc 'lai.map = unit.map *.273' >/dev/null 2>&1")
	        os.system('mv lai.map lai[0].map')
	        os.system("pcrcalc 'psi_ae.map = unit.map * 0.259' >/dev/null 2>&1")
	        os.system("pcrcalc 'rootfrac1.map = unit.map *0.65' >/dev/null 2>&1")#0.65
	        os.system("pcrcalc 'rootfrac2.map = unit.map *0.35' >/dev/null 2>&1")#0.5
		os.system("pcrcalc 'age.map = unit.map *1' >/dev/null 2>&1")
	        os.system('mv age.map age[0].map')
		os.system("pcrcalc 'leakance.map = unit.map *0.0' >/dev/null 2>&1")
	        os.system("pcrcalc 'root.map = unit.map *200' >/dev/null 2>&1")# 200
	        os.system('mv root.map root[0].map')
                os.system("pcrcalc 'bas.map = unit.map *15' >/dev/null 2>&1")
                os.system('mv bas.map bas[0].map')
                os.system("pcrcalc 'KvKh.map = unit.map *1' >/dev/null 2>&1")
                os.system("pcrcalc 'depth_soil1.map = unit.map *0.1' >/dev/null 2>&1")
                os.system("pcrcalc 'depth_soil2.map = unit.map *0.3' >/dev/null 2>&1")
                os.system("pcrcalc 'p.map = unit.map *1' >/dev/null 2>&1")
                os.system('mv p.map p[0].map')
                os.system("pcrcalc 'hgt.map = unit.map *.1' >/dev/null 2>&1")
                os.system('mv hgt.map hgt[0].map')
                os.system("pcrcalc 'ntr.map = unit.map *100' >/dev/null 2>&1")
                os.system('mv ntr.map ntr[0].map')
		os.system("pcrcalc 'theta_r.map = unit.map *.0016' >/dev/null 2>&1")#0.0016
		os.system("pcrcalc 'chanmanningn.map = chanmask.map *1' >/dev/null 2>&1")
		os.system("pcrcalc 'chanparam.map = chanmask.map *10' >/dev/null 2>&1")
                os.system("pcrcalc 'snowmeltCoeff.map = unit.map *4.1E-8' >/dev/null 2>&1")
                os.system("pcrcalc 'randrough.map = unit.map *0.2' >/dev/null 2>&1")

		if directory == '9cell_Satparams':
			os.system("pcrcalc 'theta_r.map = unit.map *.0016' >/dev/null 2>&1")#0.0016
	        	os.system("pcrcalc 'root.map = unit.map *200' >/dev/null 2>&1")# 200
	        	os.system("pcrcalc 'rootfrac1.map = unit.map *0.65' >/dev/null 2>&1")#0.65
	        	os.system("pcrcalc 'rootfrac2.map = unit.map *0.35' >/dev/null 2>&1")#0.5
                	os.system("pcrcalc 'randrough.map = unit.map *0.005' >/dev/null 2>&1")
			

		if cells == 9:
			os.chdir('../Climate')
			os.system('cp ../../../../ech2o_inputs_1cell/'+fn+'/Climate/*.bin ./')
			os.system('cp ../Spatial/unit.map ./ClimZones.map')

		os.chdir(cwd+'/Scripts')
	
