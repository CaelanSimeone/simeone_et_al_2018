### This script executes all commands assoicated with the USFS Seedling modeling project.

import os

os.chdir('./Scripts')
os.system('python Setup.py 9cell')
os.system('python Run.py 9cell') 
os.system('mv ../Outputs/* ../USFS_Analysis/Data/')
os.chdir('../USFS_Analysis/Scripts')
os.system('python Scatter_Plot.py')
