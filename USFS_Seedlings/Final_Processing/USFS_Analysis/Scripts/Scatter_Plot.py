## This script plots a dataframe of DSI values. 

import pandas as pd 
import numpy as np
import matplotlib.pylab as plt
import scipy
from scipy.stats import gaussian_kde

#plt.style.use(['ggplot','seaborn-poster'])

from pylab import rcParams
rcParams['figure.figsize'] = 4, 3


def Plot(dfData,name):	
	slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(dfData[Modeled],dfData['Percent_Mortality'])

	print 'p=',p_value
	print 'R2=',r_value ** 2
	print 'Slope=',slope
	print 'intercept=',intercept
	print dfData[Modeled].mean()
	print dfData['Percent_Mortality'].mean()
	
	plt.plot([0,1],[0,1],'--',color = 'black')
	x = np.asarray(dfData[Modeled]).max()
	plt.plot([0,1],[intercept,intercept+(1*slope)],color='blue')
	
	dfSpecies = dfData[dfData.Species != 'PP']
	plt.plot(dfSpecies[Modeled],dfSpecies['Percent_Mortality'],'o',label = 'Other Species')
	
	for sp in ['PP']:
		dfSpecies = dfData[dfData.Species == sp]
		plt.plot(dfSpecies[Modeled],dfSpecies['Percent_Mortality'],'o',markeredgewidth=2,markeredgecolor='r',
		markerfacecolor='None',label='Ponderosa')
	
	#plt.title(name + ' n=' + str(len(dfData)))
	plt.legend(loc=0)
	plt.ylabel('Observed Mortailty')
	plt.xlabel('Modeled Mortality')
	plt.xlim(-.1,1.1)
	plt.ylim(-.1,1.1)
	plt.tight_layout()	
	#plt.show()
	plt.savefig(name+'USFSscatterplot.png',dpi=600)
	plt.close()

cells = '9cellr25'

dfData = pd.read_csv('../Data/'+cells+'.csv')
Modeled = 'DSI'
dfData.dropna(subset=['Percent_Mortality',Modeled],inplace=True)

dfData = dfData[dfData.Species != 'ES']
dfData = dfData[dfData.Species != 'LP']
dfData = dfData[dfData.Stakes > 30]

df1_0 = dfData[dfData.Stock_Type == '1-0']
df1_0 = df1_0[df1_0.Stakes >= 30]
Plot(df1_0, '1-0')

#########################################
#dfData = pd.read_csv('../Data/DSI_'+cells+'.csv')
#Modeled = 'DSI'
#dfData.dropna(subset=['Percent_Mortality',Modeled],inplace=True)
#dfData = dfData[dfData.Forest == 3]
#dfData = dfData[dfData.Year.isin([2002,2003,2004])]
#dfData = dfData[dfData.DSI > 0]
#
#Plot(dfData,'Bitterroot')


#
#
