## This script plots a dataframe of DSI values. 

import pandas as pd 
import numpy as np
import matplotlib.pylab as plt
import scipy
from scipy.stats import gaussian_kde

#plt.style.use(['ggplot','seaborn-poster'])

from pylab import rcParams
rcParams['figure.figsize'] = 4, 3


def Plot(dfData,name):	
	print dfData.Stakes
	print 'Mean=', dfData.Stakes.mean()
	print 'Std=',np.std(np.asarray(dfData.Stakes))



	density_pm = gaussian_kde((dfData['Stakes']).dropna())
	xs = np.linspace(0,200,200)
	density_pm.covariance_factor = lambda : .1
	density_pm._compute_covariance()
	plt.plot(xs,density_pm(xs),label='No Lag')
	plt.tight_layout()
	plt.show()

	plt.savefig('./Distribution.png')

cells = '9cell'

dfData = pd.read_csv('../Data/'+cells+'.csv')
Modeled = 'DSI'
dfData.dropna(subset=['Percent_Mortality',Modeled],inplace=True)

dfData = dfData[dfData.Species != 'ES']
dfData = dfData[dfData.Species != 'LP']
dfData = dfData[dfData.Stakes > 30]

df1_0 = dfData[dfData.Stock_Type == '1-0']
df1_0 = df1_0[df1_0.Stakes >= 30]
Plot(df1_0, '1-0')

#########################################
#dfData = pd.read_csv('../Data/DSI_'+cells+'.csv')
#Modeled = 'DSI'
#dfData.dropna(subset=['Percent_Mortality',Modeled],inplace=True)
#dfData = dfData[dfData.Forest == 3]
#dfData = dfData[dfData.Year.isin([2002,2003,2004])]
#dfData = dfData[dfData.DSI > 0]
#
#Plot(dfData,'Bitterroot')


#
#
