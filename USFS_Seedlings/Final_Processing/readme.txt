This folder contains all of the information required to run the USFS portion of this study. It has been partitioned into two parts. The main folder which does the main running of Ech2o across the point distributions, and the USFS_Analysis folder, where the plotting is done. 

The entire folder can be run with the Execute.py script. 

Run Order
1../Scripts python Setup.py 9cell
2../Scropts python Run.py 9cell
3. mv Outputs/* USFS_Analysis/Data/
4../USFS_Analysis/Scripts python Scatter_Plot.py

Last edited 4/17/18

-Caelan Simeone
