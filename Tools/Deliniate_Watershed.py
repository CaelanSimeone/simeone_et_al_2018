''' This Script Delineates watersheds using pcraster. The files that are needed are: base.map, ldd.map, and a Tsmask.map file with a single one value at the outlet.'''

import os
import pcraster as pcr
import numpy as np
from pcraster import *

#Delineate Watershed
#os.system("pcrcalc 'col2map --clone base.map probes.txt Tsmask.map'")
os.system("pcrcalc 'Points.map = ordinal(Tsmask.map)'")
os.system("pcrcalc 'Catchment.map = catchment(ldd.map,Points.map)'")
os.system("pcrcalc 'Catchment_Scalar.map = scalar(Catchment.map)'")

#Clip off areas outside of watershed.
catchment = pcr.readmap('Catchment_Scalar.map')
Catchment = pcr.pcr2numpy(catchment, 0)
print Catchment.sum()
catchment_pcr = pcr.numpy2pcr(Scalar, Catchment, 0)
pcr.report(catchment_pcr, 'CleanCatchment.map')

os.system("pcrcalc 'DEM.map = dem.map * CleanCatchment.map'")







