#This script examines the monte carlo runs for the porporato framework associated with the greenhouse.

#Last Edit: 9-18-17

from __future__ import division
import os, glob
os.environ['MKL_THREADING_LAYER']='GNU'
import numpy as np
import pandas as pd
from scipy.stats import gaussian_kde
import scipy
import matplotlib.dates as mdates
import matplotlib.pylab as plt
import math
import pymc3 as mc
import seaborn as sns
import pickle
import matplotlib.dates as mdates
import operator

plt.rcParams.update({'font.size': 16})
#plt.style.use(['ggplot','seaborn-talk'])

def __Main__():	
	Monte_Carlo_Analysis()
	dfPoint_obs = Get_Observed()
	simdsi = Plot_mcmc(dfPoint_obs)
	Plot_Scatter(simdsi,dfPoint_obs)


#Monte Carlo Analysis
def Monte_Carlo_Analysis():

	model = mc.Model()
	with model:
	
	    # Parameter Priors 
	    PLC_init = mc.Normal('PLC_init',sd=.1)
	    PLC_crit = mc.Normal('PLC_crit',sd=.1)
	    Por_k = mc.Normal('Por_k',sd=.2)
	    Por_r = mc.Normal('Por_r',sd=.2)
	    
	    sd1 = mc.Normal('sd1', sd=.01)
	
	    
	    trace_full  = mc.backends.text.load('Modeled_data/trace_results')
	    print trace_full
	    trace = trace_full
	    trace = trace_full[500:]
	
	mc.df_summary(trace)
	(mc.df_summary(trace)).to_csv('../Figures/Porporato_MC_Table.csv')
	
	
	d = {}
	for var in trace.varnames:
	    if "log" not in var:
	        d[var] = trace[var][:-2].mean()
	        mc.traceplot(trace, varnames=[var], grid=True, lines=d)
		plt.savefig('../Figures/Traces/Porporat0o_MC_trace_'+var)

def Get_Observed():
	dfPoint_obs = pd.read_csv('../Observational_Data/Master_Data_Caelan.csv')
	dfPoint_obs['Measurement.Time'] = dfPoint_obs['Date'].map(str) +' '+ dfPoint_obs['Time']+':00'
	dfPoint_obs = dfPoint_obs.set_index(pd.DatetimeIndex(dfPoint_obs['Measurement.Time']))
	
	#Extract Rocky Mountain Race
	dfPoint_obs = dfPoint_obs[dfPoint_obs.Race == 'RMR']
	
	#Delete Values noted as problematic by Gerard Sapes the Data Collector
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '6/22/2017 13:58:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:29:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:22:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:28:00']
	
	#Delete Data With different Treatment
	#dfPoint_obs = dfPoint_obs[dfPoint_obs['Treatment'] != 'C2' ]
	#dfPoint_obs = dfPoint_obs[dfPoint_obs['Treatment'] != 'CFC1' ]
	#dfPoint_obs = dfPoint_obs[dfPoint_obs['Treatment'] != 'CFC2' ]
	
	#Reset Problematic PLC
	dfPoint_obs['Plant_PLC'] = np.where(dfPoint_obs['Plant_PLC_%'] > 1, 
	                                    dfPoint_obs['Plant_PLC_%'],0)
	dfPoint_obs['Stem_PLC'] = np.where(dfPoint_obs['Stem_PLC_%'] > 1, 
	                                   dfPoint_obs['Stem_PLC_%'],0)
	
	#Remove Nighttime Observations There were two obs at 1 in the morning.
	dfPoint_obs = dfPoint_obs[dfPoint_obs.index.hour > 2]
	
	dfPoint_obs['Plant_PLC'] = np.where(dfPoint_obs['Plant_PLC_%'] > 1, dfPoint_obs['Plant_PLC_%'],0)
	dfPoint_obs['Stem_PLC'] = np.where(dfPoint_obs['Stem_PLC_%'] > 1, dfPoint_obs['Stem_PLC_%'],0)

	return dfPoint_obs


def plot_timeseries(df_sim, df_obs, **args):
    mean = df_sim.mean(axis=1)
    q025 = df_sim.quantile(q=0.025, axis=1)
    q01 = df_sim.quantile(q=0.1, axis=1)
    q20 = df_sim.quantile(q=0.2, axis=1)
    q80 = df_sim.quantile(q=0.8, axis=1)
    q90 = df_sim.quantile(q=0.9, axis=1)
    q985 = df_sim.quantile(q=0.985, axis=1)
    q50 = df_sim.quantile(q=.50, axis=1)
    p = plt.figure(figsize=(7,5))
    #ax = plt.plot(q50, c='r', label="Simulated Median Mortality", alpha=1)
    #ax = plt.fill_between(mean.index, q025, q985, color='0.75', alpha=0.35,label='95% Confidence Interval')
    #ax = plt.fill_between(mean.index, q01, q90, color='0.45', alpha=0.35,label='80% Confidence Interval')
    ax = plt.fill_between(mean.index, q20, q80, color='0.1', alpha=0.35,label='60% Confidence Interval')
    ax = plt.plot(mean, c='r', label="simulated", alpha=0.5)


    df_obs = df_obs.dropna()
    ax = plt.plot(df_obs.index, df_obs,'o',c='b', label="Observed Mortality", **args)
    #plt.ylim(0,1)
    return p, ax

def Plot_mcmc(dfPoint_obs):

	with open('Modeled_data/postPred2.txt', 'r') as buff:
	    ppc = pickle.load(buff)
	
	dfPredTraces = pd.DataFrame(ppc['obs'].T)
	
	## Give dates to the dataframes
	simdsi= dfPredTraces#[0:2]
	ind = pd.date_range('2017-02-27 20:00:00', periods=simdsi.shape[0], freq='24H')
	simdsi.index = ind
	simdsi=simdsi.dropna(axis=1,how='all')
		
	plt.close()
	p, ax =  plot_timeseries(simdsi, dfPoint_obs['P_mortality']/100)
	a = p.gca()
	myFmt = mdates.DateFormatter('%m/%d')
	a.xaxis.set_major_formatter(myFmt)
	a.set_ylabel("Mortality Fraction",fontsize=20)
#	a.legend(fontsize=20)
	plt.ylim(-.1,1.1)
	plt.tight_layout()
	p.savefig("../Figures/DSI_mcmc",dpi=600)
	plt.close()
	return simdsi

def Plot_Scatter(simdsi,dfPoint_obs):	
	#Scatter
	fig=plt.figure(figsize=[7,7])
	dfPoint_obs = dfPoint_obs.resample('24H').mean()
	mean = simdsi.quantile(q=.50, axis=1)
	mean = mean.resample('24H').max()
	
	peak1 = mean.max()
	peak2 = (dfPoint_obs['P_mortality']/100).max()
	peak = max(peak1,peak2)
	
	DSI_both = pd.concat([dfPoint_obs['P_mortality']/100,mean],axis=1)
	DSI_both = DSI_both.dropna(how='any')
	DSI_both = DSI_both.set_index(pd.DatetimeIndex(DSI_both.index))
	print DSI_both
	DSI_both = DSI_both.dropna(subset=['P_mortality'])
	slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(
	    DSI_both['P_mortality'], DSI_both[0.5])
	print r_value
	print 'DSI', ' Rsquared=',r_value**2,' p_value=',p_value,' slope=',slope,' intercept=',intercept	
	
	#plt.plot(DSI_both['P_mortality'])
	plt.plot(np.asarray(DSI_both['P_mortality']),np.asarray(DSI_both[0.5]),'o',label='Rsquared='+str(r_value**2)[0:5])
	plt.plot([0,1],[0,1],'--')
	plt.plot([0,1],[intercept,slope+intercept],label='bestfit')
	plt.legend(loc=0)
	plt.xlabel('Observed Mortality')
	plt.ylabel('Median Modeled Dynamic Stress')
	plt.ylim(-.1,peak)
	plt.xlim(-.1,peak)
	plt.savefig('../Figures/DSI_Scatter')
	#mean.to_csv('dsi.csv')

__Main__()

