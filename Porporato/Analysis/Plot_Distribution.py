#This script examines the monte carlo runs for the porporato framework associated with the greenhouse.

#Last Edit: 9-18-17

from __future__ import division
import matplotlib.dates as mdates
import matplotlib.pylab as plt
import os, glob
import numpy as np
import pandas as pd
from scipy.stats import gaussian_kde
import scipy
import math
import seaborn as sns
import pickle
import matplotlib.dates as mdates
import operator
plt.rcParams['figure.figsize'] = 5, 4

plt.rcParams.update({'font.size': 16})
#plt.style.use(['ggplot','seaborn-talk'])

dfData = pd.read_csv('Modeled_data/trace_results/chain-0.csv')
print dfData


density_pm = gaussian_kde((dfData['Por_k']).dropna())
xs = np.linspace(0,1,200)
density_pm.covariance_factor = lambda : .2
density_pm._compute_covariance()
plt.plot(xs,density_pm(xs),label='$k$')

density_pm = gaussian_kde((dfData['PLC_crit']).dropna())
xs = np.linspace(0,1,200)
density_pm.covariance_factor = lambda : .2
density_pm._compute_covariance()
plt.plot(xs,density_pm(xs),label='$PLC_{crit}$')

density_pm = gaussian_kde((dfData['PLC_init']).dropna())
xs = np.linspace(0,1,200)
density_pm.covariance_factor = lambda : .2
density_pm._compute_covariance()
plt.plot(xs,density_pm(xs),label='$PLC_{init}$')
plt.legend(loc=0)
plt.tight_layout()
#plt.show()
plt.savefig('./Distribution.png',dpi=600)

