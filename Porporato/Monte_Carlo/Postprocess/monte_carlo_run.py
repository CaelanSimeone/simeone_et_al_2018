import os
#os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu"
os.environ["OMP_NUM_THREADS"] = "1"
import mapio
from customDist import logp_gmix
import pymc3 as mc
from subprocess import call, check_call
import writeVegetParamTab
import theano.tensor as T
from theano import as_op
from scipy import optimize 
from scipy.stats import gaussian_kde
import numpy as np
import pandas as pd
from shutil import copyfile, rmtree
import pickle
import math

def Load_Observations():
	
	# LOAD OBSERVATIONS
	dfPoint_obs = pd.read_csv('../../Observational_Data/Master_Data_Caelan.csv')
	dfPoint_obs['Measurement.Time'] = dfPoint_obs['Date'].map(str) +' '+ dfPoint_obs['Time']+':00'
	dfPoint_obs = dfPoint_obs.set_index(pd.DatetimeIndex(dfPoint_obs['Measurement.Time']))
	
	#Extract Rocky Mountain Race
	dfPoint_obs = dfPoint_obs[dfPoint_obs.Race == 'RMR']
	
	#Delete Values noted as problematic by Gerard Sapes the Data Collector
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '6/22/2017 13:58:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:29:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:22:00']
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Measurement.Time'] != '7/7/2017 13:28:00']
	
	#Delete Data With different Treatment
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Treatment'] != 'C2' ]
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Treatment'] != 'CFC1' ]
	dfPoint_obs = dfPoint_obs[dfPoint_obs['Treatment'] != 'CFC2' ]
	
	#Reset Problematic PLC
	dfPoint_obs['Plant_PLC'] = np.where(dfPoint_obs['Plant_PLC_%'] > 1, dfPoint_obs['Plant_PLC_%'],0)
	dfPoint_obs['Stem_PLC'] = np.where(dfPoint_obs['Stem_PLC_%'] > 1, dfPoint_obs['Stem_PLC_%'],0)
	
	lwpobs = 0
	
	dfPoint_obs['P_mortality']=dfPoint_obs['P_mortality']/100
	dfPoint_obs = dfPoint_obs.resample('24H').mean()
	dfPoint_obs = dfPoint_obs.dropna(subset = ['P_mortality'])
	return dfPoint_obs


def Load_Simulated(sperryb,sperryc):

	dfSimLWP = pd.read_csv('Mean_LWP.csv',header=None,delimiter=',',names=['id','site1'])
	dfSimLWP.index = pd.to_datetime(dfSimLWP['id'])


	dfSimLWP['PLC'] = 1.0 - math.e ** (-((-dfSimLWP['site1']/ sperryb) ** sperryc))
	return dfSimLWP




### Define Model
def compiled_DS(dfSimulated,PLC_init,PLC_crit,porporatok,r):
        ''' This function takes a timeseries of LWP and convered PLC, and runs the porporato framework exporting a single pandas column of DSI'''

	dfSimulated['boolean'] = np.where(dfSimulated['PLC']>=PLC_init,1.0,0.0)#Change this later

        #This routine selects days where droughts starts and sets them to boolean true. A drought here is devined as 3 consecutive days wiht stress.
        dfDroughtfreq = dfSimulated.copy()
        DroughtDays = np.asarray(dfDroughtfreq.boolean)
        DroughtStart = np.zeros(len(DroughtDays))

        for i in np.arange(1,len(DroughtDays)-1):
            if DroughtDays[i] == 1 and DroughtDays[i-1]<0.5 and DroughtDays[i+1]==1:
                DroughtStart[i]=1

	## Static Stress
	dfSimulated['Static_Stress'] = (PLC_init - dfSimulated['PLC'])/(PLC_init - PLC_crit)
	dfSimulated['Static_Stress'] = np.where(dfSimulated['Static_Stress'] >= 0, dfSimulated['Static_Stress'],0)
	dfSimulated['Static_Stress'] = np.where(dfSimulated['Static_Stress'] <= 1, dfSimulated['Static_Stress'],1)

        Dynamic_Stress_array = np.empty(len(dfSimulated))
        Tseas = 150
	for i in np.arange(1,len(dfSimulated)+1):
                Stress_Sum = dfSimulated['Static_Stress'][0:i].sum()
                Days_of_Stress = dfSimulated['boolean'][:i].sum()
                if Days_of_Stress == 0:
			weightedaverage = 0
			Dynamic_Stress = 0
		else:
			weightedaverage = (Stress_Sum/(Days_of_Stress))
			#N = DroughtStart[0:i].sum()
			#if N == 0:
			#	N = 1
                	N=1
			T = Days_of_Stress/N
       		 
	        	Dynamic_Stress = (weightedaverage*T/(porporatok*Tseas)) ** (N**(-r))
		Dynamic_Stress_array[i-1] = Dynamic_Stress 
	
	##This portion needs to be activated if outputing actually DSI values.
	#dfSimulated['DSI'] = np.where(Dynamic_Stress_array <= 1, Dynamic_Stress_array,1)
	
	##This portion needs to be activated when outputing DSI vaules for an ensamble run. It prevents the piecewise function from disrupting the calibration. 
	dfSimulated['DSI'] = Dynamic_Stress_array

	
	dfSimulated_points = dfPoint_obs.join(dfSimulated,how='inner')
        return dfSimulated_points['DSI'] ## Return the points for the mcmc, and the dfSimulated natural for the posteriorprocessing.

sperryb = 7.84436 #PLC= 1 - e^(-(-Psi/b))^c
sperryc = 1.48807

dfPoint_obs = Load_Observations()
resfolder = '../Results'
dfSimLWP = Load_Simulated(sperryb,sperryc)


# Default vegetation parameter values
key_pars = ['PLC_init','PLC_crit','Por_k','Por_r','sd1']
init_pars = [0.3,0.6,0.15,.5,.1]
start_pars = dict(zip(key_pars, init_pars))

count = 1
@as_op(itypes=[T.dscalar]*5, otypes=[T.dvector])
def theta(PLC_init,
	PLC_crit,
	Por_k,
	Por_r,
	sd1):
    global count
    #print "Number of model calls ", count
    count+=1
    # Do some checks on parameters  
    PLC_init = max(PLC_init,0.0)
    PLC_init = min(PLC_init,PLC_crit)  
    #PLC_crit = max(PLC_crit,0.0)
    PLC_crit = min(PLC_crit,1)
    Por_k = max(Por_k,0.001)
    Por_r = max(Por_r,0.0)
    #print PLC_init,PLC_crit,Por_k,Por_r 
    

    log = os.tempnam('./','')+".txt"

    with open(log, 'w') as f:
           call(['ls'], stdout=f)



#Call the actual model
    try:
    	Output = compiled_DS(dfSimLWP,PLC_init,PLC_crit,Por_k,Por_r)
        
	#print 'Run Success'            
	
    except:
        print "Run failed!"    


    os.remove(log)    
    # postrocess and return the data
    if str(count)[-3:]=='000':
	   print 'Number of model calls ',count
 	   print "Parameter values, ", PLC_init,PLC_crit,Por_k,Por_r
 
    #Match lengths of modeled and observed.
    #print Output.values
    return Output.values



model = mc.Model()
with model:

    # Parameter Priors 
    PLC_init = mc.Normal('PLC_init', mu=0.3, sd=0.1)
    PLC_crit = mc.Normal('PLC_crit', mu=0.6, sd=0.1)
    Por_k = mc.Normal('Por_k', mu=0.15, sd=0.1)
    Por_r = mc.Normal('Por_r', mu=.5, sd=0.1)


    
    sd1 = mc.HalfNormal('sd1',sd=.1)
    
    vlist = [PLC_init,PLC_crit,Por_k,Por_r,sd1]
    
    Outputs = theta(*vlist)
    means = T.concatenate([Outputs])
    
    
    sd = T.ones_like(Outputs)*sd1
    #sd = T.concatenate((p1))

    observations = np.asarray(dfPoint_obs['P_mortality'])
            
    #print(observations)
    #print(means.tag.test_value)

	
    obs = mc.Normal('obs', mu=means, sd=sd, 
                        observed=observations)
                        
	#Set Sampler Type.
    step_slice = mc.Slice([sd1])
    step_mh1 = mc.Metropolis(vars=[PLC_init], S=np.ones(1)*.2, tune=True, tune_interval=100)
    step_mh2 = mc.Metropolis(vars=[PLC_crit], S=np.ones(1)*.2, tune=True, tune_interval=100)
    step_mh3 = mc.Metropolis(vars=[Por_k], S=np.ones(1)*.2, tune=True, tune_interval=100)
    step_mh4 = mc.Metropolis(vars=[Por_r], S=np.ones(1)*.5, tune=True, tune_interval=100)
    #step_mh5 = mc.Metropolis(vars=[sd1], S=np.ones(1)*.15, tune=True, tune_interval=100)

    backend = mc.backends.Text('trace_results')
    #oldtrace = mc.backends.text.load('trace_results')
    #start_pars = oldtrace.point(-1)

    trace = mc.sample(draws=5000, start=start_pars, init='ADVI', 
    njobs=1, step=[step_slice,step_mh1,step_mh2,step_mh3,step_mh4], trace=backend)

    


