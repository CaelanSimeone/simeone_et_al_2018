def writeVegetParamTab(fn, dictParVals):
    """

    :param fn: output filename
    :param dictParVals: List of dictionaries with parameter values
    :return: None
    """
    ks = [
        "SpeciesID",
        "NPP / GPPRatio",
        "gsmax(ms - 1)",
        "gsmin(ms - 1)",
        "CanopyQuantumEffic(gCJ - 1)",
        "MaxForestAge(yrs)",
        "OptimalTemp(C)",
        "MaxTemp(C)",
        "MinTemp(C)",
        "FoliageAllocCoef_a",
        "FoliageAllocCoef_b",
        "StemAllocCoef_a",
        "StemAllocCoef_b",
        "gs_light_coeff",
        "gs_vpd_coeff",
        "lwp_low(-m)",
        "lwp_high(-m)",
        "WiltingPnt",
        "SpecificLeafArea(m2g - 1)",
        "SpecificRootArea(m2kg - 1)",
        "Crown2StemDRat",
        "TreeShapeParam",
        "WoodDens(gCm - 2)",
        "Fhdmax",
        "Fhdmin",
        "LeafTurnoverRate(s - 1)",
        "MaxLeafTurnoverWaterStress(s - 1)",
        "LeafTurnoverWaterStressParam",
        "MaxLeafTurnoverTempStress(s - 1)",
        "LeafTurnoverTempStressParam",
        "ColdStressParam(degC)",
        "RootTurnoverRate(s - 1)",
        "MaxCanStorageParam(m)",
        "albedo",
        "emissivity",
        "KBeers",
        "CanopyWatEffic(gCm - 1)",
        "sperry_d_par(m)",
        "sperry_c_par(m)",
        "Sperry_Kp(ms - 1)",
        "gsr_param_a",
        "is_grass",
        "DeadGrassLeafTurnoverRate(s - 1)",
        "DeadGrassLeafTurnoverTempAdjustment(degC)"]

    with open(fn, mode='w') as file:
        file.write(str(len(dictParVals))+"\t 44\n")
        for s in dictParVals:
            for i in ks:
                file.write('{}\t'.format(s[i]))
            file.write("\n")
        file.write("numSpecs    NumParam\n")
        for i in ks:
            file.write('{}\t'.format(i))



if __name__ == "__main__":
    ks = [
        "SpeciesID",
        "NPP / GPPRatio",
        "gsmax(ms - 1)",
        "CanopyQuantumEffic(gCJ - 1)",
        "MaxForestAge(yrs)",
        "OptimalTemp(C)",
        "MaxTemp(C)",
        "MinTemp(C)",
        "FoliageAllocCoef_a",
        "FoliageAllocCoef_b",
        "StemAllocCoef_a",
        "StemAllocCoef_b",
        "gs_light_coeff",
        "gs_vpd_coeff",
        "lwp_low(-m)",
        "lwp_high(-m)",
        "WiltingPnt",
        "SpecificLeafArea(m2g - 1)",
        "SpecificRootArea(m2kg - 1)",
        "Crown2StemDRat",
        "TreeShapeParam",
        "WoodDens(gCm - 2)",
        "Fhdmax",
        "Fhdmin",
        "LeafTurnoverRate(s - 1)",
        "MaxLeafTurnoverWaterStress(s - 1)",
        "LeafTurnoverWaterStressParam",
        "MaxLeafTurnoverTempStress(s - 1)",
        "LeafTurnoverTempStressParam",
        "ColdStressParam(degC)",
        "RootTurnoverRate(s - 1)",
        "MaxCanStorageParam(m)",
        "albedo",
        "emissivity",
        "KBeers",
        "CanopyWatEffic(gCm - 1)",
        "sperry_d_par(m)",
        "sperry_c_par(m)",
        "Sperry_Kp(ms - 1)",
        "gsr_param_a",
        "is_grass",
        "DeadGrassLeafTurnoverRate(s - 1)",
        "DeadGrassLeafTurnoverTempAdjustment(degC)"]

    vals = [
        1, 0.47, 0.0167, 0.0018, 290, 7, 30, 0, 2.24, 0.006, 3.30E+00, 6.00E-07, 25, 0.0003,
        250, 30, 0.05, 0.009, 0.022, 0.25, 0.4, 220000, 15, 5, 8.56E-09, 1.80E-08, 0.2,
        1.80E-08, 0.2, 1, 5.34E-09, 6.24E-05, 0.1, 0.95, 0.55, 800, 200, 4, 1.17E-06, 8, 0,
        0, 0]
    pars = dict(zip(ks, vals))
    print pars
    writeVegetParamTab('test.tab', [pars])
