import os
os.environ["OMP_NUM_THREADS"] = "1"
import mapio
from customDist import logp_gmix
import pymc3 as mc
from subprocess import call, check_call
import writeVegetParamTab
import theano.tensor as T
from theano import as_op
from scipy import optimize 
import numpy as np
import pandas as pd
from shutil import copyfile, rmtree
import pickle
import math

dfSimLWP = pd.read_csv('Mean_LWP.csv',header=None,delimiter=',',names=['id','site1'])
dfSimLWP.index = pd.to_datetime(dfSimLWP['id'])

#dfSimLWP2 = dfSimLWP.resample('T').asfreq().interpolate()
#dfLWP_all = dfPoint_obs.join(dfSimLWP2,how='inner')

sperryb = 7.84436 #PLC= 1 - e^(-(-Psi/b))^c
sperryc = 1.48807

dfSimLWP['PLC'] = 1.0 - math.e ** (-((-dfSimLWP['site1']/ sperryb) ** sperryc))


                        


### Define Model
def compiled_DS(dfSimulated,PLC_init,PLC_crit,porporatok,r):
        ''' This function takes a timeseries of LWP and convered PLC, and runs the porporato framework exporting a single pandas column of DSI'''

	dfSimulated['boolean'] = np.where(dfSimulated['PLC']>=PLC_init,1.0,0.0)#Change this later

        #This routine selects days where droughts starts and sets them to boolean true. A drought here is devined as 3 consecutive days wiht stress.
        dfDroughtfreq = dfSimulated.copy()
        DroughtDays = np.asarray(dfDroughtfreq.boolean)
        DroughtStart = np.zeros(len(DroughtDays))

        for i in np.arange(1,len(DroughtDays)-1):
            if DroughtDays[i] == 1 and DroughtDays[i-1]<0.5 and DroughtDays[i+1]==1:
                DroughtStart[i]=1

	## Static Stress
	dfSimulated['Static_Stress'] = (PLC_init - dfSimulated['PLC'])/(PLC_init - PLC_crit)
	dfSimulated['Static_Stress'] = np.where(dfSimulated['Static_Stress'] >= 0, dfSimulated['Static_Stress'],0)
	dfSimulated['Static_Stress'] = np.where(dfSimulated['Static_Stress'] <= 1, dfSimulated['Static_Stress'],1)

        Dynamic_Stress_array = np.empty(len(dfSimulated))
        Tseas = 160
	for i in np.arange(1,len(dfSimulated)+1):
                Stress_Sum = dfSimulated['Static_Stress'][0:i].sum()
                Days_of_Stress = dfSimulated['boolean'][:i].sum()
                if Days_of_Stress == 0:
			weightedaverage = 0
			Dynamic_Stress = 0
		else:
			weightedaverage = (Stress_Sum/(Days_of_Stress))
			#N = DroughtStart[0:i].sum()
			#if N == 0:
			N = 1
                	T = Days_of_Stress/N
       		 
	        	Dynamic_Stress = (weightedaverage*T/(porporatok*Tseas)) ** (N**(-r))
		Dynamic_Stress_array[i-1] = Dynamic_Stress 
	
	##This portion needs to be activated if outputing actually DSI values.
	#dfSimulated['DSI'] = np.where(Dynamic_Stress_array <= 1, Dynamic_Stress_array,1)
	
	##This portion needs to be activated when outputing DSI vaules for an ensamble run. It prevents the piecewise function from disrupting the calibration. 
	dfSimulated['DSI'] = Dynamic_Stress_array
	
        return dfSimulated['DSI'] ## Return the points for the mcmc, and the dfSimulated natural for the posteriorprocessing.




# Default vegetation parameter values
key_pars = ['PLC_init','PLC_crit','Por_k','Por_r','sd1']
init_pars = [0.3,0.5,0.20,.5,.01]
start_pars = dict(zip(key_pars, init_pars))



count = 1
@as_op(itypes=[T.dscalar]*5, otypes=[T.dvector])
def theta(PLC_init,
	PLC_crit,
	Por_k,
	Por_r,
	sd1):
    global count
    #print "Number of model calls ", count
    count+=1
    
    # Do some checks on parameters  
    PLC_init = max(PLC_init,0.0)
    PLC_init = min(PLC_init,PLC_crit)  
    #PLC_crit = max(PLC_crit,0.0)
    PLC_crit = min(PLC_crit,1)
    Por_k = max(Por_k,0.0)
    Por_r = max(Por_r,0.0)
    

    log = os.tempnam('./','')+".txt"

    with open(log, 'w') as f:
           call(['ls'], stdout=f)



#Call the actual model
    #try:
    Output = compiled_DS(dfSimLWP,PLC_init,PLC_crit,Por_k,Por_r)
        
	#print 'Run Success'            
	
    #except:
    #    print "Run failed!"    


    os.remove(log)    
    # postrocess and return the data
    if str(count)[-3:]=='000':
	   print 'Number of model calls ',count
 	   print "Parameter values, ", PLC_init,PLC_crit,Por_k,Por_r
 
    #Match lengths of modeled and observed.
    #print Output.values
    return Output.values



model = mc.Model()
with model:

    # Parameter Priors 
    PLC_init = mc.Normal('PLC_init', mu=0.3, sd=0.01)
    PLC_crit = mc.Normal('PLC_crit', mu=0.5, sd=0.01)
    Por_k = mc.Normal('Por_k', mu=0.2, sd=0.01)
    Por_r = mc.Normal('Por_r', mu=.5, sd=0.2)


    
    sd1 = mc.HalfNormal('sd1',sd=.01)
    
    vlist = [PLC_init,PLC_crit,Por_k,Por_r,sd1]
    
    Outputs = theta(*vlist)
    means = T.concatenate([Outputs])
    
    
    sd = T.ones_like(Outputs)*sd1
    #sd = T.concatenate((p1))

    observations = sd #np.asarray(dfPoint_obs['P_mortality'])
            
    #print(observations)
    #print(means.tag.test_value)
    obs = mc.Normal('obs', mu=means, sd=sd,
                        observed=observations)




    trace = mc.backends.text.load('trace_results')
    trace = trace[500:]    
#    model_id = 0
#    with open('model_{}.pkl'.format(model_id), 'wb') as buff:
#        pickle.dump({'model': model, 'trace': trace}, buff)
    
    print "Starting postserior predictions"
    postPred = mc.sample_ppc(trace)
    
    with open('postPred2.txt', 'w') as buff:
        pickle.dump(postPred, buff) 
    #backend = mc.backends.Text('trace_results')
    #trace = mc.sample(draws=500, start=start_pars, init='ADVI', njobs=1, step=step, trace=backend)
    

    


