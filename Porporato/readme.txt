This is the folder for Greenhouse Porporato analysis and figure development for my thesis.

The Monte_Carlo folder contains infromation pertinant to monte-carlo calibration for the Greenhouse Porporato Framework. The majority of operable files are in the Postprocess folder. mcmc3.py is the primary monte-carlo script. mcmc3_gradient.py is a varient which solve a find_MAP optimization route for the data. posteriorProccessing.py re runs the monte-carlo chains to produce data which is eventually used in the Postprocessing portion.

Run Order

1../Monte_Carlo/Postprocess/	python monte_carlo_run.py
2../Monte_Carlo/Postprocess/    python monte_carlo_posterior_processing.py
3../Analysis	./updateFiles.sh
4.Analysis	 python Analysis.py

Last Edited: 04-17-18

-Caelan Simeone

