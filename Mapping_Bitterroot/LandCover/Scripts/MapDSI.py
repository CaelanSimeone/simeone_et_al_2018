import matplotlib.pyplot as plt
import cartopy.feature as cfeature
import numpy as np
import cartopy.io.img_tiles as cimgt
import cartopy.crs as ccrs
import os
import pcraster as pcr
#os.system('gdalinfo ./PREF_dem_NOBUFFER.tif')


def scale_bar(ax, length, location=(0.4, 0.05), linewidth=3):
    """
    ax is the axes to draw the scalebar on.
    location is center of the scalebar in axis coordinates ie. 0.5 is the middle of the plot
    length is the length of the scalebar in km.
    linewidth is the thickness of the scalebar.
    """
    #Projection in metres, need to change this to suit your own figure
    proj = ccrs.AlbersEqualArea(central_longitude=-96,
                central_latitude=23,standard_parallels=(29.5,45.5)) 
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(proj)
    #Turn the specified scalebar location into coordinates in metres
    sbcx, sbcy = x0 + (x1 - x0) * location[0], y0 + (y1 - y0) * location[1]
    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbcx - length * 500, sbcx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sbcy, sbcy], transform=proj, color='k', linewidth=linewidth)
    #Plot the scalebar label
    ax.text(sbcx, sbcy, str(length) + ' km', transform=proj,
            horizontalalignment='center', verticalalignment='bottom')



def main(year):

	lc = pcr.readmap('../Maps/DSI_masked.map')
	LC = pcr.pcr2numpy(lc, 9999)
	LC[LC==9999]=np.nan	

	unit = pcr.readmap('../Maps/unit_WS.map')
	Unit = pcr.pcr2numpy(unit,9999)
	Unit[Unit==9999]=np.nan

	mask = pcr.readmap('../Maps/FireMask_WS.map')
	Mask = pcr.pcr2numpy(mask,9999)
	Mask[Mask==9999]=np.nan
    	
	slice_val_xmin = 200
	slice_val_xmax = 75
	slice_val_ymin = 300
	slice_val_ymax = 50

	LC = LC[slice_val_ymax:-slice_val_ymin, slice_val_xmin:-slice_val_xmax]
	Unit = Unit[slice_val_ymax:-slice_val_ymin, slice_val_xmin:-slice_val_xmax]
	Mask = Mask[slice_val_ymax:-slice_val_ymin, slice_val_xmin:-slice_val_xmax]
        
	
	proj = ccrs.AlbersEqualArea(central_longitude=-96,
                    central_latitude=23,standard_parallels=(29.5,45.5))
    	ax = plt.axes(projection=proj)
	ax.outline_patch.set_visible(False)	
   
	minx = -1446763 + slice_val_xmin*250
	maxy = 2732249 - slice_val_ymax*250
	maxx = -1340013 - slice_val_xmax*250
	miny = 2615749 + slice_val_ymin*250
	
	x = np.linspace(minx,maxx,LC.shape[1])
	y = np.linspace(miny,maxy,LC.shape[0])
	xx, yy = np.meshgrid(x,y)
	LCdata = np.flipud(LC)	
    	Maskdata = np.flipud(Mask)
	Unitdata = np.flipud(Unit)
	colormesh = ax.pcolor(xx, yy, Unitdata,
    	            	transform=proj,
    	            	cmap='Pastel2',linewidth=0,vmin=0,vmax=.1)
    	colormesh = ax.pcolor(xx, yy, Maskdata,
    	            	transform=proj,
    	            	cmap='Pastel2',linewidth=0,vmin=.1,vmax=.101)
	colormesh = ax.pcolor(xx, yy, LCdata,
    	            	transform=proj,
    	            	cmap='summer',linewidth=0,vmin=.5,vmax=.5001)
    
	scale_bar(ax,10)
	#plt.colorbar(colormesh, fraction =0.02,pad=0.04)
	plt.savefig('../Figures/BitterrootDSI.png')
	#plt.show()
	plt.close()
#if __name__ == '__main__':
#    main()

main('2004')
