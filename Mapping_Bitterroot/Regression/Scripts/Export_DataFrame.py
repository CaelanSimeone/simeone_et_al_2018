# Run Regression on Maps

import matplotlib.pylab as plt	
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.interpolate import UnivariateSpline

Variables = ['DEM','TPI','VPD','VWC','DynamicStress','Sdown','LandCover','Min_DynamicStress','Precip']
dfData = pd.DataFrame(columns = Variables)
for Variable_name in Variables:
	Variable_pcr = pcr.readmap('../Maps/'+Variable_name+'.map')
	Variable = pcr.pcr2numpy(Variable_pcr, 9999)
	#Variable[Variable==9999]=np.nan
	dfData[Variable_name] = Variable.flatten()

dfData = dfData[dfData.DEM != 9999]
print dfData
	
dfData.to_csv('Darby_Dataframe.csv')
