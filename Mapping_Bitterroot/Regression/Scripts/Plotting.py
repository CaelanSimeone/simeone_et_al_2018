# Run Regression on Maps

import matplotlib.pylab as plt	
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.interpolate import UnivariateSpline

#Variables = ['DEM','TPI','LandCover2015','VPD','VWC','DynamicStress','Sdown']
#dfData = pd.DataFrame(columns = Variables)
#for Variable_name in Variables:
#	Variable_pcr = pcr.readmap('../Maps/'+Variable_name+'.map')
#	Variable = pcr.pcr2numpy(Variable_pcr, 9999)
#	#Variable[Variable==9999]=np.nan
#	dfData[Variable_name] = Variable.flatten()
#
dfData = pd.read_csv('Darby_Dataframe.csv')

dfData = dfData[dfData.DynamicStress <= 1]
#dfData = dfData[dfData.LandCover <= .3]
#dfData = dfData[dfData.DynamicStress >= .3]
dfData = dfData[dfData.DEM >= 0]

dfData = dfData.sample(len(dfData))

params = {'legend.fontsize': 'x-large',
          'figure.figsize': (7, 5),
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
plt.rcParams.update(params)
# Plot...
plt.scatter(dfData.Precip, dfData.VWC, c=dfData.DynamicStress, s=5,cmap='jet',alpha = .25,vmin=0.)
plt.colorbar()
plt.xlabel('Elevation (m)')
plt.ylabel('Solar Radiation ($W/m^2$)')
plt.tight_layout()
plt.show()
