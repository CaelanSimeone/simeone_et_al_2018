#Run Regression on Maps

import matplotlib.pylab as plt	
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.interpolate import UnivariateSpline
import scipy.stats

fig, axarr = plt.subplots(3,3)

def Binned_Regression(X_Variable_name,Y_Variable_name):
	print X_Variable_name
	if X_Variable_name == 'pass':
		return	
	Y_Variable_pcr = pcr.readmap('../Maps/'+Y_Variable_name+'.map')
	Y_Variable = pcr.pcr2numpy(Y_Variable_pcr, 9999)
	Y_Variable[Y_Variable==9999]=np.nan
	
	X_Variable_pcr = pcr.readmap('../Maps/'+X_Variable_name+'.map')
	X_Variable = pcr.pcr2numpy(X_Variable_pcr, 9999)
	X_Variable[X_Variable==9999]=np.nan


	Elevation_pcr = pcr.readmap('../Maps/TPI.map')
	Elevation = pcr.pcr2numpy(Elevation_pcr, 9999)
	Elevation[Elevation==9999]=np.nan
	
	
	dfData = pd.DataFrame({'X_Variable':X_Variable.flatten(),'Y_Variable':Y_Variable.flatten(),'Elevation':Elevation.flatten()})

	dfData = dfData.sort_values(['X_Variable'],ascending=True)
	dfData = dfData.dropna(axis=0,how='any')
	#dfData = dfData[dfData.Elevation <=-50]
	#dfData = dfData[dfData.Elevation <=50]

	Data_X_list = np.array_split(np.asarray(dfData.X_Variable),100)
	Data_Y_list = np.array_split(np.asarray(dfData.Y_Variable),100)

	X_means = [np.mean(x) for x in Data_X_list]
	Y_means = [np.mean(y) for y in Data_Y_list]
	Y_median = [np.percentile(y,50) for y in Data_Y_list]	
	Y_bottom_mean = np.nan_to_num([np.mean(y[np.where(y < median)]) for y,median in zip(Data_Y_list, Y_median)])
	Y_top_mean = [np.mean(y[np.where(y > median)]) for y,median in zip(Data_Y_list, Y_median)] 
	 
	Y_25 = [np.percentile(y,10) for y in Data_Y_list]
	Y_75 = [np.percentile(y,90) for y in Data_Y_list]

	Color = 'grey'

	
	Y_label = Y_Variable_name		
	
	if Y_Variable_name == 'DynamicStress':
	#	Color = 'green'
		Y_label = 'Dynamic Stress'
	if Y_Variable_name == 'VPD':
		Color = 'red'
		Y_label = 'Mean VPD ($MPa$)'
	if Y_Variable_name == 'VWC':
		Color = 'blue'
		Y_label = 'Mean VWC ($m^3/m^3$)'

	X_label = X_Variable_name		
	if X_Variable_name == 'DEM':
		X_label = 'Elevation ($m$)'
	if X_Variable_name == 'Sdown':
		X_label = 'Sdown ($W/m^3$)'
	if X_Variable_name == 'slope':
		X_label = 'Slope ($degrees$)'
	if X_Variable_name == 'TPI':
		X_label = 'Topographic Position Index'
	if X_Variable_name == 'LandCover':
		X_label = 'Forest Cover'
	if X_Variable_name == 'VPD':
		X_label = 'Mean VPD ($KPa$)'
	if X_Variable_name == 'VWC':
		X_label = 'Mean VWC ($m^3/m^3$)'
	if X_Variable_name == 'Temp':
		X_label = 'Temperature ($C^o$)'
	if X_Variable_name == 'Precip':
		X_label = 'Precipitation ($m$)'
	if X_Variable_name == 'SWE':
		X_label = 'Maximum Snow Water Equivalent ($m$)'

	axarr[countx,county].fill_between(X_means,Y_25,Y_75,alpha=.5,color=Color)
	axarr[countx,county].plot(X_means,Y_25,color=Color)
	axarr[countx,county].plot(X_means,Y_75,color=Color)
	axarr[countx,county].plot(X_means,Y_median,'o',color='black',mfc='white')	
	axarr[countx,county].set_xlabel(X_label)
	axarr[countx,county].set_ylabel(Y_label)
		
	xarray,yarray=np.asarray(dfData.X_Variable),np.asarray(dfData.Y_Variable)
	mask = ~np.isnan(xarray) & ~np.isnan(yarray)
	slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(xarray,yarray)
	x=np.linspace(dfData.X_Variable.min(),dfData.X_Variable.max(),10)
	axarr[countx,county].plot(x,intercept+x*slope,label='R2 ='+str(round(r_value**2,3))+' p='+str(round(p_value,5)))
	axarr[countx,county].legend(loc=0)

countx = 0
county = 0
for X in ['DEM','Temp','VPD','TPI','Precip','VWC','Sdown','SWE','LandCover']:
	
	if countx == 3:
		county += 1
		countx = 0
	#axarr[countx,0].set_xlabel(X)	
	for Y in ['TPI']:
		Binned_Regression(X,Y)
		#axarr[0,county].set_title(Y)
	countx += 1

fig.set_size_inches(8,6)
plt.tight_layout()
plt.savefig('../Figures/Regression.png')
plt.show()



