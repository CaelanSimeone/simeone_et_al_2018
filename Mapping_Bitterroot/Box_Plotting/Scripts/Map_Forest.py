import matplotlib.pyplot as plt
import cartopy.feature as cfeature
import numpy as np
import cartopy.io.img_tiles as cimgt
import cartopy.crs as ccrs
import os
import pcraster as pcr
#os.system('gdalinfo ./PREF_dem_NOBUFFER.tif')


def scale_bar(ax, length, location=(0.3, 0.2), linewidth=3):
    """
    ax is the axes to draw the scalebar on.
    location is center of the scalebar in axis coordinates ie. 0.5 is the middle of the plot
    length is the length of the scalebar in km.
    linewidth is the thickness of the scalebar.
    """
    #Projection in metres, need to change this to suit your own figure
    proj = ccrs.AlbersEqualArea(central_longitude=-96,
                central_latitude=23,standard_parallels=(29.5,45.5)) 
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(proj)
    #Turn the specified scalebar location into coordinates in metres
    sbcx, sbcy = x0 + (x1 - x0) * location[0], y0 + (y1 - y0) * location[1]
    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbcx - length * 500, sbcx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sbcy, sbcy], transform=proj, color='k', linewidth=linewidth)
    #Plot the scalebar label
    ax.text(sbcx, sbcy, str(length) + ' km', transform=proj,
            horizontalalignment='center', verticalalignment='bottom')



def main(Map):

	dem = pcr.readmap('../Data/'+Map+'.map')
	DEM = pcr.pcr2numpy(dem, 9999)
	DEM[DEM==9999]=np.nan	

	slice_val_xmin = 225 #Left
	slice_val_xmax = 150 #Right
	slice_val_ymin = 340 #Bottom
	slice_val_ymax = 70  #Top

	DEM = DEM[slice_val_ymax:-slice_val_ymin, slice_val_xmin:-slice_val_xmax]
        
    	stamen_terrain = cimgt.StamenTerrain()
	proj = ccrs.AlbersEqualArea(central_longitude=-96,
                    central_latitude=23,standard_parallels=(29.5,45.5))
    	ax = plt.axes(projection=proj)
	ax.outline_patch.set_visible(False)	
   
    	ax.add_image(stamen_terrain,12)
	
	minx = -1446763 + slice_val_xmin*250
	maxy = 2732249 - slice_val_ymax*250
	maxx = -1340013 - slice_val_xmax*250
	miny = 2615749 + slice_val_ymin*250
	
	x = np.linspace(minx,maxx,DEM.shape[1])
	y = np.linspace(miny,maxy,DEM.shape[0])
	xx, yy = np.meshgrid(x,y)
	data = np.flipud(DEM)	
    	if Map == 'DynamicStress':	
		colormesh = ax.pcolor(xx, yy, data,
    	            	transform=proj,
    	            	cmap='summer',linewidth=0,vmin=.51,vmax=.51001,alpha=.5)
    	elif Map == 'Min_DynamicStress':	
		colormesh = ax.pcolor(xx, yy, data,
    	            	transform=proj,
    	            	cmap='summer',linewidth=0,vmin=.28,vmax=.28001,alpha=.5)
	elif Map == 'LandCover':
		colormesh = ax.pcolor(xx, yy, data,
    	            	transform=proj,
    	            	cmap='summer_r',linewidth=0,vmin=.09999,vmax=.1,alpha=.5)
	elif Map == 'LandFire_250m':
		colormesh = ax.pcolor(xx, yy, data,
    	            	transform=proj,
    	            	cmap='summer_r',linewidth=0,vmin=0.1,vmax=1,alpha=.5)
   	else:
		print Map
		print 'Oh Shit somthing broke af' 
#
	scale_bar(ax,1)
	cbar = plt.colorbar(colormesh, fraction =0.02,pad=0.04)
	cbar.set_label(Map, rotation=270)
	plt.savefig('../Figures/'+Map+'Forest.png',transparent=True)
	plt.show()
	plt.close()
#if __name__ == '__main__':
#    main()

Variables =  ['LandFire_250m','LandCover','DynamicStress','Min_DynamicStress']
for i in Variables:
	main(i)

