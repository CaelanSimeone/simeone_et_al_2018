import os
import pcraster as pcr
import numpy as np
from pcraster import *
import pandas as pd

os.chdir('Incoming_Data')
os.system('cp vcf_2015.map LandCover2015.map')

##Calculate Averages
os.system("pcrcalc 'DynamicStress.map = (Dynamicstress2001.map + Dynamicstress2002.map + Dynamicstress2003.map + Dynamicstress2004.map + Dynamicstress2005.map + Dynamicstress2006.map+ Dynamicstress2007.map + Dynamicstress2008.map + Dynamicstress2009.map+Dynamicstress2010.map + Dynamicstress2011.map + Dynamicstress2012.map+Dynamicstress2013.map + Dynamicstress2014.map + Dynamicstress2015.map)/15'")

os.system("pcrcalc 'LWP.map = (Mean_summer_midday2001.map + Mean_summer_midday2002.map + Mean_summer_midday2003.map + Mean_summer_midday2004.map + Mean_summer_midday2005.map + Mean_summer_midday2006.map+ Mean_summer_midday2007.map + Mean_summer_midday2008.map + Mean_summer_midday2009.map+Mean_summer_midday2010.map + Mean_summer_midday2011.map + Mean_summer_midday2012.map+Mean_summer_midday2013.map + Mean_summer_midday2014.map + Mean_summer_midday2015.map)/15'")

os.system("pcrcalc 'PLC.map = (Mean_summer_PLC2001.map + Mean_summer_PLC2002.map + Mean_summer_PLC2003.map + Mean_summer_PLC2004.map + Mean_summer_PLC2005.map + Mean_summer_PLC2006.map+ Mean_summer_PLC2007.map + Mean_summer_PLC2008.map + Mean_summer_PLC2009.map+Mean_summer_PLC2010.map + Mean_summer_PLC2011.map + Mean_summer_PLC2012.map+Mean_summer_PLC2013.map + Mean_summer_PLC2014.map + Mean_summer_PLC2015.map)/15'")

os.system("pcrcalc 'Transp.map = (Mean_Transpiraiton2001.map + Mean_Transpiraiton2002.map + Mean_Transpiraiton2003.map + Mean_Transpiraiton2004.map + Mean_Transpiraiton2005.map + Mean_Transpiraiton2006.map+ Mean_Transpiraiton2007.map + Mean_Transpiraiton2008.map + Mean_Transpiraiton2009.map+Mean_Transpiraiton2010.map + Mean_Transpiraiton2011.map + Mean_Transpiraiton2012.map+Mean_Transpiraiton2013.map + Mean_Transpiraiton2014.map + Mean_Transpiraiton2015.map)/15'")

os.system("pcrcalc 'Tseason.map = (Tseason_2001.map + Tseason_2002.map + Tseason_2003.map+Tseason_2004.map + Tseason_2005.map + Tseason_2006.map+ Tseason_2007.map + Tseason_2008.map + Tseason_2009.map+Tseason_2010.map + Tseason_2011.map + Tseason_2012.map+Tseason_2013.map + Tseason_2014.map + Tseason_2015.map)/15'")

os.system("pcrcalc 'VPD.map = (VPD2001.map + VPD2002.map + VPD2003.map+VPD2004.map + VPD2005.map + VPD2006.map+ VPD2007.map + VPD2008.map + VPD2009.map+VPD2010.map + VPD2011.map + VPD2012.map+VPD2013.map + VPD2014.map + VPD2015.map)/15'")

os.system("pcrcalc 'Temp.map = (Temp2001.map + Temp2002.map + Temp2003.map+Temp2004.map + Temp2005.map + Temp2006.map+ Temp2007.map + Temp2008.map + Temp2009.map+Temp2010.map + Temp2011.map + Temp2012.map+Temp2013.map + Temp2014.map + Temp2015.map)/15'")

os.system("pcrcalc 'Precip.map =(86400/8)* (Precip2001.map + Precip2002.map + Precip2003.map+Precip2004.map + Precip2005.map + Precip2006.map+ Precip2007.map + Precip2008.map + Precip2009.map+Precip2010.map + Precip2011.map + Precip2012.map+Precip2013.map + Precip2014.map + Precip2015.map)/15'")

os.system("pcrcalc 'SWE.map = (SWE2001.map + SWE2002.map + SWE2003.map+SWE2004.map + SWE2005.map + SWE2006.map+ SWE2007.map + SWE2008.map + SWE2009.map+SWE2010.map + SWE2011.map + SWE2012.map+SWE2013.map + SWE2014.map + SWE2015.map)/15'")

os.system("pcrcalc 'Sdown.map = (Sdown2001.map + Sdown2002.map + Sdown2003.map+Sdown2004.map + Sdown2005.map + Sdown2006.map+ Sdown2007.map + Sdown2008.map + Sdown2009.map+Sdown2010.map + Sdown2011.map + Sdown2012.map+Sdown2013.map + Sdown2014.map + Sdown2015.map)/15'")

os.system("pcrcalc 'VWC.map = (VWC2001.map + VWC2002.map + VWC2003.map+VWC2004.map + VWC2005.map + VWC2006.map+ VWC2007.map + VWC2008.map + VWC2009.map+VWC2010.map + VWC2011.map + VWC2012.map+VWC2013.map + VWC2014.map + VWC2015.map)/15'")

###Make min
forest_cover = pcr.readmap('vcf_2015.map')
Forest = pcr.pcr2numpy(forest_cover,9999)
Forest_Cover = Forest.flatten()

forest_fire = pcr.readmap('LandFire_250m.map')
Forest_Fire = pcr.pcr2numpy(forest_fire,9999).flatten()

dfData = pd.DataFrame(data = {'Forest_Cover':Forest_Cover,'Land_Fire':Forest_Fire})

Years =np.arange(2001,2016,1).astype('str')
for year in Years:
	raster = pcr.readmap('Dynamicstress'+year+'.map')
	dfData[year] = pcr.pcr2numpy(raster, 9999).flatten()


dfData['Min'] = dfData[Years].min(axis=1)
DSI_Min = np.asarray(dfData['Min'])

DSI_Min = DSI_Min.reshape(Forest.shape)
Min_pcr = pcr.numpy2pcr(Scalar, DSI_Min, 9999)
pcr.report(Min_pcr, 'Min_DynamicStress.map')

#Delineate Watershed
os.system("pcrcalc 'unit.map = DEM.map/DEM.map'")
os.system('col2map --clone DEM.map probes.txt Tsmask.map')
os.system("pcrcalc 'Points.map = ordinal(Tsmask.map)'")
os.system("pcrcalc 'Catchment.map = catchment(ldd.map,Points.map)'")
os.system("pcrcalc 'Catchment_Scalar.map = scalar(Catchment.map)'")

#Clip off areas outside of watershed.
catchment = pcr.readmap('Catchment_Scalar.map')
Catchment = pcr.pcr2numpy(catchment, 0)
print Catchment.sum()
catchment_pcr = pcr.numpy2pcr(Scalar, Catchment, 0)
pcr.report(catchment_pcr, 'CleanCatchment.map')

os.system("pcrcalc 'unit_WS.map = (CleanCatchment.map/CleanCatchment.map)'")

####Make Small Maps
os.system("pcrcalc 'VWC_WS.map = unit_WS.map* VWC.map'")
os.system("pcrcalc 'DynamicStress_WS.map = unit_WS.map* DynamicStress.map'")
os.system("pcrcalc 'Min_DynamicStress_WS.map = unit_WS.map* Min_DynamicStress.map'")
os.system("pcrcalc 'VPD_WS.map = unit_WS.map* VPD.map'")
os.system("pcrcalc 'DEM_WS.map = unit_WS.map* DEM.map'")
#os.system("pcrcalc 'TPI_WS.map = unit_WS.map* TPI.map'")
os.system("pcrcalc 'Sdown_WS.map = unit_WS.map* Sdown.map'")
os.system("pcrcalc 'SWE_WS.map = unit_WS.map* SWE.map'")
os.system("pcrcalc 'Precip_WS.map = unit_WS.map* Precip.map'")
os.system("pcrcalc 'Temp_WS.map = unit_WS.map* Temp.map'")
os.system("pcrcalc 'LandCover2015_WS.map = unit_WS.map* LandCover2015.map'")
#os.system("pcrcalc 'LandFire_250m_WS.map = unit_WS.map* LandFire_250m.map'")
os.system("pcrcalc 'pvalue_15_WS.map = unit_WS.map* pvalue_15.map'")
os.system("pcrcalc 'FireMask_WS.map = unit_WS.map* FireMask.map'")

### Make Chan0.map
os.system("pcrcalc 'unit.map = DEM.map/DEM.map'")
os.system("pcrcalc 'chan0.map = unit.map - chanmask.map'")
os.system("pcrcalc 'chan0.map = chan0.map/chan0.map'")
os.system("pcrcalc 'chan0_WS.map = unit_WS.map* chan0.map'")


#### Clip Small WS
DSI_pcr = pcr.readmap('DynamicStress.map')
DSI = pcr.pcr2numpy(DSI_pcr, 9999)
DSI[DSI==9999]=numpy.nan

LC_pcr = pcr.readmap('vcf_2015.map')
LC = pcr.pcr2numpy(LC_pcr, 9999)
LC[LC==9999]=numpy.nan

LF_pcr = pcr.readmap('LandFire_250m.map')
LF = pcr.pcr2numpy(LF_pcr, 9999)
LF[LF==9999]=numpy.nan

FireMask_pcr = pcr.readmap('FireMask_250m.map')
FireMask = pcr.pcr2numpy(FireMask_pcr, -1)
FireMask_Full = FireMask.copy()
FireMask_Full[FireMask_Full<=0]=numpy.nan
FireMask[FireMask>=0]=numpy.nan

Tseas_pcr = pcr.readmap('Tseason.map')
Tseas = pcr.pcr2numpy(Tseas_pcr, 9999)
Tseas[Tseas<=150]=numpy.nan
Tseas = Tseas/Tseas

unit_pcr = pcr.readmap('unit_WS.map')
Unit = pcr.pcr2numpy(unit_pcr, 9999)
Unit[Unit==9999]=numpy.nan

unit_pcr = pcr.readmap('unit.map')
Unit_Full = pcr.pcr2numpy(unit_pcr, 9999)
Unit_Full[Unit_Full==9999]=numpy.nan

chan_pcr = pcr.readmap('chan0.map')
Channel = pcr.pcr2numpy(chan_pcr, 9999)
Channel[Channel==9999]=numpy.nan

### Masked Full Maps
LC_Full = LC * Tseas * Channel* (-1*FireMask)
LC_Full[np.isnan(LC_Full)] = 9999
LC_pcr = pcr.numpy2pcr(Scalar, LC_Full, 9999)
pcr.report(LC_pcr, 'LandCover_masked.map')

DSI_Full = DSI * Tseas * Channel* -1 * FireMask
DSI_Full[np.isnan(DSI_Full)] = 9999
DSI_pcr = pcr.numpy2pcr(Scalar, DSI_Full, 9999)
pcr.report(DSI_pcr, 'DynamicStress_masked.map')

LF_2 = LF.copy()
LF_2[LF_2==0]=numpy.nan
DSI_Min_Forest = DSI_Min * LF_2
DSI_Min_Forest[np.isnan(DSI_Min_Forest)] = 9999
DSI_Min_Forest_pcr = pcr.numpy2pcr(Scalar, DSI_Min_Forest, 9999)
pcr.report(DSI_Min_Forest_pcr, 'DynamicStress_Min_Forest.map')

DSI_Min = DSI_Min * Tseas * Channel*-1*FireMask
DSI_Min[np.isnan(DSI_Min)] = 9999
DSI_Min_pcr = pcr.numpy2pcr(Scalar, DSI_Min, 9999)
pcr.report(DSI_Min_pcr, 'DynamicStress_Min_masked.map')

LF_Full = LF * Tseas * Channel* (-1*FireMask)
LF_Full[np.isnan(LF_Full)] = 9999
LF_pcr = pcr.numpy2pcr(Scalar, LF_Full, 9999)
pcr.report(LF_pcr, 'LandFire_masked.map')

TPI_pcr = pcr.readmap('TPI.map')
TPI = pcr.pcr2numpy(TPI_pcr, 9999)
TPI = TPI * Unit
TPI[np.isnan(TPI)] = 9999
TPI_pcr = pcr.numpy2pcr(Scalar, TPI, 9999)
pcr.report(TPI_pcr, 'TPI_WS.map')

#### Plain Watershed Maps
LC = LC * Unit
LC[np.isnan(LC)] = 9999
LC_pcr = pcr.numpy2pcr(Scalar, LC, 9999)
pcr.report(LC_pcr, 'LC.map')

DSI = DSI * Unit
DSI[np.isnan(DSI)] = 9999
DSI_pcr = pcr.numpy2pcr(Scalar, DSI, 9999)
pcr.report(DSI_pcr, 'DSI.map')

Tseas_WS = Tseas * Unit
Tseas_WS[np.isnan(Tseas_WS)] = 9999
Tseas_pcr = pcr.numpy2pcr(Scalar, Tseas_WS, 9999)
pcr.report(Tseas_pcr, 'Tseason_WS.map')

FireMask_WS = FireMask_Full * Unit_Full
FireMask_WS[np.isnan(FireMask_WS)] = 9999
FireMask_pcr = pcr.numpy2pcr(Scalar, FireMask_WS, 9999)
pcr.report(FireMask_pcr, 'FireMask.map')

##### Masked Watershed Maps
LC = LC * Tseas * Unit * Channel* -1 * FireMask
LC[np.isnan(LC)] = 9999
LC_pcr = pcr.numpy2pcr(Scalar, LC, 9999)
pcr.report(LC_pcr, 'LC_masked.map')

DSI = DSI * Tseas * Unit * Channel*-1*FireMask
DSI[np.isnan(DSI)] = 9999
DSI_pcr = pcr.numpy2pcr(Scalar, DSI, 9999)
pcr.report(DSI_pcr, 'DSI_masked.map')

DSI_Min = DSI_Min * Tseas * Unit * Channel*-1*FireMask
DSI_Min[np.isnan(DSI_Min)] = 9999
DSI_Min_pcr = pcr.numpy2pcr(Scalar, DSI_Min, 9999)
pcr.report(DSI_Min_pcr, 'DSI_Min_masked.map')

LF = LF * Tseas * Unit * Channel* -1 * FireMask
LF[np.isnan(LF)] = 9999
LF_pcr = pcr.numpy2pcr(Scalar, LF, 9999)
pcr.report(LF_pcr, 'LF_masked.map')



#### Copy to LandCover
os.system('cp DSI_masked.map ../../LandCover/Maps/')
os.system('cp LC_masked.map ../../LandCover/Maps/')
os.system('cp Tseason_WS.map ../../LandCover/Maps/')
os.system('cp unit_WS.map ../../LandCover/Maps/')
os.system('cp DynamicStress_masked.map ../../LandCover/Maps/')
os.system('cp DynamicStress.map ../../LandCover/Maps/')
os.system('cp LandCover_masked.map ../../LandCover/Maps/')
os.system('cp LandFire_masked.map ../../LandCover/Maps/')
os.system('cp LF_masked.map ../../LandCover/Maps/')
os.system('cp LF.map ../../LandCover/Maps/')
os.system('cp FireMask.map ../../LandCover/Maps/')
os.system('cp FireMask_WS.map ../../LandCover/Maps/')
os.system('cp unit.map ../../LandCover/Maps/')
os.system('cp DynamicStress_Min_masked.map ../../LandCover/Maps/')
os.system('cp DynamicStress_Min_Forest.map ../../LandCover/Maps/')
os.system('cp DSI_Min_masked.map ../../LandCover/Maps/')

#### Copy to GLM
os.system('cp Dynamicstress20*.map ../../GLM/Maps/')
os.system('cp vcf*.map ../../GLM/Maps/')
os.system('cp unit.map ../../GLM/Maps/')
os.system('cp chan0.map ../../GLM/Maps/')
os.system('cp chan0_WS.map ../../GLM/Maps/')
os.system('cp Tseason.map ../../GLM/Maps/')
os.system('cp LandFire.map ../../GLM/Maps/')
os.system('cp LandFire_250m.map ../../GLM/Maps/')
os.system('cp FireMask_250m.map ../../GLM/Maps/')
os.system('cp LandCover.map ../../GLM/Maps/')

#### Copy to Bitterroot Maps
os.system('cp unit_WS.map ../../BitterrootMaps/Maps/')
os.system('cp DynamicStress_WS.map ../../BitterrootMaps/Maps/')
os.system('cp DynamicStress.map ../../BitterrootMaps/Maps/')
os.system('cp Min_DynamicStress_WS.map ../../BitterrootMaps/Maps/')
os.system('cp Min_DynamicStress.map ../../BitterrootMaps/Maps/')
os.system('cp VPD.map ../../BitterrootMaps/Maps/')
os.system('cp VWC.map ../../BitterrootMaps/Maps/')
os.system('cp VPD_WS.map ../../BitterrootMaps/Maps/')
os.system('cp VWC_WS.map ../../BitterrootMaps/Maps/')
os.system('cp Sdown.map ../../BitterrootMaps/Maps/')
os.system('cp DEM.map ../../BitterrootMaps/Maps/')
os.system('cp LWP.map ../../BitterrootMaps/Maps/')
os.system('cp PLC.map ../../BitterrootMaps/Maps/')
os.system('cp TPI.map ../../BitterrootMaps/Maps/')
os.system('cp Sdown_WS.map ../../BitterrootMaps/Maps/')
os.system('cp DEM_WS.map ../../BitterrootMaps/Maps/')
os.system('cp TPI_WS.map ../../BitterrootMaps/Maps/')
os.system('cp Temp_WS.map ../../BitterrootMaps/Maps/')
os.system('cp Precip_WS.map ../../BitterrootMaps/Maps/')
os.system('cp SWE_WS.map ../../BitterrootMaps/Maps/')
os.system('cp Temp.map ../../BitterrootMaps/Maps/')
os.system('cp Precip.map ../../BitterrootMaps/Maps/')
os.system('cp Transp.map ../../BitterrootMaps/Maps/')
os.system('cp SWE.map ../../BitterrootMaps/Maps/')
os.system('cp LandCover2015_WS.map ../../BitterrootMaps/Maps/')
os.system('cp LandCover2015.map ../../BitterrootMaps/Maps/')
os.system('cp pvalue_15.map ../../BitterrootMaps/Maps/')
os.system('cp pvalue_15_WS.map ../../BitterrootMaps/Maps/')
### Copy to Regression
os.system('cp VWC.map ../../Regression/Maps/')
os.system('cp VPD.map ../../Regression/Maps/')
os.system('cp DEM.map ../../Regression/Maps/')
os.system('cp DynamicStress.map ../../Regression/Maps/')
os.system('cp Min_DynamicStress.map ../../Regression/Maps/')
os.system('cp slope.map ../../Regression/Maps/')
os.system('cp TPI.map ../../Regression/Maps/')
os.system('cp LandCover.map ../../Regression/Maps/')
os.system('cp Aspect.map ../../Regression/Maps/')
os.system('cp Sdown.map ../../Regression/Maps/')
os.system('cp Temp.map ../../Regression/Maps/')
os.system('cp SWE.map ../../Regression/Maps/')
os.system('cp LWP.map ../../Regression/Maps/')
os.system('cp PLC.map ../../Regression/Maps/')
os.system('cp Precip.map ../../Regression/Maps/')

### Copy to Box
os.system('cp VWC.map ../../Box_Plotting/Data/')
os.system('cp VPD.map ../../Box_Plotting/Data/')
os.system('cp DEM.map ../../Box_Plotting/Data/')
os.system('cp DynamicStress.map ../../Box_Plotting/Data/')
os.system('cp Min_DynamicStress.map ../../Box_Plotting/Data/')
os.system('cp slope.map ../../Box_Plotting/Data/')
os.system('cp TPI.map ../../Box_Plotting/Data/')
os.system('cp LandCover.map ../../Box_Plotting/Data/')
os.system('cp Aspect.map ../../Box_Plotting/Data/')
os.system('cp Sdown.map ../../Box_Plotting/Data/')
os.system('cp Temp.map ../../Box_Plotting/Data/')
os.system('cp SWE.map ../../Box_Plotting/Data/')
os.system('cp Precip.map ../../Box_Plotting/Data/')
os.system('cp LandFire_250m.map ../../Box_Plotting/Data/')
os.system('cp LandFire_30m.map ../../Box_Plotting/Data/')

##Copy to Granger
os.system('cp Dynamicstress* ../../Granger/Maps/')
os.system('cp FireMask_250m.map ../../Granger/Maps/')
os.system('cp Tseason.map ../../Granger/Maps/')
os.system('cp chan0.map ../../Granger/Maps/')
os.system('cp vcf* ../../Granger/Maps/')


os.system('cp DEM.map ../../Sitemaps/Data/')

### Move Files



## Remove Files
os.system('rm VWC.map')
os.system('rm VPD.map')
os.system('rm DynamicStress.map')
os.system('rm Tseason.map')
os.system('rm chan0.map')
os.system('rm LC.map')
os.system('rm DSI.map')
os.system('rm LC_masked.map')
os.system('rm DSI_masked.map')
os.system('rm unit_WS.map')
os.system('rm Tseason_WS.map')
os.system('rm unit.map')
os.system('rm Tsmask.map')
os.system('rm Points.map')
os.system('rm Catchment.map')
os.system('rm Catchment_Scalar.map')
os.system('rm CleanCatchment.map')
os.system('rm chan0_WS.map')
os.system('rm DynamicStress_masked.map')
os.system('rm DynamicStress_WS.map')
os.system('rm LandCover_masked.map')
os.system('rm VPD_WS.map')
os.system('rm VWC_WS.map')
os.system('rm *WS.map')
os.system('rm *masked.map')

