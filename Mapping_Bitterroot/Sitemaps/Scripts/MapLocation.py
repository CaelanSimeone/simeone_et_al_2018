import matplotlib.pyplot as plt
import numpy as np
import cartopy.io.img_tiles as cimgt
import cartopy.crs as ccrs
import os
import matplotlib.cm as cm
import pandas as pd
import cartopy.feature as cfeature
import pcraster as pcr


def scale_bar(ax, length, location=(0.5, 0.05), linewidth=3):
    """
    ax is the axes to draw the scalebar on.
    location is center of the scalebar in axis coordinates ie. 0.5 is the middle of the plot
    length is the length of the scalebar in km.
    linewidth is the thickness of the scalebar.
    """
    #Projection in metres, need to change this to suit your own figure
    proj = ccrs.AlbersEqualArea(central_longitude=-96,
                central_latitude=23,standard_parallels=(29.5,45.5))
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(proj)
    #Turn the specified scalebar location into coordinates in metres
    sbcx, sbcy = x0 + (x1 - x0) * location[0], y0 + (y1 - y0) * location[1]
    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbcx - length * 500, sbcx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sbcy, sbcy], transform=proj, color='k', linewidth=linewidth)
    #Plot the scalebar label
    ax.text(sbcx, sbcy, str(length) + ' km', transform=proj,
            horizontalalignment='center', verticalalignment='bottom')


def main(forest):

        dem = pcr.readmap('../Data/DEM.map')
        DEM = pcr.pcr2numpy(dem, 9999)
        DEM[DEM==9999]=np.nan

        stamen_terrain = cimgt.StamenTerrain()
        proj = ccrs.AlbersEqualArea(central_longitude=-96,
                    central_latitude=23,standard_parallels=(29.5,45.5))
        ax = plt.axes(projection=proj)
        #ax.outline_patch.set_visible(False)


        minx = -1446763
        maxy = 2732249
        maxx = -1340013
        miny = 2615749

        x = np.linspace(minx,maxx,DEM.shape[1])
        y = np.linspace(miny,maxy,DEM.shape[0])
        xx, yy = np.meshgrid(x,y)
        data = np.flipud(DEM)
        colormesh = ax.pcolor(xx, yy, data,
                        transform=proj,
                        cmap='bwr_r',linewidth=0,alpha = .3,vmin=10000,vmax=32000)
	
		
	plt.plot(-583100,3262000,'.',color='white',label='Correct')	
	plt.plot(-2200000,1562000,'.',color='white',label='Incorrect')
	#x = np.linspace(-1550000,-1550001,2)
        #y = np.linspace(29000000,29000001,2)
        #xx, yy = np.meshgrid(x,y)
        #data = xx/xx*.5
        #colormesh = ax.pcolor(xx, yy,data ,
        #               transform=proj,
        #               cmap='spectral',linewidth=0,vmin=0,vmax=20)
	
	ax.add_feature(cfeature.LAND)
    	ax.add_feature(cfeature.COASTLINE)
	states_provinces = cfeature.NaturalEarthFeature(
                category='cultural',
                name='admin_1_states_provinces_lines',
                scale='50m',
                facecolor='none')
    	ax.add_feature(cfeature.BORDERS)
    	ax.add_feature(states_provinces, edgecolor='black')
	#plt.legend(loc=3)	
        scale_bar(ax,50)
	plt.savefig('../Figures/Location.png')
	plt.show()

main(1)
#for i in np.arange(1,18):
#	main(i)

