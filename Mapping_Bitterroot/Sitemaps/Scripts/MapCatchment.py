import matplotlib.pyplot as plt
import cartopy.feature as cfeature
import numpy as np
import cartopy.io.img_tiles as cimgt
import cartopy.crs as ccrs
import os
import pcraster as pcr
#os.system('gdalinfo ./PREF_dem_NOBUFFER.tif')
import pandas as pd

def scale_bar(ax, length, location=(0.5, 0.2), linewidth=3):
    """
    ax is the axes to draw the scalebar on.
    location is center of the scalebar in axis coordinates ie. 0.5 is the middle of the plot
    length is the length of the scalebar in km.
    linewidth is the thickness of the scalebar.
    """
    #Projection in metres, need to change this to suit your own figure
    proj = ccrs.AlbersEqualArea(central_longitude=-96,
                central_latitude=23,standard_parallels=(29.5,45.5)) 
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(proj)
    #Turn the specified scalebar location into coordinates in metres
    sbcx, sbcy = x0 + (x1 - x0) * location[0], y0 + (y1 - y0) * location[1]
    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbcx - length * 500, sbcx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sbcy, sbcy], transform=proj, color='k', linewidth=linewidth)
    #Plot the scalebar label
    ax.text(sbcx, sbcy, str(length) + ' km', transform=proj,
            horizontalalignment='center', verticalalignment='bottom')



def main(year):

	dem = pcr.readmap('../Data/DEM.map')
	DEM = pcr.pcr2numpy(dem, 9999)
	DEM[DEM<9999]=np.nan	
	
    	stamen_terrain = cimgt.StamenTerrain()
	proj = ccrs.AlbersEqualArea(central_longitude=-96,
                    central_latitude=23,standard_parallels=(29.5,45.5))
    	ax = plt.axes(projection=proj)
	ax.outline_patch.set_visible(False)	
   
    	ax.add_image(stamen_terrain,10)
	
	minx = -1446763
	maxy = 2732249
	maxx = -1340013
	miny = 2615749 
	
	x = np.linspace(minx,maxx,DEM.shape[1])
	y = np.linspace(miny,maxy,DEM.shape[0])
	xx, yy = np.meshgrid(x,y)
	data = np.flipud(DEM)	
    	colormesh = ax.pcolor(xx, yy, data,
    	            	transform=proj,
    	            	cmap='gray',linewidth=0,vmin=1000,vmax=3200)
    

	plt.plot(maxx,maxy+5000,'.',alpha=0,color='white')    
	scale_bar(ax,10)
	plt.colorbar(colormesh, fraction =0.02,pad=0.04)
	plt.legend(loc=4)
	plt.savefig('../Figures/BitterrootCatchment.png',transparent=True)
	plt.show()
	plt.close()
#if __name__ == '__main__':
#    main()

main('2004')
