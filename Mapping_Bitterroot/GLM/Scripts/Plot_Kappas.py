#
from __future__ import division
import matplotlib.pylab as plt
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
import math
import scipy
from scipy.stats import gaussian_kde

## Import Data
forest_cover = pcr.readmap('../Maps/vcf_2015.map')
Forest_Cover = pcr.pcr2numpy(forest_cover,9999).flatten()

forest_fire = pcr.readmap('../Maps/LandFire_250m.map')
Forest_Fire = pcr.pcr2numpy(forest_fire,9999).flatten()

fire_mask = pcr.readmap('../Maps/FireMask_250m.map')
Fire_Mask = pcr.pcr2numpy(fire_mask,-9999).flatten()

tseas = pcr.readmap('../Maps/Tseason.map')
Tseas = pcr.pcr2numpy(tseas,9999).flatten()

chan = pcr.readmap('../Maps/chan0.map')
Channel = pcr.pcr2numpy(chan,9999).flatten()

chan_WS = pcr.readmap('../Maps/chan0_WS.map')
Channel_WS = pcr.pcr2numpy(chan_WS,9999).flatten()

dfData = pd.DataFrame(data = {'Forest_Cover':Forest_Cover,'Season_Length':Tseas,'Channel':Channel,'Land_Fire':Forest_Fire,'Fire_Mask':Fire_Mask,'Channel_WS':Channel_WS})

Years =np.arange(2001,2016,1).astype('str')
for year in Years:
	raster = pcr.readmap('../Maps/Dynamicstress'+year+'.map')
	dfData[year] = pcr.pcr2numpy(raster, 9999).flatten()


Forest_Cutoff = 0.1
Season_Cutoff = 140
Fire_Cutoff = 0.1
for Time in ['No_WS','WS']:
	if Time == 'No_WS':
		dfData = dfData[dfData.Channel != 9999]
	if Time == 'WS':
		dfData = dfData[dfData.Channel_WS != 9999]
	dfData['Mean'] = dfData[Years].mean(axis=1)
	dfData['Max'] = dfData[Years].max(axis=1)
	dfData['Min'] = dfData[Years].min(axis=1)
	dfData['Q50'] = dfData[Years].quantile(0.5,axis=1)
	dfData['Q25'] = dfData[Years].quantile(0.25,axis=1)
	dfData['Q75'] = dfData[Years].quantile(0.75,axis=1)
	
	dfData = dfData[dfData.Season_Length >= Season_Cutoff]
	dfData = dfData[dfData.Fire_Mask <= Fire_Cutoff]
	
	dfData['Forest_Presence']=np.where(dfData['Forest_Cover']>=Forest_Cutoff,1.0,0.0)
	#dfData['Landfire_Presence']=np.where(dfData['Land_Fire']>=1,1.0,0.0)
	dfSample = dfData.sample(int(len(dfData)/2))

	print len(dfSample)		
	dfSample.to_csv('Forest_Cover_'+Time+'.csv')
	
	X = np.arange(0.0,1.01,0.01)
	# Just a figure and one subplot
	fig, ax = plt.subplots()

	for how in ['Min']:
		print how
		xcount = 0
		Kappa_Space = np.empty(len(X))
		P_o_Space = np.empty(len(X))
		P_e_Space = np.empty(len(X))
		for DSI_Cutoff in tqdm(X):
			dfSample['DSI_Prediction']=np.where(dfSample[how]<=DSI_Cutoff,1.0,0.0)
			#LC = np.asarray(dfSample['Forest_Presence'])
			LC = np.asarray(dfSample['Land_Fire'])
			DSI = np.asarray(dfSample['DSI_Prediction'])
		
	
			##Cohen's Kappa, See Wikipedia for instructions
			A = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y == 1.]))
			B = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y != 1.]))
			C = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y == 1.]))
			D = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y != 1.]))
			P_o = (A+D)/(A+B+C+D)
			Total = A+B+C+D
			#print 'Classification Accuracy = ', P_o 

			Forested = (A+B)/Total
			print Forested
	
			P_yes = ((A+B)/Total) * ((A+C)/Total)
			#print P_yes
			P_no = ((B+D)/Total) * ((C+D)/Total)
			#print P_no
			
			P_e = P_yes + P_no
			
			Kappa = (P_o - P_e)/(1-P_e)
			Kappa_Space[xcount] =Kappa
			P_o_Space[xcount] =P_o
			P_e_Space[xcount] =P_e
			
			xcount +=1
		ax.plot(X,Kappa_Space,label=how+' ,K = '+str(round(Kappa_Space.max(),2)))
		ax.plot(X,P_o_Space,label=how+' ,P_o = '+str(round(Kappa_Space.max(),2)))
		ax.plot(X,P_e_Space,label=how+' ,P_e = '+str(round(Kappa_Space.max(),2)))
	ax.legend(loc=0)
	ax.set_xlabel('DSI'+str(Season_Cutoff))
	ax.set_ylabel('Cohens Kappa')
	#plt.tight_layout()
	if Time == 'No_WS':
		fig.savefig('../Figures/DSI_MODIS.png')
	if Time == 'WS':
		fig.savefig('../Figures/DSI_MODIS_WS.png')
#	plt.close()
	plt.show()
	fig, ax = plt.subplots()
	
	print X	
	for how in ['LandFire']:
		xcount = 0
		Kappa_Space = np.empty(len(X))
		P_o_Space = np.empty(len(X))
		P_e_Space = np.empty(len(X))
		for DSI_Cutoff in X:
			
			dfSample['DSI_Prediction']=np.where(dfSample['Forest_Cover']>=DSI_Cutoff,1.0,0.0)
			
			LC = np.asarray(dfSample['Land_Fire'])
			DSI = np.asarray(dfSample['DSI_Prediction'])
			
			##Cohen's Kappa, See Wikipedia for instructions
			A = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y == 1.]))
			B = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y != 1.]))
			C = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y == 1.]))
			D = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y != 1.]))
			P_o = (A+D)/(A+B+C+D)
			Total = A+B+C+D
			#print 'Classification Accuracy = ', P_o 
			
			P_yes = ((A+B)/Total) * ((A+C)/Total)
			#print P_yes
			P_no = ((B+D)/Total) * ((C+D)/Total)
			#print P_no
			
			P_e = P_yes + P_no
			
			Kappa = (P_o - P_e)/(1-P_e)
			Kappa_Space[xcount] =Kappa
			P_o_Space[xcount] =P_o
			P_e_Space[xcount] =P_e
			xcount+=1
		ax.plot(X,Kappa_Space,label=how+' ,K = '+str(round(Kappa_Space.max(),2)))
		ax.plot(X,P_o_Space,label=how+' ,P_o = '+str(round(Kappa_Space.max(),2)))
		ax.plot(X,P_e_Space,label=how+' ,P_e = '+str(round(Kappa_Space.max(),2)))
		ax.axvline(x=Forest_Cutoff,color='black',label = 'K at Modis Cutoff = '+str(round(Kappa_Space[int(Forest_Cutoff*100)],2)))
	ax.legend(loc=0)
	ax.set_xlabel('Modis Cutoff For Forest/No Forest')
	ax.set_ylabel('Cohens Kappa')
	ax.set_ylim(0,1)
	if Time == 'No_WS':
		fig.savefig('../Figures/Landfire_MODIS.png')
	if Time == 'WS':
		fig.savefig('../Figures/Landfire_MODIS_WS.png')
	plt.show()


### Min 0.15 Forest Cover. .468
#X = np.arange(.025,.25,.025)
#Y = np.arange(0.41,.61,0.01)
#Kappa_Space = np.empty([len(Y),len(X)])
#xcount=0
#for Forest_Cutoff in X:
#	ycount = 0
#	for DSI_Cutoff in Y:
#		dfData = dfData[dfData.Season_Length >= Season_Cutoff]
#		dfData['Forest_Presence']=np.where(dfData['Forest_Cover']>=Forest_Cutoff,1.0,0.0)
#		#dfData['Forest_Presence']=np.where(dfData['Land_Fire']>=Forest_Cutoff,1.0,0.0)
#		dfData['DSI_Prediction']=np.where(dfData['Mean']<=DSI_Cutoff,1.0,0.0)
#		
#		LC = np.asarray(dfData['Forest_Presence'])
#		DSI = np.asarray(dfData['DSI_Prediction'])
#		
#		##Cohen's Kappa, See Wikipedia for instructions
#		A = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y == 1.]))
#		B = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y != 1.]))
#		C = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y == 1.]))
#		D = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y != 1.]))
#		print A,D,B,C	
#		P_o = (A+D)/(A+B+C+D)
#		print P_o
#		Total = A+B+C+D
#		#print 'Classification Accuracy = ', P_o 
#		
#		P_yes = ((A+B)/Total) * ((A+C)/Total)
#		#print P_yes
#		P_no = ((B+D)/Total) * ((C+D)/Total)
#		#print P_no
#		
#		P_e = P_yes + P_no
#		
#		Kappa = (P_o - P_e)/(1-P_e)
#		print 'Kappa = ', Kappa, Forest_Cutoff, DSI_Cutoff
#		Kappa_Space[ycount,xcount] =Kappa
#		ycount +=1
#	xcount +=1
#
#Kappa_Space = np.where(Kappa_Space >= 0.35,Kappa_Space,0.35)
#plt.imshow(Kappa_Space,extent=[X[0],X[-1],Y[-1],Y[0]],aspect='auto')
#plt.xlabel('Percent Forest Cover')
#plt.ylabel('DSI Predicted Cover')

#plt.show()
