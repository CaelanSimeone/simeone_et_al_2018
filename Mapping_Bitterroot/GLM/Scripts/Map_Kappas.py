#
from __future__ import division
import matplotlib.pylab as plt
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
import math
import scipy
from scipy.stats import gaussian_kde

## Import Data
forest_cover = pcr.readmap('../Maps/vcf_2015.map')
Forest_Cover = pcr.pcr2numpy(forest_cover,9999).flatten()

forest_fire = pcr.readmap('../Maps/LandFire_250m.map')
Forest_Fire = pcr.pcr2numpy(forest_fire,9999).flatten()

fire_mask = pcr.readmap('../Maps/FireMask_250m.map')
Fire_Mask = pcr.pcr2numpy(fire_mask,-9999).flatten()

tseas = pcr.readmap('../Maps/Tseason.map')
Tseas = pcr.pcr2numpy(tseas,9999).flatten()

chan = pcr.readmap('../Maps/chan0.map')
Channel = pcr.pcr2numpy(chan,9999).flatten()

chan_WS = pcr.readmap('../Maps/chan0_WS.map')
Channel_WS = pcr.pcr2numpy(chan_WS,9999).flatten()

dfData = pd.DataFrame(data = {'Forest_Cover':Forest_Cover,'Season_Length':Tseas,'Channel':Channel,'Land_Fire':Forest_Fire,'Channel_WS':Channel_WS,'Fire_Mask':Fire_Mask})

Years =np.arange(2001,2016,1).astype('str')
for year in Years:
	raster = pcr.readmap('../Maps/Dynamicstress'+year+'.map')
	dfData[year] = pcr.pcr2numpy(raster, 9999).flatten()

Fire_Cutoff = 0

for Time in ['No_WS','WS']:
	Forest_Cutoff = 0.1
	Season_Cutoff = 150
	if Time == 'No_WS':
		dfData = dfData[dfData.Channel != 9999]
	if Time == 'WS':
		dfData = dfData[dfData.Channel_WS != 9999]
	dfData['Mean'] = dfData[Years].mean(axis=1)
	dfData['Max'] = dfData[Years].max(axis=1)
	dfData['Min'] = dfData[Years].min(axis=1)
	dfData['Q50'] = dfData[Years].quantile(0.5,axis=1)
	dfData['Q25'] = dfData[Years].quantile(0.25,axis=1)
	dfData['Q75'] = dfData[Years].quantile(0.75,axis=1)
	
	dfData = dfData[dfData.Fire_Mask <= Fire_Cutoff]
	dfSample = dfData.sample(1000)
	

	dfSample['LandFire_Prediction']=np.where(dfSample['Land_Fire']>=1,1.0,0.0)
	## Forest_Cover DSI
	X = np.arange(.05,1.01,.01)
	Y = np.arange(0.01,1.01,0.01)
	Kappa_Space = np.empty([len(Y),len(X)])
	xcount=0
#	for Forest_Cutoff in tqdm(X):
#		ycount = 0
#		dfSample = dfSample[dfSample.Season_Length >= Season_Cutoff]
#		dfSample['Forest_Presence']=np.where(dfSample['Forest_Cover']>=Forest_Cutoff,1.0,0.0)
#		for DSI_Cutoff in Y:
#			dfSample['DSI_Prediction']=np.where(dfSample['Mean']<=DSI_Cutoff,1.0,0.0)
#			
#			LC = np.asarray(dfSample['Forest_Presence'])
#			DSI = np.asarray(dfSample['DSI_Prediction'])
#			
#			##Cohen's Kappa, See Wikipedia for instructions
#			A = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y == 1.]))
#			B = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y != 1.]))
#			C = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y == 1.]))
#			D = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y != 1.]))
#			P_o = (A+D)/(A+B+C+D)
#			Total = A+B+C+D
#			#print 'Classification Accuracy = ', P_o 
#			
#			P_yes = ((A+B)/Total) * ((A+C)/Total)
#			#print P_yes
#			P_no = ((B+D)/Total) * ((C+D)/Total)
#			#print P_no
#			
#			P_e = P_yes + P_no
#			
#			Kappa = (P_o - P_e)/(1-P_e)
#			Kappa_Space[ycount,xcount] =Kappa
#			ycount +=1
#		xcount +=1
#	
#	#Kappa_Space = np.where(Kappa_Space >= 0.25,Kappa_Space,0.25)
#	imgplot = plt.imshow(Kappa_Space,extent=[X[0],X[-1],Y[-1],Y[0]],aspect='auto',vmin=0,vmax=.7)
#	imgplot.set_cmap('nipy_spectral')
#	plt.colorbar(imgplot)
#	plt.xlabel('Percent Forest Cover')
#	plt.ylabel('DSI Predicted Cover')
#	if Time == 'No_WS':
#		plt.savefig('../Figures/Map_DSI_MODIS.png')
#	if Time == 'WS':
#		plt.savefig('../Figures/Map_DSI_MODIS_WS.png')
#	#plt.show()
#	plt.close()

	dfSample = dfData.sample(1000)
	Forest_Cutoff = 0.1
	#Season_Cutoff = 150

	## Season DSI
	X = np.arange(50,240,5)
	Y = np.arange(0.01,1.01,0.01)
	Kappa_Space = np.empty([len(Y),len(X)])
	xcount=0
	for Season_Cutoff in tqdm(X):
		ycount = 0
		dfSample = dfSample[dfSample.Season_Length >= Season_Cutoff]
		dfSample['Forest_Presence']=np.where(dfSample['Forest_Cover']>=Forest_Cutoff,1.0,0.0)
		for DSI_Cutoff in Y:
			dfSample['DSI_Prediction']=np.where(dfSample['Mean']<=DSI_Cutoff,1.0,0.0)
			
			LC = np.asarray(dfSample['Forest_Presence'])
			#LC = np.asarray(dfSample['Land_Fire'])
			DSI = np.asarray(dfSample['DSI_Prediction'])
			
			##Cohen's Kappa, See Wikipedia for instructions
			A = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y == 1.]))
			B = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y != 1.]))
			C = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y == 1.]))
			D = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y != 1.]))
			P_o = (A+D)/(A+B+C+D)
			Total = A+B+C+D
			#print 'Classification Accuracy = ', P_o 
			
			P_yes = ((A+B)/Total) * ((A+C)/Total)
			#print P_yes
			P_no = ((B+D)/Total) * ((C+D)/Total)
			#print P_no
			
			P_e = P_yes + P_no
			
			Kappa = (P_o - P_e)/(1-P_e)
			Kappa_Space[ycount,xcount] =Kappa
			ycount +=1
		xcount +=1
	
	#Kappa_Space = np.where(Kappa_Space >= 0.25,Kappa_Space,0.25)
	#imgplot = plt.imshow(Kappa_Space,extent=[X[0],X[-1],Y[-1],Y[0]],aspect='auto')
	#imgplot.set_cmap('nipy_spectral')
	#plt.colorbar()
	imgplot = plt.imshow(Kappa_Space,extent=[X[0],X[-1],Y[-1],Y[0]],aspect='auto',vmin=0,vmax=.7)
	imgplot.set_cmap('nipy_spectral')
	plt.colorbar(imgplot)
	plt.xlabel('Season Length')
	plt.ylabel('DSI Predicted Cover')
	if Time == 'No_WS':
		plt.savefig('../Figures/Map_DSI_Season.png')
	if Time == 'WS':
		plt.savefig('../Figures/Map_DSI_Season_WS.png')
	plt.show()
	plt.close()

#	dfSample = dfData.sample(2500)
#	Forest_Cutoff = 0.1
#	Season_Cutoff = 140
#	DSI_Cutoff = .56
#
#	## Season Modis
#	X = np.arange(50,240,5)
#	Y = np.arange(0.03,1.01,0.01)
#	Kappa_Space = np.empty([len(Y),len(X)])
#	xcount=0
#	for Season_Cutoff in tqdm(X):
#		ycount = 0
#		dfSample = dfSample[dfSample.Season_Length >= Season_Cutoff]
#		dfSample['Forest_Presence']=np.where(dfSample['Forest_Cover']>=Forest_Cutoff,1.0,0.0)
#		dfSample['DSI_Prediction']=np.where(dfSample['Mean']<=DSI_Cutoff,1.0,0.0)
#		for Forest_Cutoff in Y:
#			dfSample['Forest_Presence']=np.where(dfSample['Forest_Cover']>=Forest_Cutoff,1.0,0.0)
#			
#			LC = np.asarray(dfSample['Forest_Presence'])
#			DSI = np.asarray(dfSample['DSI_Prediction'])
#			
#			##Cohen's Kappa, See Wikipedia for instructions
#			A = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y == 1.]))
#			B = (sum([1 for (x,y) in zip(LC,DSI) if x == 1. and y != 1.]))
#			C = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y == 1.]))
#			D = (sum([1 for (x,y) in zip(LC,DSI) if x != 1. and y != 1.]))
#			P_o = (A+D)/(A+B+C+D)
#			Total = A+B+C+D
#			#print 'Classification Accuracy = ', P_o 
#			
#			P_yes = ((A+B)/Total) * ((A+C)/Total)
#			#print P_yes
#			P_no = ((B+D)/Total) * ((C+D)/Total)
#			#print P_no
#			
#			P_e = P_yes + P_no
#			
#			Kappa = (P_o - P_e)/(1-P_e)
#			Kappa_Space[ycount,xcount] =Kappa
#			ycount +=1
#		xcount +=1
#	
#	#Kappa_Space = np.where(Kappa_Space >= 0.25,Kappa_Space,0.25)
#	#plt.imshow(Kappa_Space,extent=[X[0],X[-1],Y[-1],Y[0]],aspect='auto')
#	imgplot = plt.imshow(Kappa_Space,extent=[X[0],X[-1],Y[-1],Y[0]],aspect='auto',vmin=0,vmax=.7)
#	imgplot.set_cmap('nipy_spectral')
#	plt.colorbar(imgplot)
#	plt.ylabel('Percent Forest Cover')
#	plt.xlabel('Season Length')
#	if Time == 'No_WS':
#		plt.savefig('../Figures/Map_Modis_Season.png')
#	if Time == 'WS':
#		plt.savefig('../Figures/Map_Modis_Season_WS.png')
#	#plt.show()
#	plt.close()
