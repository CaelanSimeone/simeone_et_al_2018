#
from __future__ import division
import matplotlib.pylab as plt
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
import math
import scipy
from scipy.stats import gaussian_kde

## Import Data
forest_cover = pcr.readmap('../Maps/vcf_2015.map')
Forest = pcr.pcr2numpy(forest_cover,9999)
Forest_Cover = Forest.flatten()

forest_fire = pcr.readmap('../Maps/LandFire_250m.map')
Forest_Fire = pcr.pcr2numpy(forest_fire,9999).flatten()

fire_mask = pcr.readmap('../Maps/FireMask_250m.map')
Fire_Mask = pcr.pcr2numpy(fire_mask,-9999).flatten()

tseas = pcr.readmap('../Maps/Tseason.map')
Tseas = pcr.pcr2numpy(tseas,9999).flatten()

chan = pcr.readmap('../Maps/chan0.map')
Channel = pcr.pcr2numpy(chan,9999).flatten()

chan_WS = pcr.readmap('../Maps/chan0_WS.map')
Channel_WS = pcr.pcr2numpy(chan_WS,9999).flatten()

dfData = pd.DataFrame(data = {'Forest_Cover':Forest_Cover,'Season_Length':Tseas,'Channel':Channel,'Land_Fire':Forest_Fire,'Fire_Mask':Fire_Mask,'Channel_WS':Channel_WS})

Years =np.arange(2001,2016,1).astype('str')
for year in Years:
	raster = pcr.readmap('../Maps/Dynamicstress'+year+'.map')
	dfData[year] = pcr.pcr2numpy(raster, 9999).flatten()


dfData['Min'] = dfData[Years].min(axis=1)
Min = np.asarray(dfData['Min'])

Min = Min.reshape(Forest.shape)
Min_pcr = pcr.numpy2pcr(Scalar, Min, 9999)
pcr.report(Min_pcr, 'Min_DSI.map')

