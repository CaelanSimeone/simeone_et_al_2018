#
from __future__ import division
import matplotlib.pylab as plt
import pcraster as pcr
from pcraster import *
import numpy as np
import pandas as pd
from tqdm import tqdm
import math
import scipy
from scipy.stats import gaussian_kde

## Import Data
tseas = pcr.readmap('../Maps/Tseason.map')
Tseas = pcr.pcr2numpy(tseas,9999).flatten()
print len(Tseas)
Tseas = Tseas[Tseas != 9999]
print len(Tseas)
