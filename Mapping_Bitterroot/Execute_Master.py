### This script does all of the postprocessing of maps from the bitterroot.

import os
## This part takes maps, reformats them, and distributes them to the proper folders. 
os.chdir('Processing')
os.system('python Execute_Processing.py')

## This directory creates maps for both the entire bitterroot and the skalkaho basin for a select set of variables. Figure 3
os.chdir('../BitterrootMaps/Scripts')
#os.system('./Execute_Maps.sh')

# This Directory creates maps for landcover figure 7 in my paper. 
os.chdir('../../LandCover/Scripts')
os.system('./ExecuteLC.sh')

# GLM stuff
os.chdir('../../GLM/Scripts')
os.system('python Plot_Kappas.py')
os.system('python Map_Kappas.py')
os.system('cp ../Figures/* ~/Documents/Inkscape/GLM/')

# This Directory runs regressions for DSI vs other important variables.
os.chdir('../../Regression/Scripts')
os.system('python Regression.py')
os.system('cp ../Figures/Regression.png ~/Documents/Masters_Thesis/Figures/Defence/')

## Sitemap
os.chdir('../../Sitemaps/Scripts')
os.system('./Execute.sh')
